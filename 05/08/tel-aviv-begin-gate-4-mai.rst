

.. _k_2024_05_08:

=====================================================================================================
2024-05-08 Tel Aviv, Begin Gate, Samedi 4 mai 2024, 20h37 (Jour 210) par Julia Christ & Élie Petit
=====================================================================================================

- https://k-larevue.com/tel-aviv-begin-gate-4-mai/


Deux membres de la rédaction de K., Julia Christ et Élie Petit, sont en ce
moment en Israël pour documenter et analyser les divers mouvements en cours
dans le pays, après le 7 octobre — alors que la guerre à Gaza a toujours lieu
et qu’un accord est en pourparlers indirects entre Israël et le Hamas pour
la cessation des combats et la libération des otages.

Première étape de leur parcours cette semaine : dès leur premier soir à Tel Aviv,
ils ont participé à l’une des manifestations anti-Netanyahu hebdomadaires,
guidés par la productrice Karen Belz. Ils en rapportent « choses vues »,
impressions et premières analyses.

