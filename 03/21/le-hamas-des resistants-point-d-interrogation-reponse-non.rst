.. index::
   pair: Denis Charbit; Le Hamas, des résistants ? (2024-03-21)

.. _charbit_2024_03_21:

===================================================================
2024-03-21  **Le Hamas, des résistants ?** par Denis Charbit
===================================================================

.. youtube:: u2zOzzTmKEk


Comme en témoigne la dernière saillie de Judith Butler, le discours  assimilant
les terroristes du Hamas à un mouvement de résistance prend une ampleur considérable,
jusque dans les milieux intellectuels.

**Denis Charbit poursuit sa série consacrée aux mots du conflit : il nettoie
le terme de résistance en le dissociant de l'assassinat et de la déshumanisation
de civils.**


La constitution du Hamas incontestablement s'inscrit dans un mouvement
d'opposition, de contestation de l'existence de l'Etat d'Israël, et non
pas seulement de l’occupation israélienne, et je dirais que c’est
là toute l’ambiguïté de l’emploi de ce terme.

**Parce que quand on dit résistance, on suppose donc qu'on s'oppose à une force d'occupation,
mais qu'on ne conteste pas l'existence du peuple et de l'État**.

Je prends l'exemple, bien sûr classique, de la résistance française à l'Allemagne
nazie.
Les résistants français ne s'opposaient pas à l'existence de l’Allemagne, ils
s’opposaient au fait que l’Allemagne occupait le territoire français.

Et c’est là où l’analogie ne fonctionne pas.

Parce que le mouvement national palestinien est toujours à la croisée des
chemins.
L’opposition du mouvement national palestinien commence-t-elle
avec l’occupation des territoires, ou bien est-elle bien antérieure et
bien plus fondamentale, c’est-à-dire à l’existence même de l'État d’Israël ?

Je serais presque tenté de répondre de la manière suivante.

Si l'objectif d'un mouvement national palestinien quelconque, que ce soit le
Fatah ou le Hamas, c'est l'occupation israélienne des territoires occupés,
alors je le dis en toute clarté et sans aucune ambiguïté, le mouvement
national palestinien en question est un mouvement de résistance à l'occupation
israélienne.

Seulement, si on voit de près les manifestes qui président à ces organisations
palestiniennes, on voit bien que toutes du Fatah au Hamas, initialement, c'est
bien la destruction de l'État d'Israël, en quel cas ce n'est plus un mouvement
de résistance.

Et donc à cet égard, tout mouvement palestinien initialement, c'est l'opposition
au sionisme et la destruction de l'État d'Israël qu'ils poursuivent à ceci
près qu’après la guerre des Six Jours dans les années 80 et notamment les années 90,
à un moment l'OLP fait le choix stratégique de considérer, en étant à l’écoute
de ce qui se passe dans le monde, que la légitimité d'un État palestinien
n'est réelle que si elle ne vise plus la destruction de l'État d'Israël, mais
si elle vise l'occupation israélienne.

On peut considérer que même le Fatah est dans une sorte d'ambiguïté vis-à-vis
de cette question-là, mais il y a une chose qui est claire, c'est que chez le Hamas,
il n'y a pas d'ambiguïté.

C'est pas : on est pour l'État d'Israël dans les frontières de 67, et on est seulement
contre l'occupation de la Cisjordanie et le blocus imposé à Gaza.

Pas du tout.

Si on voit la charte du Hamas, la charte initiale, **on voit bien qu'on est dans
une volonté d'éradiquer l'État d'Israël, mais même d'éradiquer les Juifs**.

D’extirper les Juifs de l'univers et donc on n'est plus du tout dans une
résistance, **on est ici dans un mouvement exterminationniste**.

**Je sais bien, je ne peux pas ne pas le mentionner, qu’il y a une charte qui
date de 2017, au terme duquel le Hamas a admis l’idée de deux États**.

Mais je dirais que cela apparaît comme une **sorte de revers tactique** auquel
on ne peut pas porter un véritable crédit.

Je n'ai jamais entendu les leaders du Hamas proclamer que leur contentieux
avec Israël se borne à l'occupation de la Cisjordanie et au blocus de Gaza.

Et donc il faut prendre énormément de précaution, de prudence, et même, je dirais
de scepticisme, la déclaration dite nouvelle du Hamas en 2017, acceptant du
bout des lèvres le principe de deux États.

Et donc cet égard, encore une fois, je dirais que oui, il y a un élément de
résistance à l'occupation dans le combat palestinien quel qu’il soit, mais
ce qui réduit la portée de cette résistance, c'est deux choses.

D'abord, la première, c'est que la finalité reste pour la plupart des mouvements
palestiniens, l'élimination de l'État d'Israël, et puis, secondement et ça,
il faut jamais l'oublier, la nature des moyens employés.

Car il faut bien faire la distinction dans un combat de résistance entre l’élimination
des soldats, des militaires, et l'élimination des civils.

Le 7 octobre 2023, c'est bien la démonstration in vivo de ce que voudrait
réaliser le Hamas à l'échelle de l'État d'Israël tout entier, c’est-à-dire
l'élimination pure et simple et plus que d'ailleurs l'élimination physique,
la profanation, les viols, les sévices, le fait de brûler vif les victimes,
tout ça démontre bien, **non plus une volonté de résistance, mais une volonté
exterminatrice**.

Et ceux qui pensent que le Hamas est un mouvement de résistance, je les appelle
à nous donner des analogies, à nous montrer des Oradour-sur-Glane que les
résistants auraient opéré sur une population civile allemande.

C'est peine perdue.

Il y a bien là une singularité du mouvement national palestinien et je dirais
que c'est peut-être la raison pour laquelle les Israéliens sont fort sceptiques
sur les intentions palestiniennes.

L’attaque du 7 octobre 2023, le crime de masse du 7 octobre 2023 ne s'est
pas contenté de s'opposer aux forces d'occupation militaire, mais de s'en
prendre aux civils et pas simplement de les éliminer physiquement, mais
de les profaner et **d'arriver à une déshumanisation qu’on n’a jamais vue
dans toute l'histoire des résistances que l'on connaît au vingtième siècle**.
