.. index::
   pair: Iannis Roder ; Y-a-t-il un génocide à Gaza ? (2024-02-08)

.. _roder_2024_02_08:

================================================================================
2024-02-08 **Y-a-t-il un génocide à Gaza ?** par Iannis Roder, historien
================================================================================

- https://akadem.org/y-a-t-il-genocide-a-gaza


Le conflit mot à mot

Iannis Roder
================

- https://akadem.org/author/iannis-roder

Iannis Roder historien Professeur agrégé dans un collège en Réseau
d'éducation prioritaire en Seine-Saint-Denis depuis 1999, Iannis Roder est
responsable des formations au Mémorial de la Shoah, membre du conseil des
Sages de la laïcité et Directeur de l'Observatoire de l'éducation de la
Fondation Jean Jaurès.

Co-auteur des Territoires perdus de la République, il est également
consultant pédagogique et formateur au Mémorial de la Shoah.

En 2018, il publie Allons z'enfants...la République vous appelle ! dans lequel
il témoigne de ses expériences positives d'enseignant dans le 93.

En 2020, il publie un ouvrage consacré à la question de l'enseignement de l'histoire
de la Shoah, "Sortir de l'ère victimaire. Pour une nouvelle de l'approche de
la Shoah et des crimes de masse", chez Odile Jacob.
