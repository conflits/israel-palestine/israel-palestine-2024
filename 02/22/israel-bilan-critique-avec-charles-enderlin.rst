.. index::
   pair: Charles Enderlin ; Israël : bilan critique  (2024-02-22)

.. _enderlin_2024_02_22:

===================================================================
2024-02-22 **Israël : bilan critique (avec Charles Enderlin)**
===================================================================

.. youtube:: Ho-x4Wslcms

#israel #palestine #charlesenderlin #hamas #sionisme #tsahal

Entretien
=============

bonjour à tous et à toutes bienvenue sur Martin modcast aujourd'hui c'est
un grand jour parce qu'on nous a que nous monsieur Anderlin bonjour monsieur
Anderlin bonjour Martin bonjour pour les quelques personnes qui vous connaîtrai
pas est-ce que vous pourriez vaguement vous présenter parce que étonnamment
enfin quand j'ai dit j'invité charlesin tout fier la moitié des gens me
disaient c'est qui voilà bah écoutez c'est vrai le temps est passé je n'ai
pratiquement plus plus fait de télé depuis le le depuis enf en fait après
2011 12 13 le nombre de sujets a considérablement diminué et puis les jeunes
euh n'ont pas étudié le proche orient commence à découvrir qu'il y a un
problème entre des Palestiniens et des Israéliens ne comprennent pas très
bien qu'est-ce qui est qu'est-ce qui est islamiste qu'est-ce qui est sioniste
donc vous savez même des personnalités de premier plan dans des rédactions
de télé ou de journaux euh découvre tout ta il faut m'expliquer de nouveau
qu'est-ce qui se passe B voilà d'accord très bien et on va parler aujourd'hui
de enfin de quatre articles qui sont plutôt intéressants et de votre petit
livre au seuil comment dir Israël d'agonieque d'e démocratie et plus tard
revenir sur votre livre votre livre au nom du temple où les l'arrivée des
Juifs messianiques au pouvoir donc je vous propose qu'on commence par l'article
par le le Hamas les origines du mal je pense qu'on je pense que commencer par
celui-là ça ça a du sens et bien écoutez correspondant envoyé spécial de
France 2 depuis le 20 8182 euh un de mes une de mes fonctions ça a été de
couvrir Gaza et à Gaza dans les années 80 j'ai découvert le développement
d'une association euh islamique euh dirigée par un cher paraplégique qui
s'appelait le Cher Ahmed Yassine c'était le chouchou de l'état-major militaire
de Gaza tout simplement c'était l'opposition au Fatar les ennemis d'Israël
à l'époque c'était la gauche le FPLP le FDLP le Fatar l'OLP alors le Cher
Yassine au tout début des années 70 est allé voir le gouverneur militaire
pour lui dire vous savez moi je ne suis pas du tout comme ces buveurs d'alcool
ces mauvais musulmans que sont le Fatar et les autres moi je veux faire du
social du sport et de la religion formidable le gars le gouverneur militaire a
dit d'accord ils ont commencé à favoriser le développement de ce qui était
à l'époque l'Union islamique l'Union islamique a poussé partout comme des
champignons sous la pluie a fini par prendre le contrôle de la plupart des
mosquées de Gaza avec la bénédiction des autorités militaires israéliennes
un beau jour le seul cinéma de Gaza a été incendié les les derniers café
qui servait encore de la bière ont été incendié aussi et puis un beauonjour
les Israéliens ont découvert que cet islam radical ça posait un problème
il y avait un officier chez le gouverneur militaire qui était chargé de
suivre le développement de la religion à Gaza évidemment l'islam mais il y
a également une petite communauté chrétienne il a découvert que cet islam
radical était effectivement extrêmement dangereux c'était la branche la
plus radicale des Frères musulmans extrêmement anti-juif anti-européen
et c'est ce qui se passait dans les prêches il a essayé de s'opposer au
développement favorisé par l'armée israélienne de l'Union islamique et
puis finalement la première Intifada euh fin fin 87 début 88 mi 88 tout à
où l'union islamique s'est transformé en ramas la branche militaire des
Frères musulmans radicaux les plus radicaux de Gaza qui ont commencé à
attaquer l'armée israélienne puis les civils israéliens j'ai suivi tout
cela et en 2009 j'ai fait un livre qui s'appelle les le grand aveuglement
racontant comment les autorités israéliennes ont contribué et favorisé le
développement de l'islam radical à Gaza plus tard lorsque les dirigeants
israéliens ont découvert que le Hamas était dans l'opposition absolue à
l'idée d'État palestinien l'État palestinien ça veut dire un accord avec
Israël or ils sont totalement anti-israéliens ils sont pour la destruction
de l'État d'Israël et bien la droite a commencé par Ariel Charon en 2005
a une grande idée je vais évacuer toutes les colonies israéliennes de
Gaza 7000 colons on on va tout détruire on va laisser Gaza au Hamas c'est
ce qui a été fait en 2005 en 2006 élection palestinienne euh Charon a eu
un AVC c'est son successeur qui s'en est occupé les responsables sont allés
voir le Premier ministre et OL mert et lui ont dit attention il va y avoir des
élections palestiniennes législatives si on autorise le ramas à se présenter
il y a de fortes chances pour que le ramas les gagne hors le ramas et hors les
élections se déroulent dans le cadre du processus d'Oslo qui doit conduire
à un état palestinien ce contre quoi se bat le ramas c'était faire rentrer
le lion le loup dans la bergerie et alors Monsieur le Premier Ministre alors on
autorise ou pas le ramas a présenté des candidats la réponse a été oui on
présente des candidats l'année suivante 2007 euh le Hamas prend le contrôle
par la force de la la bande de Gaza vire les autorités des desadministration
de l'autorité autonome dirigée à l'époque par le successeur d'Arafat qui
était Mahmoud Abbas et tue 150 militants du Fatar en blesse plusieurs centaines
expulse de nombreuses personnalités du Fatar et de l'OLP en emprisonne d'autres
ça y est le ramas est en contrôle total de Gaza Benjamin netanahou arrive
au pouvoir retourne au voir en 2009 il décide tout simplement et ben c'est
bien ça le ramas à Gaza ça nous empêche ça empêche la création d'un
État palestinien Benjamin netaniaou le dira et le répétera se y a un seul
problème pour que le Hamas puisse gérer Gaza il faut lui leur donner les
moyens les moyens c'est-à-dire des dollars et il y a un accord avec le les
Mirat du Qatar pour que le Qatar tous les mois de différentes manières
Vienne déposer des millions de dollars en cash à Gaza Benjamin netanahou
a donc autorisé le financement du Hamas par ailleurs il n'a pas laissé les
organisations internationales notamment américaines qui lutte contre l'argent du
terrorisme de s'occuper de qui d'autre finance le Hamas donc l'Iran en parallèle
a également financer le Hamas ce qui a permis à cette organisation islamiste
de mettre remplace que les Israéliens ont découvert le 7 octobre dernier
une armée de 30000 hommes équipé de dizaines de milliers de roquettes et de
missiles antichar et de commettre bah ce qui est arrivé le 7 octobre donc pour
moi le responsable de ce qui est arrivé n'est pas seulement Ariel Charon mais
celui qui a poursuivi cette politique et autorisé pendant toutes ces années le
financement du Hamas Benjamin netanahou d'ailleurs publiquement l'a dit à des
militants à des députés du Liou il faut autoriser le financement du ramas
pour empêcher la création de l'État palestinien bah c'est vrai que pendant
enfin c'est un peu l'ennemi idéal aussi c'est enfin dans la Charte il a les
protocoles du sage de Sion il y a carrément enfin carrément des éléments
nazis dans la charte du Hamas alors alors que l'ancienne résistance enfin
c'était plutôt c'était plutôt gauche laïque donc un adversaire un peu plus
chiant on va dire qu' avait plus un peu plus de soutien international déjà
c'est vra que c'était un ennemi un peu plus contraignant quoi le le Hamas
et les gens n'ont pas vraiment compris c'est une organisation fondamentaliste
comme les daesh et comme les Al-Qaïda ce fondamentalisme islamite chacun à
ses caractéristiques euh et ils se combattent entre eux euh daesh rejeté le
Hamas maintenant ces temps-ci sur les réseaux euh islamistes daesh reconnaît
le Hamas parce que le Hamas avait accepté des cesser le feu avec Israël à
certaines époques là maintenant aujourd'hui le DA dit c'est très bien le
ramas mais vous savez entre même entre DAEs et Al-Qaïda on a eu des combats
donc c'est fondamentaliste c'est pas seulement antijuif c'est antichrétien il
a des déclarations dans ce genre c'est l'intégrisme euh la grande question
pour moi des des cerveaux européens qui ont été élevés dans un milieu
pragmatique cartésien on a du mal à comprendre parfois l'ampleur du du
de l'intégrisme religieux des ce sont des gens qui sont prêts à mourir
qui sont qui utiliseront tout pour faire avancer leur vision de la religion
c'est-à-dire queusi j'en avais parlé aussi avec singer à par rapport du livre
de jean-bierre Baum un silence religieux la gauche face au djihadisme c'est vrai
que dans dans les sociétés occidentales on a du mal à à imaginer dans dans
les société sécularisée que la religion et la croyance peut être un moteur
quelque part de contestation VO même de rassemblement on a vraiment on a on a
un peu enfin on a un peu de mal à s'en souvenir alors c'est pas ACI ancien que
ça la séparation en France de l'Église et de l'État mais on a du mal à se
souvenir effectivement on a beaucoup de mal par rapport à ça quoi écoutez
en miroir vous avez le messianisme juif autant pour le ramas c'est un refus
absolu d'accepter un État juif en terre d'Islam pour leohamas d'ailleurs la
Palestine ce n'est que le faubourg d'alaxa don au centre de cette région il y a
le troisème lieu saint de l'islam àxa le monde du temple pour les Juifs pour
les messianiques Juifs euh il n'est pas question d'accepter la création d'un
état d'un nouvel État arabe d'un État palestinien en en en Terre d'Israël
et il n a pas d'alaaka il y a le monde du temple il va falloir reconstruire le
le temple juif donc nous sommes euh sur une ligne de crête vers une guerre de
religion les les aujourd'hui là les les les 2 tiers du gouvernement israélien
sont religieux messianiques c'est c'est vrai c'est on efface de qui veulent
vraiment la mort de l'un et de l'autre il y a pas de discussion possible c'est
il y a enfin c'est la victoire ou la mort je crois mine de rien vous avez syer
les trois articles que vous voulez aborder c'était Israël favorisé le Hamas
vous venez de le dire l'erreur stratégique d'Israël vous venez aussi vous
avez vous l'avez plus ou moins évoqué donc je je vous propose qu'on passe
à votre livre sur le aux éditions Le Seuil qu'est-ce que vous en pensez
comme vous voulez ok très bien donc euh comme enfin votre petit livre que
je mettrai dans la description de la vi de l'entretien c'est Israël l'agonie
d'une démocratie enin je sais que ce ce ce petit livre est s en même temps
que la réédition euh d' nom du temple mais qu'est-ce qui a spécifiquement
euh motiver l'écriture ce livre enfin qu'est-ce que ce livre dit en plus que
votre livre au nom du temp ne dit pas ou peut-être à manière un peu annexe
avec mon éditeur du seuil nous avons décidé au début de l'année 2023 de
faire un petit bilan alors qu'en parallèle effectivement il fallait mettre à
jour le livre sur le messianisme juif qui avait paru en 2013 et depuis en 2013
le sous-titre c'était Israël et l'irrésistible ascension du messianisme
juif et l'idée c'était de rééditer ce bouquin en l'augmentant et cette
fois avec le sous-titre Israël est l'arrivée au pouvoir des messianiques
Juifs donc l'histoire c'est C ce développement de la religion intégriste
juif messianique euh avec euh expliquant l'aspect politique d'où ça vient
comment Benjamin netanahou a priori non religieux pragmatique issu euh d'une
branche nationaliste du sionisme proche de Jabotinski comment on est arrivé
à cette alliance à ce gouvernement qui a été formé à la fin décembre
2022 euh en cooptant les colons les plus radicaux avec une alliance également
avec les ultraorthodoxes qui normalement jusqu'à maintenant ne nous ne nous
semblait pas très prolon ou très annexionistes comment tout tout ça s fait
et du point de vue idéologique d'où cela vient souvent le problème dans les
des analystes qui ne soit journalistes ou pollyitologue en Europe ne comprennent
pas la profondeur de l'idéologie cette idéologie c'est la vision de netanahou
netaniaou est dans l'idé de son père son père dans les années 20 et 30
était proche de Jabotinski c'est un sionisme nationaliste mais un sionisme
qui contient quand même tous les éléments d'une démocratie Jabotinski qui
était opposé au socialiste au travailliste benourion qui était à l'époque
tout à fait majoritaire dans le mouvement sioniste lui il était totalement
opposé aux grandes idées de gauche de l'époque mais nationaliste et euh
démocrate il était en faveur d'aboutinski d'un État palestinien dont le
président serit juouif et le vice-président arabe et les deux communautés à
égalité mais au sein de ce mouvement créé par joboutinski que l'on appelle le
mouvement révisionniste parce que c'était à l'époque considéré comme une
révision du siionisme donc parmi les révisionistes on avait aussi un extrême
euh toute une toute une équipe qui était ultra nationaliste souvent religieux
totalement opposé à tout accord avec des Arabes refusait par exemple d'accorder
des droits au sein d'un État juif à des Arabes ce qui est intéréressant
c'est que si on revient à la vision de l'État juif tel que le fondateur du
sionisme politique envisageit Théodor herzel Théodor herzel avait écrit un
espèce de roman utopique comment vivrait l'État juif qu'il voulait créer
et raconte l'histoire d'une élection dans cet État juif c'est à la fin du
du 19e il y a un candidat qui s'appelle le Rabin gaayer qui est totalement
nationaliste refuse d'accorder des droits à tout citoyen n'étant pas juif
les non juuifs n'auraient pas de droit dans l'état juif selon gaayer il per
les élections et l'État juif qui est finalement se fait on a a un président
juif un vice-président arabe c'était la vision de herzel qui est également il
le raconte dans ce roman qui s'appelait al neyand vieux nouveau pays euh il il
condamne complètement l'idée de messianisme on ne crée pas l'état juif dans
le cadre d'un grand mouvement escatologique et puis on ne va pas nous amener
des des nouveaux des faux messies comme il y en a eu dans l'histoire juive à
plus d'une période donc euh tout ça était condamné par thodor herzel qui
à mon avis voyant ce qui se passe il doit à l'heure actuelle se retourner
dans cette fromombe donc le père de Benjamin netaniaahou Benon netaniaahou un
historien euh était dans cette Bran du côté de cette branche des des plus
extrêmes qui refusaient tout accord avec les Arabes d'ailleurs quand Benjamin
Natano est arrivé au pouvoir en 96 et qu'il a été était obligé de faire
des concessions parce que il y avait le président américain Bill Clinton qui
faisait pression parce que l'opinion publique à l'époque ne voulait pas tout
casser tout de suite euh son père derrière surveillait de très près pour
vérifier qu'il ne fasse pas de pro de concession tout cela je l'ai raconté
dans les livres que je l'ai publié à l'époque donc netanahou est dans dans
non pas dans la branche du siionisme de de BENOR d'ailleurs qu' qui lui est
arrivé de critiquer ni dans celle principale de Jabotinski mais celle des plus
extrêmes au sein du mouvement révisionniste totalement anti-arabe d'ailleurs
dans des interviews de benion netanahou dans les années 90 et début 2000 euh
il netania le benion netanou le dit il ne faut pas faire de concession aux
Arabes les Palestiniens ont été créés uniquement pour détruire l'état
d'Israël Benjamin netaniaahou a publié un livre en anglais en 1993 où euh
il définit son sa vision un 2è en 95 et il dans la droite ligne de ce que
son père disait les Palestiniens n'existent pas ce sont des arabes qui sont
installés ici et qui euh qui qui qui en fait sont envoyés par le monde arabe
qui veut détruire l'état d'Israël c'est c'est c'est du netaniaou en 2017
betalel motrc un colon radical qui habite près de naplous a publié son plan
de pour vaincre pour qu'Israël gagne cette guerre face aux Arabes et j'ai
retrouvé je le raconte dans le livre que vous avez cité le libel Israel
laagonian en agonie euh on a des phrases qui ressemblent tout à fait à celle
de netanahou netanahou au fil des ans a fait preuve à certaines époques de
pragmatisme au moins en apparence pour à chaque fois éviter la crise majeure
avec les États-Unis avec les voisins et cetera aujourd'hui ce gouvernement
j'ai comparé le plan de smotrich anti-arabe et ce que netano écrivait 93 95
on trouve des phrases similaires c'est le même homme et je suis très content
d'avoir pu trouver cela parce qu'à plus d'une reprise je vais affire à des
diplomates à des ambassadeurs qui nous disaient mais netaniaahou est l'adulte
responsable netanahou a une vision et il va jusqu'au bout et je crois que s'il
se passe maintenant c'est tout à fait le cas netanahou a en 2019 fait adopter
une loi définissant Israël comme l'état naation du peuple juif dans lequel
seuls les Juifs ont des droits communautaires les non juifs sont discriminés
l'arabe n'a perdu son statut de langue officielle les localité israélienne
en Israël je parle encore même pas en territoire palestinen les localités
israéliennes on la la précéance par rapport au aux localités non juives
les juifs contre nonjuif donc c'est ça euh voilà et aussi enfin peut-être
aussi préciser pour le père de namiaou qu'il avait enfin que sa vision de
l'histoire était extrêmement pessimiste et pour lui entre guillemets
l'histoire juif enfin tout le monde voulait génessier les Juifs à travers
l'histoire c'est une vision très pessimiste et aussi en lien avec ludouché
parce que pourquoi pas pas lui directement mais il a été dans les années
20 et 30 rédacteur en chef de toute petite revue euh nationaliste proche
de du mouvement révisionniste dans lequel notamment des des intellectuels
par ailleurs brillant mais très à droite ont publié euh des articles des
opinions tout à fait en faveur d'accord avec Mussolini il y a eu d'ailleurs
Jabotinski qui était le patron du du parti a laissé faire jusqu'à ce que fin
des années 30 Mussolini a mis en place des des mesures antisémites et euh
les les militants à l'époque du Béard du parti révisionniste ont quitté
ont arrêté de de militer en Italie et aussi pour reprendre par rapport à
la loi du Douch enfin du dou et cetera les liens un peu distant mais commeême
plutôt réel bah bizarrement bon ils ont une certaine idéologie qui semble se
comment dirais-je se se regrouper mais aussi euh au-delà du côté messianisme
aussi il y a euh il pointe aussi une espèce de bouquimissaire qui est la gauche
aussi la gauche selon lui l'ennemi aussi un peu ultime quoi netaniaaho dans son
bouquin en 94 et en 95 raconte que le peuple juif a une maligne elle vient d'un
virus qui l' attrapé euh au début des années 2000 et qui vient de Russie ça
c'est appelle le virus du socialisme ou du communisme qui fait croire aux gens
qui ont cette maladie par exemple que les Arabes de Palestine les Arabes de
terre d'Israël comme il les appelle ont droit à l'autodétermination ce qui
est évidemment pour pour lui inacceptable inadmissible et ce qui d'ailleurs
risque de tout simplement de conduire à la destruction de l'État d'Israël
ça c'est la c'est la vision netanahou euh son retour au pouvoir en 2009 dès
2011-212 il a commencé à mettre en place des des textes de loi très antiong
c'est toute une histoire la presse française les correspondants ne l'ont pas
suivi ne l'ont trè que très peu couvert euh des associations de défense
des droits de l'homme comme bselem qui défend les droits de l'homme dans les
territoires occupés sont considérés comme des ennemis euh les vétérans euh
pour la paix breaking the silence contre le silence les soldats qui sortent des
territoires occupés et viennent témoigner après leur servic militaire sont
considérés comme des traîtres et des ennemis tout cela a été accompagné de
de véritables campagne de diffamation et parfois extrêmement violente et ça
continue il y a une période où par exemple quand un diplomate américain voir
un ministre des Affaires étrangères venait en visite et rencontrit d'abord
un représentant de C de ces ONG netaniaah refusait de les rencontrer alors
il y a une loi interdisant le boycot des colonies si un artiste israélien
refuse d'aller chanter à Kiriat TBA ou si une pièce de théâtre les acteurs
refusent d'aller jouer dans une colonie il risque des amendes et des poursuites
judiciaires donc ça ça a été mis en place il y a une véritable répression
en ce moment avec bah l'arrivée dans le dans ce gouvernement des des des
plus radicaux le ministre chargé de la police et titamar bengvir qui lui est
l'héritier idéologique du rabat raciste meirkanana si vos amis ne savent pas
qui c'est vos faire Google maisirkaana c'est un rabin raciste qui qui a créé
la Ligue de Défense Juive interdite aux États-Unis mais pas en France donc
lui poursuit de la répression également euh pas dans selon des procédures
judiciaires mais des des de la répression policière notamment de manifestant
pour la démocratie maintenant c'est c'est c'estfin c'est c'est à chaque fois
c'est on avait j'avais accui sur cette chaîne den Charby pour un peu nuancer
sortir un petit peu du fantasme de la société israélienne mais à enfin Moné
c'est vrai qu'on vit dans même s'il est très CR enfin den Charby est très
critique par rapport à netamiaahou et au gouvernement israélien mais c'est
vrai qu'il avait lui-même noté que les voilà pourtant c'est pas quelqu'un
de très radical mais même lui-même avait noté une dérive vers une forme
de de religiosité extrême et là ce quefinin en lisant votre votre petit
livre sur le Douch sur la R pression concrète et cetera c'est vrai que c'est
ça me fait penser au comment dire aux quelques interviews de de l'exérience
diernal je sais pas si vous si vous vous souvenez de quel déclaration qu'il
avait eu comme quoi la société israélienne est entrée dans un processus
préfasciste aussi bah écoutez Zè était un grand copain euh qui habitait pas
loin d'ici euh je vous ai envoyer le film que j'ai fais sur au nom du Temple
le messianisme et Z sternel est interviewé et il déclare on ne l'a pas vu
venir on n pas compris les Barour Goldstein le terroriste juif qui en début
94 a massacré 29 musulmans en prière un jour de Ramadan dans le Caveau des
Patriarches àébron euh terroriste juif cananiste amire nationaliste religieux
messianique l'assassin d'itrak Rabine et tous ces militants annexionistes qui
développent la colonisation autant que possible sur le compte des Palestiniens
znel a dit on a cru que c'était pas très important qu'on avait la grippe en
fait on avait le cancer et le cancer de znel il est bien là heureusement je
remercie euh mon ami Denis Charbit de de bien commencer à regarder les choses
c'est il prend beaucoup de précautions après bon je sais pas je v pas lui
faire un faux procès c'est pas le propos de son livre israëlé paradoxe car
en tout cas un très bon livre pour décrire vraiment la société israélienne
ou en France on a plutôt tendance à diaboliser enfin on a plus à faire à
un régime politique ou un pays avec une politique ou un gouvernement mais la
quessence du mal absolu juste pour ça et son livre sur ça remet un peu de de
raison quoi euh aussiin parort il faudrait que je place Z sternal aussi avec
den charbi qu la première fois qu'enregistrerai avec lui aussi vous parlez
de coup d'Ét identitaire c'est carrément un coup d'état c'est il y a pas de
nuance c'est coup d'État en fait c'est il y a pas de c'est c'est frontal coup
d'État il y a pas de discussion quoi bah il s'agit de transformer le le régime
judiciaire en Israël de placer la Cour suprême qui est l'instance suprême
en Israël la placé sous le contrôle du pouvoir euh la la la justice perd
son indépendance comment ça s'appelle comment vous appelez ça non mais
un coup d'état enfin je pensais plutôt à un coup d'État dans l'armée
mais non ils ont pris le le pouvoir par l'élection et après une fois qu'ils
ont le pouvoir effectivement ils modèlent les les structures démocratiques
en VO justement la séparation de du pouvoir et enfin de l'exécutif et du
judiciaire et cetera donc ok donc c'est euh c'est c'est pas très réjouissant
et donc par rapport à ça euh vous vous avez parlé aussi d'un souschapitre
la schizophrénie morale a part tide je je j'avoue qu'est-ce que vous allez
dire par rapport à ça ce sont ce sont deux choses différentes Vladimir
rabinovic qui sioniste français dans les années 30 40 il ensuite il est
devenu un grand magistrat grand spécialiste du droit de la montagne était au
sain de la de la communauté juive française très active la guerre de 67 et
l'occupation l'a fait basculer il il était très opposé aux conctionniste
de la de la communauté critiquant des gens comme E visel qui avec qui entre
autres j'ai eu quelques quelques discussions un peu vive pour qui Jérusalem
ne pouvait aller qu'aux Juifs ne pouvait pas aller à des arabes on pouvait
pas partager Jérusalem pour ISEL et d'autres euh et rabinovic celui que je
cite a écrit euh c'est une schizophrénie morale que de considérer qu'il
y a une une justice euh démocratique pour les uns mais pas pour les autres
vous comprenez ce que je veux dire oui bah oui c'est il visis VO en plus je
vois qui c'est mais c'est ça me surprends qu'il ait pu avoir ce genre de
parole d'ailleurs c'est ça me surprend quoi oui en après après il c'est
c'est pas un sain mais je on pourrait croire avec son avec son passé dans les
comp de concentration qui serait peut-être un peu plus sensible à l'idée
de partage ou je sais pas euh je je pense pas non malheureusement mais c'est
donc voilà c'est ESP à la fois c'est comment dirais ce côté très moral
très bon sentiment mais à la fois dès qu'on touche à des affects un petit
peu identitair il y a tout de suite quelque chose qui se cris se cris quelque
part c'est ça que vous êtes en train de dire ouais ouais on est enfin même
ISEL mon Dieu et enfin le le dernier souschapitre j'avais prévu 2 he mais
finalement c'est allé assez vite vous avez synthétisé vous vous avez été
en ligne droite ça a été c'est assez assez expéditif mais après pour
pour votre pour les enfin pour le prochain enregistrement avec votre livre
je pense qu'on ça être un peu plus le propos va être un peu plus complexe
en tout cas il y a beaucoup plus matière C lu en ce petit livre il fait à
peine 50 pages il est pas très grand donc voilà je je le mettrai dans la
description de la vidéo de l'entretien et leenfant et de le dernier ce c'est
du National judaïsme au National conservatisme je vous trouve étonnamment
étonnamment un peu l dans la formulation du chapitre comparment à ce que
vous avez dit dans le contenu quoi bah d'abord le judé au nationalisme c'est
pour moi l'idéologie de netanahou hein ben je crois que on est bien d'accord
là-dessus les dernières déclarations s jour ses jours dernier de netaniaahou
c'est nom absolu à l'idée d'État palestinien point c'est non et la politique
anti-gauche je crois qu'on est clair maintenant euh dans les années 90 un juif
américain de droite qui a qui est devenu sioniste euh alors qu'il étudiait
dans une grande université américaine a été convaincu par le fameux rabat
Meir Kanana qu'il devait venir en Israël pour se lancer dans dans le le la
défense de du pays euh face aux Arabes il s'appelle mais razoni il arrive
il va s'installer dans une colonie avec d'autres amis juifs américains euh
il est assez brillant il travaille à l'époque pour un canard en anglais qui
s'appelle Jérusalem poste qui est plutôt à droite et il devient l'assistant
de netanahou quiilaide à éditer ses bouquins euh rasoni au fil des ans euh
est néoclon néoconservateur et il développe son idéologie critiquant les
grands sionistes laïqu séculiers les les écrivains euh les écrivains
comme David Grossmann amamos sont sont qualifiés pour lui d'antiioniste
parce que ils acceptent l'existence de l'autre et euh rasoni a publié il y a
3 ans 3 ans et demi un bouquin qui fait l'apologie du nationalisme euh et il
développe avec des néoconservateurs américains la notion de nationalisme
et de conservatisme on met les deux ensemble et il crée à Washington une
fondation qui s'appelle la Fondation Burk alors vous qui bien entendu êtes
des grands historiens qui connaissent l'hoire de la République française et
de l'opposition en Europe à la République française à la fin du 18e le
grand ennemi des droits de l'homme de la République française c'était un
député britannique qui s'appelait Burk buke qui est la grand le grand critique
de du droit universel des droits de l'homme pour lui fondamament allement ça
n'arrive pas comme ça tout seul il y a la ligée il y a la descendance il y
a voilà c'est autre chose et razoni dans sa vision du National conservatisme
a donc créé cette fondation burquet à Washington alors on imagine un juif
qui quand même il y a eu le le la la libération des Juifs par la par la
République française les Juifs sont devenus citoyens à part entière dans le
cadre de la Déclaration des droits de l'homme par la République française et
maintenant en un juif israelloaméricain qui crée la fondation burket qui est
contre la Révolution française maintenant le National conservatisme et là
je lance l'alerte vous devez regarder chercher il y a des grandes réunions
de national conservatisme il y a Orban le Premier ministre hongrois il y a
l'ancien Premier ministre ministre polonais il y a tout l'entourage de il
y a tout l'entourage de Trump il y a de CTIS le gouverneur de de la Floride
et puis on y retrouve également Mario maréchal on y retrouve tout tout ça
c'est le National conservatisme alors vous voulez vous battre vous voulez vous
battre pour la démocratie regardez les idéologies en Israël une idéologie
nationaliste messianique qui est aujourd'hui au pouvoir et qui casse casse
la la la la République ce qui restait encore de républicain en Israël de
démocratie et euh d'ailleurs entre parenthèses les grandes manifestations
prodémocratie commencent à se réveiller oui c'est les réservistes voilà
ça recommence grâce à Dieu j'ai j'ai vu que j'ai vu que vous avez relié
pas mal il y avait pas mal de manifestations en Israël anti antinitamiaou
justement c'est vous avez relié pas mal ceci et ça serait bien aussi qu'on
parle de en France d'Israël mis à part pour parler de nettoyage ethnique et il
faudrait peut-être un un peu plus parler d'Israël quand il y a des mouvements
sociaux intéressants surtout quand c'est pour contrer netamiaahou quoi euh
c'est écoutez là on est dans une situation un peu différente d'abord parce
qu'on est encore en état de guerre euh toutes les localités autour de Gaza
ont été évacués les gens vivent encore dans des hôtels un peu partout
dans le pays ça représente à peu près 60 à 80000 habitants autour de de
Gaza maintenant euh quand vous êtes à côté continuez à recevoir des obus
ou des roquettes de temps en temps les gens qui n'ont pas été évacués
sur décision gouvernementales sont également aller déménager ailleurs
c'est la même chose dans le nord le long de la frontière libanaise où les
bombardement entre Israël et leur hzbolla sont très importants plus de 500
maisons israéliennes ont été détruites par des tir du rzbola je ne connais
pas les destructions côté libanais mais c'est c'est c'est très important aussi
s'est évacué ça ne s'est jamais arrivé dans l'histoire d'Israël jamais
un kibuts comme Manara qui est sur la frontière libanaise n'a été évacué
là il y a seulement une section d'alerte quelques quelques militaires avec
leurs armes c'est tout à Manara donc euh on a l'énorme problème social avec
une crise économique qui arrive maintenant on sait pas où ça va mais les le
mouvement politique antietanahou se réveille enfin ouais c'est c'est-à-dire
aussi je c'est peut-être les deux pointsù je voudrais un peu finir sur cet
entretien c'est justement parce que là enfin juste durant l'entretien j'avais
fait le lien entre le le père enfin le le grand un lien de la famille de de
netamiaahou et le douché euh personnellement c'est un c'est le parallèle juif
est égal nazisme ou est égal fascisme c'est un parallèle que je ne feraai
pas c'est parce que c'est un parallèle qui est beaucoup trop dendancieux et
le terrain est sacrément glissant donc moi personnellement je l'utilise pas
pas vraiment et euh parce que bon dire que Israël est dégal nazisme h c'est
vu vu le poids de l'histoire c'est moi personnellement je je le tente pas tout
tout toute personne tout de suite qui me parle de nazi je le bloque tout simple
une partie de ma famille est passée au travers du côté de la famille de ma
mère juif autrichien réfugié à Nancy ils ont ils ont été sauvés par la
police étrangers de Nancy mais toute la famille du côté de ma grand-mère
et de mon grand-père sont morts dans les camps voilà donc c'est un parallèle
que je qualifie c'est un c'est une imagerie que je qualifie d'antisémite pour
moi il y a pas de discussion à avoir et surtout c'est je voudrais surtout
j'ai invité encore Denis Charby pour en parler vu qu'il a écrit un livre
sur le sionisme là vous avez on vous avez commencé les origines politiques
de idéologiqu de netamiaahou déjà vous avez parlé que déjà à l'époque
déjà que le sionisme que le mouvement révisionniste était déjà minoritaire
dans mouioniste et au sein de ce mouvement révisionniste il y avait euh le le
le parent desamou donc c'est une minorité dans la minorité parce qu'en France
on a une certaine tendance à dire que sioniste est égal à partê des égal
juif messianique alors apartide apartide euh c'est une politique et je cite
les responsables politiques israéliens qui ont dit attention si on ne fait
pas d'accord avec les Palestiniens on va glisser dans une forme d'apartide
ça va Deah Rabine une fois la a dit attention si on fait pas d'accord avec
les PES on va aller avec vers une forme d'apartide on a eu d'autres également
ils sont tous cités dans mon libel donc on ne peut pas dire aujourd'hui qu'il
sera l' un état apartiide mais en tout cas si vu la politique actuelle raciste
notamment des des colons radicaux au gouvernement Israël glisse vers un état
apartide c'est-à-dire on risque dans les territoires occupés carrément
je pense que le mot n'est pas usurpé d'ailleurs oui mais dans la société
israélienne pas tellement mais dans les territoires occupés comme la C Jordanie
là je pense que le mot n'est pas déconnant quoi la société israélienne
du train où vend les choses risque d'arriver c'est une question de temps on
est bien d'accord je pense que je voulais éclairer ces deux points parce que
je me suis dit tiens le doucher c'est peut-être une MTE un peu glissante par
rapport au lien avec le nazisme et cetera donc je voulais mettre les choses
au clair et aussi vraiment rappeler que le sionisme par déjà par nature
il est pluriel et des et que le sionisme était pas é égal racisme est
égal à partê c'est le point de base que je voulais rappeler parce que qu'
on critique Israël on est enfin je sais qu'il y a des gens qui vont je sais
que c'est entendé beaucoup plus de vues que M entendé avec Denis Charby donc
c'est un peu pour Histor poser un petit peu voilà un petit peu les limites
de les cadres du truc parce que la critique d'Israël c'est une critique qui
doit être à minimum un peu fine et un peu subtile donc à Moné voilà c'est
juste rappeler C ces deux choses donc voilà je pense que vous avez le mot de
la fin monsieur Derlin à le mot de la fin peut-être bah écoutez croisons les
doigts et espérons que euh cette guerre ce pays va réussir à se redresser
car la crise actuelle ce n'est pas seulement une crise israélienne une crise du
sionise mais aussi une crise du judaïme le messianisme qui aujourd'hui est au
pouvoir en Israël est extrêmement dangereux pas seulement pour le pays mais
pour moi pour l'ensemble des des communautés juives h oui parce que c'est
c'est enfin le missamisme just distingue le mauvais juif du bon juif c'est
un peu comme les les groupes djihadistes qui distinguent le bon musulman du
maua musulman enfin bref c'est c'est c'est un peu compliqué donc enfin bref
et puis de toute façon on va se revoir pour parler de votre livre sur les
messianismes les messianiques au pouvoir de toute manière qui est un livre
que j'ai commencé et qui est filrement intéressant c'est euh enfin enfin je
trouve que c'est intéressant et ça ça claire que pas mal de points et et
on aimerait vous entendre tê un peu plus souvent que sur une chaîne YouTube
à 2000 abonnéson moi je suis content de vous accueillir mais c'est vrai que
ça ser c'est dommage que votre vous entende pas plus souvent ailleurs on va
dire enfin bref c'était martinen et Charl directec bisous à vous


Liens
========

- https://www.seuil.com/ouvrage/israel-l-agonie-d-une-democratie-charles-enderlin/9782021539158
- https://www.lemonde.fr/idees/article/2006/02/03/quand-israel-favorisait-le-hamas-par-charles-enderlin_737642_3232.html
- https://www.parismatch.com/actu/international/hamas-les-origines-du-mal-230384
- https://www.monde-diplomatique.fr/2024/01/ENDERLIN/66457
- https://www.lemonde.fr/idees/article/2018/02/18/zeev-sternhell-en-israel-pousse-un-racisme-proche-du-nazisme-a-ses-debuts_5258673_3232.html
- https://www.youtube.com/playlist?list=PLkVAm1rOEsU0erTKabtfJWbDKxoyCi1h- (Histoire de l'antisémitisme à gauche (avec Michel Dreyfus))
- https://www.youtube.com/watch?v=j1I5CUhBzTQ (Misère de l'antisionisme (avec Ivan Segré))
- https://www.youtube.com/watch?v=eH-mYHUbZEc (Que sais-je ? L'antisémitisme (avec Juifves révolutionnaires))
