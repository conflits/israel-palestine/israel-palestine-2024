.. index::
   pair: Hommage ; 41 morts frnaçis

.. _hommage_2024_02_07:

==================================================================
2024-02-07 **Hommage aux 41 morts francais du 7 octobre 2023**
==================================================================


UEJF
====

- https://nitter.cz/uejf/status/1747386583601271029#m

Oui. Le 7/10, c’est aussi une attaque contre la France, ses valeurs, et
les français, dont 41 ont été assassinés et 3 sont encore otages du Hamas à Gaza

L’hommage national annoncé par @EmmanuelMacron était attendu et nécessaire.
Nous le saluons.

1/2


- https://nitter.cz/uejf/status/1747386588076572853#m

Nous déplorons en revanche qu’il n’ait lieu que 4 mois après l’attaque,
rappelant combien le simple fait de rendre hommage à ces morts n’est
pas naturel.

Ce n’est ainsi pas seulement « ceux qui le veulent » qui devront y participer
mais, enfin, toute la Nation réunie.

2/2

Yonathan Arfi
===============

- http://nitter.moomoo.me/Yonathan_Arfi/status/1747375071377736142#m

Le 7 Octobre est aussi une tragédie française.

Avec 41 victimes françaises, c'est le bilan le plus meurtrier depuis
l'attentat de Nice.

L'hommage national aux 41 Français assassinés qui sera rendu le 7 février 2024
est une étape nécessaire pour qu'ils trouvent pleinement leur place dans
la Mémoire nationale.
