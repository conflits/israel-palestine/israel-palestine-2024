.. index::
   pair: CNT-AIT ; Tal Mitnick de nouveau emprisonné. Deux nouveaux objecteurs refusent de rejoindre l’armée israélienne (2024-04-17)

.. _cnt_ait_2024_04_17:

=====================================================================================================================================
2024-04-17 **Tal Mitnick de nouveau emprisonné. Deux nouveaux objecteurs refusent de rejoindre l’armée israélienne** par CNT-AIT
=====================================================================================================================================

- http://cnt-ait.info/2024/04/17/tal-mitnick-de-nouveau-emprisonne-deux-nouveaux-objecteurs-refusent-de-rejoindre-larmee-israelienne/


L’association Mesarvot (מסרבות ) de solidarité avec les objecteurs israélien
=================================================================================

- https://linktr.ee/Meaarvot
- http://cnt-ait.info/2024/02/16/tal-mitnick

Ce mercredi 17 avril 2024, `l’association Mesarvot <- https://linktr.ee/Meaarvot>`_ (מסרבות ) de solidarité avec
les objecteurs israélien a annoncé que Tal Mitnik venait d’être condamné
à 45 jours supplémentaires de prison militaire et à 15 jours supplémentaires
de probation pour son refus de s’enrôler.

C’est la quatrième fois que Tal est jugé pour son refus de s’enrôler dans
l’armée israélienne.
Jusqu’à présent, Tal a purgé une peine de prison cumulée de 105 jours.
(cf. Israël : Deuxième peine de prison pour l’objecteur de conscience Tal Mitnick
http://cnt-ait.info/2024/02/16/tal-mitnick)

Avant de retourner en prison, il a pu participer à un débat organisé par Mesarvot
(https://linktr.ee/Meaarvot) où il a pu répondre à une question sur la façon
dont il continue dans une telle période :

Question : Considérant que tous les activistes de gauche sont déprimés, que la situation est merdique, et continue d’être merdique, comment vas-tu, Tal ?
=============================================================================================================================================================

Tal Mitnick
---------------

J’ai déjà répondu à cette question, mais disons, comme chaque personne qui voit,
qui lit et qui peut-être est aussi influencée [par cela], cela me fait penser
que même si nous ne sommes pas en train de gagner, en aucune manière, il y a
quand même une sorte de changement.

Ce que nous sommes en train de faire est absolument important, même si cela
n’arrête pas la guerre ni l’occupation.
Mais il y a quand même une chance que cela y participe.

**L’espoir est dans l’Action ! Voilà ce que c’est**.

Voir des gens qui sortent pour manifester chaque semaine [pour le cessez-le-feu],
voir qu’il y a des gens qui font attention à voir le progrès, même s’il est
mineur, de la normalisation de parler d’une solution politique, voir que dans les
énormes manifestations pour le retour des otages, dans le même temps, un grand
nombre des participants appelle à un cessez-le-feu en échange des otages.

Je pense que nous devrions toujours avoir l’espoir, **et je vois réellement cet
endroit devenir meilleur dans les années à venir, même si actuellement c’est
vraiment très merdique ici.**


Après Tal Mitnick, deux autres jeunes israéliens, la jeune Sofia Orr (en Israël
les femmes sont aussi soumises au service militaire) et Ben Arad ont annoncé
leur refus de s’enrôler pour protester publiquement contre la guerre à Gaza


Ben Arad
===============

Ben Arad a été condamné le 1er avril 2024 à 20 jours de prison militaire, une
peine qui devrait être prolongée s’il refuse à nouveau de s’enrôler.

Ben Arad a décidé de refuser en raison de la guerre à Gaza. Il a noté que le
refus de Tal Mitnick lui a montré qu’il était utile de refuser publiquement.


.. figure:: images/ben.webp

Voici sa déclaration de refus :

Je m’appelle Ben Arad, j’ai 18 ans et je refuse de m’enrôler dans les forces
de défense israéliennes.
**Je m’oppose aux tueries absurdes, à la politique de famine et de maladie
intentionnelle, et au sacrifice de soldats, de civils et d’otages pour une
guerre qui ne peut pas atteindre et n’atteindra pas ses objectifs déclarés
et qui pourrait dégénérer en une guerre régionale**.

**Pour ces raisons et d’autres encore, je refuse de m’engager.**

Je ne participerai pas à une guerre de vengeance, qui ne mène qu’à la destruction
et n’apportera pas la sécurité aux citoyens d’Israël.

Si vous n’avez qu’un marteau, tout ressemble à un clou."
-----------------------------------------------------------

Je pense constamment à cette phrase lorsque je considère le comportement d’Israël
depuis le début de la guerre.
Le seul outil que nous connaissons est l’outil militaire.
Par conséquent, la solution à chaque problème doit être militaire.

Mais notre stratégie de dissuasion ne fait pas ses preuves. Le terrorisme ne peut
être arrêté par des menaces, car les terroristes n’ont pas grand-chose à
perdre.

En outre, le massacre sans précédent de citoyens non impliqués à Gaza, la faim,
la maladie et la destruction de biens ne font qu’alimenter la flamme de haine
et de terreur du Hamas, et **tôt ou tard, nous paierons pour l’agonie des Palestiniens**.

Le 7 octobre 2023 Israël s’est réveillé avec une attaque brutale qu’il
n’avait jamais vue auparavant. Des enfants, des femmes et des personnes âgées
ont été victimes d’atrocités auxquelles personne ne devrait être exposé.
La barbarie et l’inhumanité de l’attaque étaient censées éradiquer tout espoir
de paix et d’avenir commun.
L’impact du 7 octobre sur le peuple israélien est encore immense, d’autant
plus que plus de 130 otages sont toujours retenus dans la bande de Gaza.

Depuis ce samedi, Israël mène une campagne meurtrière sans précédent, non
seulement contre le Hamas, mais aussi contre l’ensemble du peuple palestinien.

À Gaza, on dénombre au moins 30.000 morts, dont 70 % seraient des femmes et des
enfants.

Chaque jour, les responsables israéliens menacent de lancer une offensive
terrestre à Rafah, où plus de 1,5 million de Palestiniens évacués ont trouvé
refuge. L’entrée d’Israël dans Rafah coûtera la vie à des dizaines ou des
centaines de soldats israéliens et à des milliers ou des dizaines de milliers de
Palestiniens. Elle mettra en danger la vie des otages et entraînera une escalade
significative des combats avec le Hezbollah au Liban.

Et pour quoi faire ?
Qu’est-ce que ces combats réalisent ?

**Les combats ne ramèneront pas les otages. Ils ne ressusciteront pas les morts.
Ils ne libéreront pas les Gazaouis du Hamas et n’apporteront pas la paix**.

Au contraire, les combats continueront à tuer des otages, à menacer davantage
de Juifs et de Palestiniens, à perpétuer le règne des organisations terroristes
à Gaza et à garantir qu’il n’y aura pas d’horizon pacifique.

L’opinion publique israélienne est confrontée à un choix : maintenons-nous le
cycle actuel de la violence et entretenons-nous une réalité de destruction qui
aggravera la haine et créera une escalade sur tous les fronts ?

**Ou choisissons-nous une autre voie, fondée sur le caractère sacré de la vie,
dans laquelle nous cesserons d’envoyer de magnifiques êtres humains se faire
tuer ou blesser dans d’horribles batailles ?**

Pourrions-nous garantir le retour de tous les otages vivants, arrêter les
tueries insensées à Gaza, condamner la violence des colons en Cisjordanie
et empêcher le déclenchement d’une nouvelle guerre ?

L’opinion publique, c’est nous
----------------------------------

Nous avons un grand pouvoir que les gouvernements et les organisations corrompues
qui nous représentent n’ont pas.

**C’est donc de nous que doit venir l’impulsion du changement**.

**Nous ne pouvons progresser vers la paix que par le biais d’un mouvement social
sans compromis, engagé dans la communication et la désescalade**.

**Nous devons toujours faire preuve d’esprit critique, regarder la situation
dans son ensemble et lutter pour la paix, l’égalité et la vérité**.
