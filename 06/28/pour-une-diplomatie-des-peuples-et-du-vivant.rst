.. index::
   pair: Sylvaine Bulle ; Pour une diplomatie des peuples et du vivant (2024-06-28)

.. _bulle_2024_06_28:

==========================================================================================
2024-06-28 **Pour une diplomatie des peuples et du vivant** par Sylvaine Bulle |paix|
==========================================================================================

- https://www.youtube.com/playlist?list=PL_icmLCLTKLyVi7k5xeruoQazukpi0t-T
- https://www.youtube.com/watch?v=7FyoZCkqVKs
- :ref:`antisem:sylvaine_bulle`

.. figure:: images/paix_youtube.webp


Contre l'homogénéisation illusoire de la société israélienne d'une part, 
palestinienne d'autre part, indissociable du "campisme" qui fige le débat 
concernant la région, Sylvaine Bulle, sociologue et autrice de 
"Sociologie de Jérusalem" (La Découverte, 2020)  insiste sur la multiplicité 
des origines et des prises de position en Israël Palestine ; elle prône 
une diplomatie du quotidien restaurant le dynamisme de la société civile 
et lui donnant de nouvelles orientations, dont le soin de l'habitat et 
de la terre.


.. youtube:: 7FyoZCkqVKs

   https://www.youtube.com/watch?v=7FyoZCkqVKs


Texte issu de la transcription automatique google  (à mettre en forme)
==============================================================================


Israël Palestine est devenu le drame moral central pour une grande
partie de l'opinion publique avec la même indignation devant le désastre de
Gaza 

Une guerre de position appelée campisme se joue dans tous les milieux
universitaires, militants, médiatiques mais ce sont bien souvent des lectures
simplistes qui enferment les deux sociétés israélo-palestiniennes dans leurs
oppositions éternelles notamment parce que ces deux sociétés sont regardées à
distance, de façon lointaine, la plupart du temps sans connaissance des réalités

c'est la raison pour laquelle on note une atrophie de toutes les sensibilités
autrement dit l'impossibilité d'une empathie réciproque entre les deux camps

Prenons par exemple les positions militantes en soutien à la Palestine venant
du sud global, de l'antiracisme ou de l'anti-impérial elles se consacrent de façon
récurrente à l'explicitation du sionisme comme idéologie et comme fait colonial
sans prise en compte des temporalités et des mondes vécus israélo-palestiniens

Ce geste **idéologique** consiste bien à réduire les formes politique et sociale des
deux sociétés israéliennes et palestiniennes ; ainsi la dénonciation homogène
d'Israël renommé "entité sioniste", "état génocidaire" d'apartheid a pour
but de créer une 	commensurabilité entre entre un gouvernement d'occupation et
l'extermination des Juifs durant la Seconde Guerre mondiale mais également avec
la souffrance du peuple palestinien alors que ces souffrances n'ont pas à être
définies ensemble.

Le terme "d'entité sioniste" utilisé par la critique d'Israël permet aussi 
d'amalgamer toutes sortes de composantes sociales et religieuses de
la société israélienne ; les Israéliens sont vus soit comme des rescapés de la
Shoah soit comme des colons blancs ou descendants de colons et dans tous les cas
sont considérés comme étant dépourvus de lien avec la terre palestinienne 

Ils sont comparés aux afrikaners racistes qui assujétissent des indigènes, ici et
les Arabes, pourtant israël compte pour la moitié de sa population des `misrahim https://fr.wikipedia.org/wiki/Juifs_Mizrahim <https://fr.wikipedia.org/wiki/Juifs_Mizrahim>`_
c'est-à-dire des juifs arabes venant d'Irak, du Yémen, du Maghreb qui ne sont pas
des descendants de la Shoah et qui eux-mêmes revendiquent d'être reconnus dans
leurs différences et dans leur rite 

La déshistoricisation du sionisme oublie aussi que des indigènes juifs ont 
cohabité sur la terre de Palestine depuis le début du 20e siècle au moins 

En effet enfin le terme de résistance tel qu'il est appliqué par les voix 
des militants à la relation aux Palestiniens est lui aussi
souvent essentialisé les Palestiniens sont vus comme des résistants privés de
terre, héroïsés, fétichisés comme les représentants de l'anti-impérialisme
sans distinction de statut, de religion et alors que toute une palette d'êtres et de
positions compose la Palestine tout comme Israël 

C'est le cas des chrétiens des Arméniens des Bédoins et de toutes ceux et 
celles qui souffrent d'une absence de reconnaissance sociale 

Pour résumer l'homogénéisation ne peut pas être une base pour comprendre 
l'organisation des sociétés pas plus que les formes politiques d'un au-delà 
de la guerre 

Le maintien des antagonismes ne permet pas de sortir de la crise politique 
et intellectuelle actuelle parce que de telles positions sont coupées 
des mondes ordinaires et n'offrent d'horizon intellectuel que celui de la 
généralisation 

Il nous faut donc pour envisager un horizon de la démocratie une autre relation 
à la politique et reconnaître la multitude de résistance et d'appartenance 
israélo-palestinienne dans la possibilité d'une coexistence 

Prenons le cas des résistances israéliennes 

En Israël des chercheurs et des activistes depuis deux décennies qualifient 
leur propre espace colonial et décolonial et différentes sources de domination 
appelées intersectionnelle le collectif des `black Panthers israélien <https://fr.wikipedia.org/wiki/Black_Panthers_(Isra%C3%ABl)>`_ (HaPanterim HaSh'horim) est
l'un des premiers à avoir articulé questions sociales et questions politiques
dans les années 70, en pointant l'exigence d'égalité de tous les citoyens y compris Palestiniens 

Dans leur sillage de nombreux mouvements ont émergés qui
se font les ?? des populations orientales d'Afrique du Nord, d'Asie du Sud-Est et
du Moyen-Orient 

De très nombreux chercheurs et militants écrivains décrivent
l'éveil décolonial israéliens en remettant en cause le récit hégémonique
effectué par les pionniers olim Ashkenazim 

On peut également se tourner vers la sociologie des minorités non palestiniennes 
comme les Arméniens engagés dans des luttes urbaines à Jérusalem 

on peut regarder la lutte contre la pauvreté urbaine en Israël 

celle contre l'injustice perçue ou vécu des Orientaux Juifs
irakis yéménis 

regarder aussi la désobéissance de certains jeunes religieux
ultraorthodoxes 

et enfin se tourner vers la possibilité de forme de cohabitation et
d'interaction entre les populations 

Il est donc souhaitable que les histoires multiples, entremêlées ou distinctes 
puissent être une clé de lecture au-delà de la seule logique mortifère de Gaza 
et d'une unique source d'oppression renvoyée à Israël 

de même les résistances à la guerre existent comme la dissidence
des Israéliens, des pilotes, des généraux refusant de servir dans les Forces de
Défense 

les pratiques des pisniks?? en faveur de la paix celle des refuzniks qui sont
des jeunes mobilisés refusant de servir dans les territoires palestiniens 
celles des officiers dénonçant les méthodes de l'armée dans les territoires 
comme ce fut le cas avant le 7 octobre 

Symétriquement ces dissidences existent aussi au sein des Palestiniens comme 
ceux et celles qui transgressent les régions morales et refusent tout nationalisme 

ceux et celles qui quittent les assignations identitaires par
les mariages mixtes ou par l'engagement politique c'est le cas des Palestiniens
de Jérusalem désobéissant aux injonctions patriotes du boycott israélien ou de
ceux qui candidatent aux élections municipales 

de même des Palestiniens n'hésitent pas s'engager dans des mouvements de 
solidarité mixte au côté d'Israéliens, de féministes de cisjordanie 

La société est celle où s'éprouve concrètement et quotidiennement le vivre 
ensemble ; se pose donc une question au-delà de la
guerre et de leur partisans peut-on compter sur des forces démocratiques 

- qui est le "nous" de la résistance 
- qui est le nous de la démocratie 

la possibilité de la paix suppose de trouver le "nous" ou le "commun" et 
de reconnaître par exemple l'association d'être ce liant et préexistant 
à toute préinstitution étatique 

Il y a un siècle le philosophe `Martin Buber <https://fr.wikipedia.org/wiki/Martin_Buber>`_ faisait le pari des interpolations
le pari de petites communautés qui soient capables de générer et regénérer
la politique sans coup d'éclat mais par une reconquête de la communauté et
de la solidarité 

`Martin Buber <https://fr.wikipedia.org/wiki/Martin_Buber>`_ proposait une 
vision de la communauté fondée sur la relation du "je" au "tu" en tant que 
relation de valeur à valeur 

Avec le philosophe Gustave Landauer `Martin Buber <https://fr.wikipedia.org/wiki/Martin_Buber>`_ 
faisait le pari de la terre commune comme espace de la démocratie directe 
et réelle faisait le pari de relier terre et démocratie sur la base de la 
présence de communautés inclusives et non exclusives non pas juives
ou palestiniennes mais juives et palestiniennes 

N'est-ce pas le cas de la situation actuelle ?

La possibilité de la paix repose sur une diplomatie de l'ordinaire des peuples et du vivant 
================================================================================================

Une diplomatie de l'ordinaire qui permet de dépolariser des
affects clivés au profit d'actions collectives, de nouveaux concepts d'existence
possibles et de nouveaux assemblages humains et non humains post-conflit contre le
dogmatisme qui parcourt la lecture du conflit actuel il importe de prendre comme
boussole une géopolitique de l'ordinaire 

Ni un cesser lefeu ni un plan de paix et de nouveaux gouvernants ne suffiront 
à refonder un voisinage entre Israël et Gaza et la Cisjordanie 

Ils ne permettront pas de créer les conditions d'un espace
politique émotionnel pacifié reliant des êtres meurtris et sous le choc de la
cruauté qu'ils exercent de part et d'autre 

Admettons que la diplomatie paternaliste internationale a échoué depuis 
50 ans à faire se rapprocher deux sociétés et ne pourra sans doute pas 
empêcher les violences intercommunautaires et l'hostilité
mutuelle croissante 

Car une fois passé un éventuel défilé diplomatique et l'aide humanitaire 
à Gaza deux données nécessaires et préalables à un cesser le feu les deux 
sociétés se retrouveront à nouveau face à face dans
le ressentiment et la haine 

C'est la raison pour laquelle la situation implique d'abord une séparation 
qui n'empêche pas l'interdépendance. 

La démocratie sur une terre partagée passe par la reconnaissance de deux parties 
dans leurs frontières respectives 

La frontière n'est pas l'équivalent de la fracture elle
est une séparation 

D'autre part ouvrir la voie à de nouveaux dirigeants de part et d'autre sera insuffisant.

Il faut s'acheminer vers la diplomatie des peuples;  seul de nouveaux 
groupes d'expression et d'opinion et d'acteurs sociaux engagés
peuvent donner un sens politique aux sociétés et en particulier à la société
palestinienne afin de combler le vide démocratique actuel 

Celle-ci doit s'organiser pour définir quelles sont ses idéaux de justice 
de reconnaissance de l'autre mais aussi l'habitabilité de son territoire 

Enfin et surtout il faut appeler à une diplomatie de la vie quotidienne 
de la part de citoyens éveillés en faveur de la coexistence. 

Celle-ci a existé durant un siècle à partir de multiples
circulations économiques, intellectuelles ou politique et enfin artistiques 
qui seules peuvent maintenir la commune humanité.

Une diplomatie du quotidien et de l'ordinaire repose sur des citoyens qui 
cherchent à mettre en relation les sociétés civiles entre elles avec 
leur diaspora juive, israéliennes et palestiniennes croyant en
leur destin commun. 

Cet horizon politique de l'interrelation s'étend en particulier
aux questions environnementales qui peut fournir les bases d'un projet réaliste
**ceux de l'habitabilité d'une terre partagée** 

Les projets environnementaux et autour du vivant peuvent donner forme 
facilement à cette **diplomatie du quotidien**, diplomatie environnementale 
avec des échanges d'idées de part et d'autrre comme c'est déjà le cas 
dans le désert commun entre Israël, la Jordanie, l'Égypte.

Au final la cause palestinienne ne peut pas être un prétexte pour l'évitement
de la politique en se focalisant exclusivement sur les exactions alors que les
formes politiques de la coexistence sont négligées tout comme l'avenir de la
démocratie en Israël et en Palestine.

Nous ne libérons pas la Palestine c'est la Palestine qui nous libère 
écrivent les militants ; il faut alors ne pas éviter la
politique ni la possibilité d'assemblages humains y compris avec ses adversaires.

c'est la raison pour laquelle avant tout les Israéliens et les Palestiniens
doivent se libérer eux-mêmes et par eux-mêmes.
