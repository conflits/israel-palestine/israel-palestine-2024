

.. _charbit_2024_06_19:

=====================================================================================================================================================
2024-06-19 **Études du Crif n°66 : 7 octobre 2023 - Fracture(s) - Article de Denis Charbit : le 7 octobre : quel impact ? Quelle empreinte ?**
=====================================================================================================================================================

- https://www.crif.org/fr/content/etudes-du-crif-n66-7-octobre-2023-fractures-article-de-denis-charbit-le-7-octobre-quel-impact-quelle-empreinte

Auteur Denis Charbit
========================

Spécialiste de l’histoire du sionisme et d’Israël, Denis Charbit revient
sur l’évidence avec laquelle l’événement du 7 octobre a immédiatement
"fait date et pourrait faire génération" – pour Israël comme
pour l’ensemble du monde juif. Attentif à la manière dont l’attaque
du Hamas a traumatisé la société israélienne, il nous instruit sur les
interrogations et attentes politiques que celle-ci, avec la guerre à Gaza,
provoquent déjà.  

**Dédicace : À celles et ceux qui se rendent en Israël depuis le 7 octobre 2023**
==============================================================================================

Vous êtes venus au premier signal. Spontanément, instinctivement. Pour
réparer la blessure qui vous a atteint dans votre for intérieur  ; pour
offrir la dignité de votre présence sur les lieux où la dignité des
victimes a été violée  ; pour donner l’amour là où la terreur a
semé la haine et la désolation. Vous avez accompli les devoirs du cœur
avec utilité, humilité, simplicité. Comme tant de nos compatriotes, vous
avez retroussé les manches et travaillé dans les champs, dans les usines
et dans des bases militaires. Vous avez cueilli des fraises ou des avocats,
préparé des plats chauds destinés aux réservistes, lavé et repassé le
linge sale des évacués du nord et du sud qui avaient trouvé refuge dans
une chambre d’hôtel. La sueur qui a coulé de votre front ne remplacera
pas le sang versé, mais le regard ou la parole de gratitude que vous ont
adressée celles et ceux que vous avez aidés restera pour vous la seule
récompense qui vaille et que vous ne dédaignerez point. Nul concours ni
compétition, nul prix ni palmarès ; cependant, vous avez été les plus
nombreux à venir partager cette expérience que vous n’oublierez pas. Rien
que de très normal, répondrez-vous. C’est comme une visite à un parent
souffrant. Vous avez été les plus proches. La compassion n’est pas chez
vous qu’une vertu, elle est une action en marche. Indépendamment de vos
opinions religieuses et de vos affinités politiques respectives, vous avez
tous voté oui au référendum que vous a dicté votre cœur, lequel bat pour
Israël lorsque celui-ci est terrassé. À votre retour, beaucoup salueront
votre ténacité, d’autres seront perplexes face à tant de témérité ;
certains souligneront votre inconscience. Les plus critiques manifes-teront
leur réserve pour un engagement par trop unilatéral. J’entends, pour
ma part, saluer votre dévouement. Les lignes qui suivent interrogent avec
gravité les évé-nements récents et tentent de déchiffrer leur cours
possible. Que vous en partagiez les idées ou non, elles sont indissociables
de l’hommage que je tiens à vous rendre.  

Avant / Après
================

Toute réflexion sur la société israélienne aujourd’hui part
du postulat que le 7 octobre 2023 constitue une rupture dans l’histoire
moderne d’Israël. Il y aura pour toujours un "avant" et un
"après". Pour les morts, civils ou militaires, il n’y aura plus
d’après. Pour ceux qui ont subi l’invasion du Hamas et, frôlant la mort
de près, ont survécu, rien ne sera plus comme avant, de même que pour les
familles qui enterrent celles et ceux qui sont morts au champ d’honneur
qui fut souvent un champ d’horreur. Et que dire du calvaire que subissent
dans la bande de Gaza les otages encore détenus à ce jour où j’écris
[le 20 janvier] ?

Sidération, traumatisme… Au-delà du premier cercle directement concerné,
nous sommes tous ébranlés, secoués, mis à l’épreuve. Les repères et
les critères de la division idéologique et sociale ont volé en éclats. On
rapporte que bien des "gauchistes", devenus "lucides"
(mitpakhim, en hébreu), expient leur naïveté et jurent qu’on ne les
reprendra plus en train de scander "la paix maintenant". Dans la
même proportion, bien des supporters du Likoud ne supportent plus celui
qu’ils tenaient hier encore pour un leader irremplaçable. L’économie
qui était au vert tourne au rouge ; la culture est en berne ; les médias,
si incisifs d’ordinaire, sont au garde-à-vous ; la classe politique,
si bavarde en général, est devenue muette, soit qu’on ne lui tende plus
le micro soit qu’elle se retire dans un silence prudent. Tout chancelle,
et surtout, tout paraît négligeable et sans importance face au déluge de
haine et de sang qui a explosé, face au drame des otages dont la moitié
croupit encore dans les geôles de Gaza, face à la douleur et la peur qui
ont frappé et frappent encore.

On veut croire qu’un autre Israël surgira de l’épreuve : l’union
sacrée qui a stoppé net les dissensions intestines persistera, espère-t-on,
après la fin de la guerre ; les clivages, réels ou artificiels, qui ont
opposé les Israéliens ne se reconstitueront pas ; un nouveau consensus
s’est imposé, tous ou presque s’y rallient et continueront de lui
prêter allégeance. Pressentiment, intuition, vœu pieux ? Les historiens
sont souvent des professeurs de désenchantement ; ils constatent que dans la
longue durée les promesses de changement sont rarement tenues. L’opinion est
volatile et versatile. Enfin, les changements sont souvent les confirmations
explicites d’une tendance souterraine qui couvait depuis longtemps déjà
et que la catastrophe a précipitée.  


"La crise politique, judiciaire, sur l’avenir de la démocratie, étalée, de semaine en semaine, sur pas moins de neuf mois, s’est aussitôt figée face à la catastrophe la plus effroyable éprouvée par des Juifs depuis la fin de la Seconde Guerre mondiale, à plus forte raison par les Israéliens depuis 1948."
=========================================================================================================================================================================================================================================================================================================================

Le jour du 7 octobre 2023 fait date ; il pourrait faire génération. 

Il porte à jamais la couleur du deuil et de l’effroi : le "shabbat noir"
(ha-shabbat ha-shrora, en hébreu) ainsi qu’il a aussitôt été appelé
[1]. Dans cette même veine, 2023 aura été "l’année terrible". Sa
place est retenue d’office dans le martyrologe juif. Au lieu de tirer
gloire du 75e anniversaire de la création de l’État d’Israël et
de commémorer le 50e anniversaire de la guerre du Kippour, dont on était
censé avoir tiré toutes les leçons, le pays a vécu l’année de tous les
dangers : une crise intérieure sans précédent autour d’une législation
controversée qui a rapproché les Israéliens chaque jour un peu plus du
précipice où nous sommes tombés le lendemain, avec, en substitut d’une
guerre civile, une guerre avec les ennemis jurés, mais qui ne ressemble à
aucune de celles qu’Israël avait menées jusque-là. La crise politique,
judiciaire, sur l’avenir de la démocratie, étalée, de semaine en semaine,
sur pas moins de neuf mois, s’est aussitôt figée face à la catastrophe
la plus effroyable éprouvée par des Juifs depuis la fin de la Seconde
Guerre mondiale, à plus forte raison par les Israéliens depuis 1948 qui,
de guerres en attentats, d’embuscades en opérations, ont pourtant une
grande expérience de la violence.

Le bilan des pertes a déterminé cette perception de l’événement :
en moins de vingt-quatre heures, ce sont près de 1 200 personnes qui ont
été massacrées tandis que 250 autres ont été enlevées et plus de
130 sont encore détenus comme otages. Le déroulement des faits a été
un élément déclencheur du traumatisme : les systèmes de défense ont
failli les uns après les autres, et la faillite s’est répercutée à
tous les éche-lons, comme des dominos entraînant dans leur chute tous
les autres rouages. Les habitants des cités et des communes voisines de
la bande de Gaza ont dû attendre des heures avant que n’arrivent les
premiers secours, des jours et des semaines avant que le gouvernement
ne commence à réagir et à s’organiser. Cette insupportable attente,
scandée par des messages WhatsApp laconiques et désespérés, a suscité
une onde de choc : on savait l’administration peu efficace et tatillonne,
mais l’armée ? Force d’intervention rapide, son image de marque a pâli,
s’est ternie. Celle de l’État, à plus forte raison. C’est sans
doute de la crise intérieure l’aspect le plus grave. L’État a été
pris en flagrant délit de défaillance. La société juive a éprouvé,
en quelque sorte, ce que la société arabe a vécu depuis 2019 lorsque
la pègre sévissait, tirant sur tout ce qui bougeait sans que la police
intervienne, blâmant les pouvoirs publics pour leur cruelle absence, alors
par négligence et indifférence, aujourd’hui, par incompétence. Cette
expérience dramatique d’un État qui faillit à sa mission a eu pour
effet de stimuler la société civile à agir, à agir sans l’État puisque
celui-ci était aux abonnés absents. Attentif à la contestation qui couvait
en sourdine, le gouvernement, pour rétablir sa légitimité auprès des
gouvernés, a procédé à la suppression de quelques ministères superflus
distribués pour satis-faire les appétits de cette drôle de coalition,
illustration la plus affligeante du gaspillage et de la vanité qui avait
régné sans vergogne depuis un an.  


**Le Hamas : ennemi exterminateur**
=====================================


Avant d’examiner les responsabilités indirectes à l’échelon militaire et
gouverne-mental, commençons par le commencement, c’est-à-dire par le Hamas.

En donnant à l’opération terroriste qu’il a conduite une dimension
exterminatrice pour en faire un massacre de masse, le Hamas s’est privé
sciemment des avantages politiques qu’il aurait pu retirer s’il avait
limité son opération à des cibles militaires. Par le paroxysme de la
violence qu’il a atteint, il s’est exclu de toute participation à un
règlement futur, imprimant au conflit une dimension existentielle que,
frappés d’effroi, les Israéliens perçoivent désormais comme tel :
c’est eux ou nous et si vous n’êtes pas avec nous (ONU, Croix-Rouge
internationale, Universités de la Ivy League, organisations féministes),
c’est que vous êtes contre nous.

En frappant des civils avec une telle frénésie, le Hamas a signé son arrêt
de mort. C’est une lutte à mort qui se déroule sous nos yeux. Il y entre,
du côté d’Israël, la soif de venger le crime, la volonté de punir ses
auteurs, la nécessité de riposter, l’urgence de l’éliminer. Tel est
le sens donné aussitôt à l’opération militaire qui a suivi, et dont
l’objectif est l’éradication du Hamas, c’est-à-dire la liquidation
de ses capacités militaires et le terme mis à l’autorité politique
qu’il exerce depuis 2007 dans la bande de Gaza.

Le Hamas que l’on croyait accommoder en autorisant les transferts de fonds
du Qatar et dont on avait fini par s’accommoder a frappé un grand coup.

"Le Hamas [...] a tendu un piège à Israël et nous y sommes tombés. La paix avec lui étant impossible et non souhaitable, on a estimé que la coexistence, pour être tendue, résultait de l’intériorisation pragmatique par le Hamas d’un rapport de force structurellement asymétrique."
=======================================================================================================================================================================================================================================================================================================

Lorsque des mouvements religieux radicaux font irruption dans le champ
politique, on tend à penser que si leur idéologie demeure rigide, infaillible
et inaltérée, c’est dans l’exercice du pouvoir qu’ils révèlent une
certaine souplesse. La maîtrise des affaires courantes requiert des prises
de décision qui forcent ces mouvements à faire place à des compromis tout
en maintenant intacts leurs dogmes fondamentaux. C’est globalement de la
sorte que les autorités politiques et militaires d’Israël ont perçu
le Hamas avec d’autant plus de conviction que cette thèse s’avérait
parfaitement compatible avec la volonté politique d’affaiblir l’Autorité
palestinienne dans le but de boucher la voie à toute négociation susceptible
de modifier le statu quo. Bref, on a longtemps préféré "contenir" le
Hamas, distinguant la théorie de la pratique, l’idéologie de l’action
sur le terrain, nouant avec lui une diplomatie tacite, laquelle est une
antenne de communication toujours disponible entre ennemis qui ne se parlent
pas. C’était sans compter avec la ruse dont le Hamas a fait preuve sans
fléchir.

Le Hamas a laissé croire qu’il se contente-rait de régner à Gaza,
d’obtenir des milliers de permis de travail et de maintenir le statu
quo, l’interrompant à intervalles réguliers par des provocations
successives, mais bien maîtrisées. Il a tendu un piège à Israël
et nous y sommes tombés. La paix avec le Hamas étant impossible et non
souhaitable, on a estimé que la coexistence, pour être tendue, résultait
de l’intériorisation pragmatique par le Hamas d’un rapport de force
structurellement asymétrique.  

Quelle guerre et comment la faire ?
=======================================

Pour mener l’opération militaire jusqu’à son terme, le gouvernement a
besoin du consensus général de la population israélienne. Il l’a obtenu
et le soutien a été unanime. Une contradiction s’est pourtant insinuée
entre le but de guerre et l’objectif de la libération des otages. C’est
là le seul clivage autorisé en Israël, ou plus exactement, le seul clivage
qu’on s’autorise : une opposi-tion encore feutrée entre les fervents
de la Raison d’État et les partisans de la Raison de la Nation. La guerre
est faite pour sauver l’État juif et rétablir sa sécurité, déclarent
les premiers, estimant que la libération des otages peut compléter le but
de guerre, mais non s’y substituer ; les seconds estiment que l’État
d’Israël signe avec chaque citoyen un contrat implicite en vertu duquel
son devoir est de protéger et de sauver tout citoyen en contrepartie de la
disposition à sacrifier sa vie en temps de guerre. C’est le sens actualisé
du "plus jamais ça" à "nous : victime, tu ne seras plus !"
Or, les otages en sont. Ils doivent être libérés par tous les moyens,
– la guerre ou la négociation. Et si la poursuite des combats menace leur
existence, alors la négociation prime sur la guerre.

Compte tenu de l’objectif de longue haleine poursuivi par les autorités,
le soutien de la population israélienne est nécessaire pour assurer la
légitimité de l’action militaire, mais insuffisant : il est impératif pour
Israël de pouvoir compter sur le soutien actif, militaire et diplomatique,
de l’administration américaine et l’approbation tacite de l’Union
européenne. Pour en disposer pleinement et sans réserve, il eût fallu
réaliser l’opération en un temps limité – comme une nouvelle guerre
des Six Jours – et épargner autant que faire se peut les civils. Mission
impossible. Qu’Israël s’y soit efforcé ou qu’il n’en ait eu cure,
la plupart des Israéliens ont fait de nécessité vertu. Autrefois, l’on
aurait juré les grands dieux que tous les moyens avaient été employés
pour réduire les pertes, désormais Israël assume. Le 7 octobre a fait
bouger les lignes.  


"Toute issue aura un coût politique pour Israël, bien au-delà du coût humain que la société israélienne tolèrera pourvu que l’objectif soit réalisé. S’il n’est pas atteint, la colère populaire, contenue en temps de guerre en solidarité avec les soldats, ne tardera pas à exploser  à l’issue des combats."
====================================================================================================================================================================================================================================================================================================================

Autre leçon amère à tirer depuis le déclenchement de la guerre : alors
qu’il apparaissait comme un ennemi irréductible sur le plan idéologique,
mais doté de moyens limités sur le plan militaire, le Hamas tient bon,
beaucoup plus longtemps qu’on ne le pensait, et adopte la tactique de
la guérilla : il se dissimule dans ce labyrinthe des tunnels et évite
l’affrontement direct. La guerre durant plus de trois mois et le nombre
de civils dépassant les quinze mille, le chèque en blanc accordé
par l’administration américaine sera probablement assorti de quelques
conditions restrictives dans les jours et les semaines à venir. Le temps
étant la ressource la plus précieuse, il n’est pas exclu que l’armée
israélienne consente, non à revoir l’objectif initial, mais à être
plus attentive sur la conduite de la guerre en réduisant la puissance
de feu employée et en tenant compte de considérations humanitaires qui
avaient été pratiquement balayées jusque-là dans le but de protéger
les effectifs de Tsahal.

Toute issue aura un coût politique pour Israël, bien au-delà du coût
humain que la société israélienne tolèrera pourvu que l’objectif soit
réalisé. S’il n’est pas atteint, la colère populaire, contenue en
temps de guerre en solidarité avec les soldats, ne tardera pas à exploser
à l’issue des combats. On peut passer outre une faillite en amont, pas
une faillite en aval. Il faudrait alors admettre, la mort dans l’âme,
que les soldats tombés au combat ont été un vain sacrifice. Si le Hamas
parvient à se maintenir, demandera-t-on, la rage au ventre, pourquoi
avoir mené délibérément, une guerre d’une telle ampleur pour un aussi
piètre résultat ? Israël est donc condamné à réussir. Or, et c’est
là tout le paradoxe de cette guerre : si Israël l’emporte, si le Hamas
est bel et bien neutralisé, c’est alors que la coalition des pays qui
participeront à la reconstruction économique de la bande de Gaza auront non
seulement le droit mais le devoir de réclamer de l’État hébreu qu’il
apporte une contribution substantielle à la résolution du conflit après
qu’ils aient apporté leur soutien à l’opération militaire, malgré
les réserves apparues au sein de leurs opinions publiques respectives.

La question palestinienne
===============================

Alors quelle leçon faut-il tirer, fût-ce à titre provisoire, à ce stade,
puisque l’issue des combats n’a pas encore sonné ? Quoi qu’il en soit
des misperceptions et des miscalculations dont ont fait preuve les autorités
militaires et politiques israéliennes, aucune d’elles ne devrait plus
avoir le front de prétendre, comme elles l’ont prétendues depuis plus
d’une décennie, que la ques-tion palestinienne a perdu de sa centralité,
qu’elle est marginale et qu’elle n’intéresse plus Israël, le monde
arabe et la communauté internationale.

Les Accords d’Abraham avaient confirmé, en apparence, ce scénario, lequel
eut droit aux honneurs d’être baptisé du nom autrement plus prestigieux de
"paradigme". Les Palestiniens n’avaient-ils pas perdu leur droit
de veto implicite sur toute normalisation des relations entre Israël et
les pays arabes ? Ce changement de paradigme s’est écroulé. Le Hamas
qui, de l’extérieur, a torpillé les Accords d’Oslo, a stoppé net
le processus de normalisation des relations de l’Arabie saoudite avec
Israël, autrement dit, le joyau de la couronne des Accords d’Abraham
que l’administration américaine était sur le point de sertir. Notons
toutefois que les accords signés avec les Émirats, le Bahreïn et le
Maroc ont résisté et survécu à l’attaque du Hamas. Les négociations
avec l’Arabie saoudite sont interrompues, mais interrompre n’est pas
rompre. L’Arabie saoudite, qui s’apprêtait à jeter quelques miettes
économiques à l’Autorité palestinienne, prendra en charge l’essentiel
de l’enveloppe financière pour la reconstruction de Gaza, mais elle ne le
fera désormais qu’en contrepartie d’une repolitisation de la question
palestinienne. Si cette démarche pragmatique se concrétise, le consensus
interne né le 7 octobre s’effritera. Si, en revanche, le conflit est
toujours perçu comme existentiel, le consensus se maintiendra. Il est
plausible que la représentation dominante en Israël sera à mi-chemin
entre les deux.  


"Pour que les Palestiniens se démarquent du Hamas et s’en éloignent, il incombe de leur offrir un horizon concret, un cadre impératif, un calendrier garanti par une task force internationale, la mise à exécution de leur droit à l’autodétermination, assortie d’une reconnaissance sans ambiguïté de l’État d’Israël."
==============================================================================================================================================================================================================================================================================================================================

À cet égard, si les convictions profondes ne changent guère, les
modalités ont été durablement altérées. Nul ne prétend plus
aujourd’hui que la solution à deux États peut être immédiatement
appliquée. La solution économique, que l’on trouvait méprisante et
irrespectueuse des revendi-cations politiques palestiniennes légitimes, sera
demain l’ingrédient indispensable. Quant à la radicalisation islamiste,
elle ne peut plus être sous-estimée. Pour ceux qui en sont pénétrés,
la réponse sera sécuritaire et consistera à les mettre hors d’état
de nuire. Pour les priver de toute audience, le rôle d’Israël et de
la communauté internationale, n’est pas négligeable : pour que les
Palestiniens se démarquent du Hamas et s’en éloignent, il incombe de
leur offrir un horizon concret, un cadre impératif, un calendrier garanti
par une task force internationale, la mise à exécution de leur droit à
l’autodétermination, assortie d’une reconnaissance sans ambiguïté de
l’État d’Israël.

Charles Péguy opposait le parti de la charrue à celui du sabre. Il faut
du travail et du travail pour faire un homme, écrivait-il, il faut une
minute pour le défaire. Les forces qui démolissent ont toujours une mesure
d’avance sur les forces qui construisent. Comme la guerre, il faut sans doute
se résoudre à faire la paix sans s’aimer, sans illusions. Une paix froide.

Le conflit a resurgi, avec une violence sans précédent dans l’histoire
palestinienne et une riposte sans précédent elle aussi dans l’histoire
d’Israël. Je ne prétends nullement qu’un processus de paix aurait
empêché une telle agression puisque la raison d’être du Hamas est
d’empêcher un règlement pacifique du conflit, mais au moins l’opinion
palestinienne en Cisjordanie et à Gaza aurait été divisée, ce qui est
loin d’être le cas aujourd’hui. Faute d’horizon politique, elle se
rallie au drapeau comme le font les Israéliens, même si se réjouir de la
mort de son ennemi n’est pas identique à la sensation de n’avoir plus
aucune compassion pour autrui. Voilà où se trouve le conflit aujourd’hui :
on ne se bat pas seulement pour une terre, un drapeau et sa survie, Dieu est
mobilisé de part et d’autre, et le conflit ébranle, cette fois, l’idée
même d’humanité. Les hommes du Hamas qui ont participé au crime s’en
sont exclus par leur barbarie ; et, effet inévitable d’une guerre à
outrance, nous ne sommes plus en capacité de discernement : tous ceux qui
sont dans le camp d’en face paraissent identiques.  

Résoudre le conflit ?
===========================

L’histoire est tragique. Malgré les leçons qu’on prétend avoir
apprises, peuples et leaders sont incapables d’emprunter les raccourcis qui
permettraient d’éviter les bains de sang. Non que ce massacre de masse
était prévisible, mais force est de reconnaître que le contournement
et l’évitement de la question palestinienne ont échoué. Que ceux qui
n’ont cessé de nous dire : "on a essayé de négocier, ils ont reculé
à chaque fois" fassent un pas de côté. Cela fait dix ans déjà
qu’ils martelaient que c’était peine perdue et qu’il était inutile
d’essayer. Rien n’aura lieu si les partenaires ne sont pas à la hauteur
de l’événement. Côté palestinien, le seul duo crédible est celui que
pourrait constituer Marwan Barghouti, "le Mandela palestinien" et
Salam Fayyad, l’expert de la Banque mondiale. Le premier bénéficiera de la
légitimité interne intra-palestinienne de par son statut d’ex-détenu ;
le second, de la légitimité internationale et israélienne car il a mis
de l’ordre dans les services de sécurité palestiniens et combattu la
corruption lorsqu’il était Premier ministre de l’Autorité palestinienne
de 2007 à 2013.  


"Cette longue nuit commencée le 7 octobre 2023 ne promet nul lendemain qui chante. Le changement qui surgira du traumatisme peut être une régression aussi bien qu’une percée étroite pour couper court à une guerre sans fin alternant massacre et destruction."
======================================================================================================================================================================================================================================================================

Côté israélien, il est prévisible que de nouvelles élections anticipées
feront des listes modérées le centre de gravité de l’échiquier
politique. La gauche ne parviendra pas à ressusciter de manière significative
; l’extrême droite sera une force d’opposition condamnée à une fonction
tribunitienne. Les partis religieux devraient renouer avec leur tendance
modératrice, voire modérantiste, abandonnée depuis une décennie. Ils
ne demanderont plus guère d’établir une équivalence entre un service
militaire et un service en yéchiva. La mise en cause du pouvoir judiciaire
est révolue. Il a suffi qu’on entende à la Cour internationale de justice
(CIJ) de La Haye l’un des représentants de la partie israélienne vanter
l’indépendance de la Cour suprême pour comprendre que le gouvernement ne
touchera plus le dispositif institutionnel actuel. Il n’est pas impossible
d’espérer une nouvelle entente entre Juifs et Arabes d’Israël : le 7
octobre, les premiers ont exigé des seconds qu’ils se prononcent et les
Arabes ont répondu en conscience qu’ils étaient du côté d’Israël,
malgré la suite des événements. Les émeutes de 2021 ne se sont pas
reproduites malgré les provocations émanant du ministre de la "Sécurité
nationale". Les Arabes ont redouté, à leur tour, qu’on les tienne pour
des citoyens en sursis. La guerre civile n’a pas eu lieu. L’addition de
ces changements peut créer une nouvelle donne. Mais qu’on ne se méprenne
pas : pour en arriver là, il ne faudra rien moins qu’un alignement des
planètes, version scientifique et sécularisée du miracle.

Cette longue nuit commencée le 7 octobre ne promet nul lendemain qui
chante. Le changement qui surgira du traumatisme peut être une régression
aussi bien qu’une percée étroite pour couper court à une guerre sans fin
alternant massacre et destruction. Si le Hamas qui a replacé la question
palestinienne au centre des pré-occupations internationales se maintient,
il entraînera le problème palestinien dans l’impasse. Depuis dix ans,
les gouvernements israéliens successifs ont préféré gérer le conflit
et ce pari semblait être gagné. Il s’est écroulé le 7 octobre.

Résoudre le conflit alors ? Ce ne sera plus, 7 octobre oblige, la paix ou la
sécurité : la complémentarité des deux est désormais impérative. Mais
nous ne sommes plus en 1993, après six ans d’Intifada. Le chemin est
d’autant plus long et semé d’embûches qu’il n’est pas balisé. Reste
à savoir s’il est encore temps.

Denis Charbit

Notes 
=============

[1] Le syntagme n’est pas inédit ; il fut employé pour qualifier le
couvre-feu général décrété dans le pays par les autorités britanniques
mandataires et l’arrestation massive des principaux leaders du yichouv le
samedi 29 juin 1946.

Biographie 
==============

Denis Charbit, spécialiste de l’histoire et de la société d’Israël,
est Directeur de l’Institut de recherche sur les relations entre juifs,
chrétiens et musulmans et professeur de science politique à l’Open
University d’Israël (Ra’anana).

- Les opinions exprimées dans les articles n'engagent que leurs auteurs -

À lire aussi 
=================

- `Études du Crif n°66 : 7 octobre 2023 - Fracture(s) <https://www.crif.org/fr/content/etudes-du-crif-n66-7-octobre-2023-fractures>`_

::

    - `Études du Crif n°66 : 7 octobre 2023 - Fracture(s) - Édito du Président du Crif <https://www.crif.org/fr/content/etudes-du-crif-n66-7-octobre-2023-fractures-edito-du-president-du-crif>`_
    - `Études du Crif n°66 : 7 octobre 2023 - Fracture(s) – Entretien avec Joan Sfar -  Nous nous répétions “ Plus jamais ça ” et “ ça ” s’est produit sous nos yeux <https://www.crif.org/fr/content/etudes-du-crif-n66-7-octobre-2023-fractures-entretien-avec-joann-sfar>`_
    - `Études du Crif n°66 : 7 octobre 2023 - Fracture(s) – Article de Bruno Karsenti et Danny Trom -  Depuis le pogrom. La mutation de la configuration juive <https://www.crif.org/fr/content/etudes-du-crif-n66-7-octobre-2023-fractures-article-de-bruno-karsenti-et-danny-trom>`_
    - `Études du Crif n°66 : 7 octobre 2023 - Fracture(s) – Article de Frédérique Leichter-Flack - Un massacre, des massacres : Les archives du 7 octobre et la mémoire <https://www.crif.org/fr/content/etudes-du-crif-n66-7-octobre-2023-fractures-article-de-frederique-leichter-flack-un-massacre-des-massacres>`_
    - `Études du Crif n°66 : 7 octobre 2023 - Fracture(s) – Article de Julia Christ - Le silence du savoir <https://www.crif.org/fr/content/etudes-du-crif-n66-7-octobre-2023-fractures-article-de-julia-christ-le-silence-du-savoir>`_
    - `Études du Crif n°66 : 7 octobre 2023 - Fracture(s) – Article de Julien Darmon -  Les Juifs français à un tournant de leur histoire ? État des lieux et perspectives  <https://www.crif.org/fr/content/etudes-du-crif-n66-7-octobre-2023-fractures-article-de-julien-darmon-les-juifs-francais-a-un-tournant-de-leur-histoire>`_
    - `Études du Crif n°66 : 7 octobre 2023 - Fracture(s) – Entretien avec Danny Trom L’inquiétude, entendue comme une appréhension à l’égard du futur, est un affect typiquement juif <https://www.crif.org/fr/content/etudes-du-crif-n66-7-octobre-2023-fractures-entretien-avec-danny-trom-linquietude-entendue>`_
