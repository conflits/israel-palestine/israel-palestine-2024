
.. _muir_2024_01_03:

====================================================================================
2024-01-03 « Une terre sans peuple pour un peuple sans terre » par Diana Muir
====================================================================================

- https://k-larevue.com/une-terre-sans-peuple-pour-un-peuple-sans-terre/ 

.. tags:: Histoire

Les premiers sionistes croyaient-ils vraiment que la Palestine
fût une terre déserte, sans population ? C’est, pour certains, ce qui se
laisserait deviner derrière la formule « Une terre sans peuple pour un peuple
sans terre ». Diana Muir, en retraçant ici l’histoire de l’origine et
des usages de cette formule, montre que ce serait à la fois faire au sionisme
un mauvais procès et évacuer la question de la construction de l’identité
nationale palestinienne.


« Une terre sans peuple pour un peuple sans terre » est l’une des formules
les plus fréquemment citées dans la littérature sur le sionisme et peut-être
aussi la plus problématique. Les antisionistes considèrent qu’elle résume
parfaitement l’injustice fondamentale du sionisme : les premiers sionistes,
croyant que la Palestine était inhabitée[1], ont nié — et continuent de
rejeter — l’existence d’une culture palestinienne distincte[2]. Les mêmes
vont jusqu’à voir dans ce slogan la preuve que les sionistes ont toujours
planifié un nettoyage ethnique de la population arabe[3]. De telles affirmations
sont sans fondement : elles nient la conscience qu’avaient les premiers
sionistes de la présence d’Arabes en Palestine et insistent exagérément
sur la gestation de l’identité nationale palestinienne, laquelle ne s’est
en réalité manifestée qu’en réaction à l’immigration sioniste[4]. Par
ailleurs, contrairement aux allégations de nombreux antisionistes, il est faux
d’affirmer que les premiers sionistes ont largement utilisé cette formule.
Origines de la formule

De nombreux commentateurs, tels que le théoricien de la littérature arabe
Edward Said, attribuent à tort la première utilisation de l’expression à
Israël Zangwill, un auteur, dramaturge et poète britannique[5]. 

En fait, l’expression a été inventée et propagée par des écrivains chrétiens
du XIXe siècle.

En 1831, Muhammad Ali Pacha, le souverain égyptien, arrache la Grande Syrie
au contrôle direct des Ottomans : un changement politique qui conduit le
ministère britannique des Affaires étrangères à envoyer un consul à
Jérusalem. Cette initiative enflamme l’imagination populaire.

La première utilisation de cette phrase dans une œuvre écrite semble
pouvoir être attribuée à Alexander Keith, membre du clergé de l’Église
d’Écosse, auteur d’un ouvrage publié en 1843 et intitulé The Land of
Israel According to the Covenant with Abraham, with Isaac, and with Jacob [La
terre d’Israël selon l’alliance avec Abraham, Isaac et Jacob][6]. Keith
était un penseur évangélique influent dont l’ouvrage le plus populaire,
Evidence of the Truth of the Christian Religion Derived from the Literal
Fulfillment of Prophecy [Preuves de la vérité de la religion chrétienne
tirées de l’accomplissement littéral de la prophétie][7] est toujours
imprimé près de deux siècles après sa première publication. Défendant
l’idée que les chrétiens devraient s’efforcer d’encourager la
prophétie biblique d’un retour des Juifs sur la terre d’Israël, il
écrit que les Juifs sont « un peuple sans pays, de même que leur propre
pays, comme on le verra par la suite, est dans une large mesure un pays sans
peuple »[8]. S’étant lui-même rendu en Palestine en 1839 pour le compte
de l’église d’Écosse et y étant retourné cinq ans plus tard avec son
fils George Skene Keith — lequel fut probablement le premier photographe à
se rendre en Terre sainte —, Keith n’ignorait pas que cette même terre
était peuplée.

En juillet 1853, l’homme d’État et réformateur social Lord Shaftesbury
écrit au ministre britannique des Affaires étrangères, Lord Palmerston,
que la Grande Syrie est « un pays sans nation » à la recherche d’une
« nation sans pays… Or, existe-t-il une telle nation ? Bien sûr en
la personne des anciens et légitimes détenteurs du sol, c’est-à-dire
les Juifs ! »[9] Et Shaftesbury de préciser dans son journal que ces «
régions vastes et fertiles seront bientôt sans souverain, sans pouvoir
connu et reconnu en mesure de revendiquer son dominium. Le territoire doit
être attribué à tel ou tel. Il y a un pays sans nation ; et Dieu, dans sa
sagesse et sa miséricorde, nous dirige maintenant vers une nation sans pays
»[10]. La biographie de Shaftesbury publiée par la suite s’est bien vendue
et a permis de diffuser la formule auprès d’un public plus large[11].

L’année suivant cette première utilisation de la formule par Shaftesbury,
l’auteur d’un magazine presbytérien déclare à ses lecteurs : « Il
est certain que la terre sans peuple et le peuple sans terre sont destinés
à se rencontrer bientôt et à se posséder mutuellement. »[12] Dans un
essai de 1858, un autre presbytérien écossais, Horatius Bonar, prône le
« rapatriement d’Israël… [dans lequel] nous avons un peuple sans pays,
ainsi qu’un pays sans peuple »[13].

Au terme d’un voyage en Terre sainte en 1881, l’Américain William Eugene
Blackstone, autre chrétien partisan du rétablissement d’une population juive
en Palestine, écrit que cette « phase de la question [du sort à réserver aux
Juifs victimes des persécutions tsaristes] constitue une anomalie étonnante :
une terre sans peuple, et un peuple sans terre »[14].

Les anglicans sont eux aussi favorables à ce concept. En 1884, George Seaton
Bowes, membre du clergé de l’université de Cambridge, plaide en faveur
du retour des Juifs en Palestine et utilise également l’expression « une
terre sans peuple… [pour] un peuple sans terre »[15].

John Lawson Stoddard, Bostonien issu d’un milieu privilégié, s’enrichit
en voyageant dans des pays lointains et en donnant ensuite des conférences
utilisant les premières diapositives consacrées aux pays visités. Dans
un carnet de voyage datant de 1897, il exhorte les Juifs : « Vous êtes
un peuple sans pays ; il y a un pays sans peuple. Soyez unis. Réalisez les
rêves de vos vieux poètes et patriarches. Retournez, retournez sur la terre
d’Abraham. »[16]

À la fin du XIXe siècle, la formule est couramment utilisée en Grande-Bretagne
et aux États-Unis par les chrétiens intéressés par le retour d’une
population juive en Palestine[17]. L’utilisation par les chrétiens de cette
formule perdure jusqu’aux premières décennies du XXe siècle. En 1901, le
missionnaire américain Harlan Page Beach, qui deviendra plus tard professeur
à Yale, émet l’idée que les Juifs habiteront un jour, « au moment voulu
par Dieu, la terre de leurs ancêtres ; sinon, nous ne pouvons offrir aucune
explication valable d’un peuple sans terre et d’une terre sans peuple…
»[18] Dans son roman de 1902, The Zionist, l’écrivaine anglaise Winifred
Graham (1873-1950) fait intervenir son héros juif devant le congrès sioniste
et plaide pour le retour du « peuple sans pays dans le pays sans peuple
»[19]. Augustus Hopkins Strong, un éminent théologien baptiste américain,
utilise l’expression en 1912[20] de même qu’un article de fond rédigé par
un journaliste chrétien et paru dans le Washington Post du 12 décembre 1917.

La première utilisation de l’expression par un sioniste ne date que de 1901,
lorsqu’Israël Zangwill, reprenant probablement la formulation de Shaftesbury,
écrit dans la New Liberal Review que « la Palestine est un pays sans peuple ;
les Juifs sont un peuple sans pays »[21].  Le nationalisme juif en contexte

Bien que l’image de la Palestine en tant que « terre sans peuple » ait été
le plus souvent avancée par les partisans chrétiens d’un retour des Juifs en
Palestine, il serait faux de ne l’attribuer qu’aux seuls chrétiens. Dans
le contexte du XIXe siècle, où de nombreux mouvements nationalistes ont
marqué l’imaginaire occidental, la notion de restauration juive en Palestine
semblait logique, même en l’absence de motivations religieuses. En 1891,
William Blackstone envoie une lettre ouverte, connue aujourd’hui sous le
nom de Blackstone Memorial, au président américain Benjamin Harrison :
« Pourquoi les puissances qui, en vertu du traité de Berlin de 1878, ont
donné la Bulgarie aux Bulgares et la Serbie aux Serbes ne rendraient-elles
pas aujourd’hui la Palestine aux Juifs ? … Ces provinces, de même que
la Roumanie, le Monténégro et la Grèce, ont été arrachées aux Turcs et
rendues à leurs propriétaires naturels. La Palestine n’appartient-elle
pas aussi légitimement aux Juifs ? »[22] Les Occidentaux du XIXe siècle
associent les peuples ou les nations à un territoire, de sorte que parler
d’une terre sans peuple ne signifie pas que cette terre n’est pas peuplée,
mais seulement qu’elle ne possède pas de caractère politique national.

Ce qui peut paraître étrange, du point de vue des Arabes, c’est ce
prisme à travers lequel les Occidentaux perçoivent la terre. Aux yeux des
Occidentaux, les contours d’un territoire appelé « Terre sainte » ou «
Terre d’Israël » se surimposent depuis toujours au vaste territoire de
la Méditerranée orientale. Parce que les Occidentaux assimilent les terres
aux peuples, même les Occidentaux post-chrétiens s’attendent à trouver un
peuple identifié et coïncidant avec la Terre sainte. Les musulmans, cependant,
ne perçoivent pas la Palestine comme un pays distinct, ni les Palestiniens comme
un peuple. À l’époque ottomane, la Terre sainte et ses régions agricoles
de valeur moyenne étaient soumises au pouvoir de Beyrouth ou de Damas, où
vivaient la plupart des riches familles arabes propriétaires de terres en
Palestine. À cette époque, les Arabes considéraient la Terre sainte comme
une partie intégrante de la Syrie, Bilad ash-Sham[23]. La perception musulmane
de la Syrie et de la Palestine en tant que pays distincts est apparue au cours
du XXe siècle[24]. Aux yeux des Arabes, avant la Première Guerre mondiale,
l’ensemble du Bilad ash-Sham, y compris les parties que les chrétiens et
les juifs considéraient comme la Terre sainte, faisait partie intégrante des
possessions domaniales de familles arabes et ne constituait pas une entité
distincte.

Les partisans d’un retour des Juifs en Israël, lorsqu’ils pensent aux
habitants arabes, partent du principe que la population arabe existante
continuera à résider en Israël après la création d’un État juif. Ce
scénario semblait réalisable puisque tous les États-nations comptent des
minorités ethniques parmi leurs citoyens.  Contestation du slogan

Les opposants au sionisme commencent à attaquer le slogan peu après la
publication de la déclaration Balfour. En 1918, Ameer Rihami, un nationaliste
arabe chrétien américano-libanais, écrit : « Je dirais même… ‘Donnez
la terre sans peuple au peuple sans terre’ si la Palestine était vraiment
sans peuple et si les Juifs étaient vraiment sans terre ». Selon lui, les
Juifs n’ont pas besoin d’une patrie en Palestine parce qu’ils jouissent
partout ailleurs « de l’égalité des droits et des chances, c’est le
moins que l’on puisse dire »[25]. Cette attitude n’est pas propre aux
nationalistes arabes. Un universitaire arabe du début du XXe siècle écrit
ainsi : « Leur slogan même, ‘La terre sans peuple pour le peuple sans
terre’, constitue une insulte pour les Arabes du pays. »[26] Le journaliste
américain William McCrackan déclare : « Nous avions l’habitude de lire
dans nos journaux le slogan sioniste ‘rendre un peuple à une terre sans
peuple’, alors qu’en vérité la Palestine était déjà bien peuplée,
avec une population augmentant rapidement pour des raisons naturelles. »[27].

Les partisans d’un État binational en Palestine utilisent cette expression
lorsqu’ils débattent avec les sionistes traditionnels. Robert Weltsch,
rédacteur en chef du prestigieux hebdomadaire sioniste allemand Juedische
Rundschau, écrit par exemple en août 1925 : « Nous sommes peut-être un
peuple sans maison, mais, hélas, il n’y a pas de pays sans peuple. La
Palestine compte actuellement 700 000 habitants. »[28]

Les propagandistes anti-israéliens dénoncent le slogan après la création,
en 1964, de l’Organisation de libération de la Palestine (OLP)[29]. Dans son
discours du 13 novembre 1974 à l’Assemblée générale des Nations Unies,
le chef de l’OLP, Yasser Arafat, déclare : « Notre peuple souffre beaucoup
d’assister à la propagation du mythe selon lequel sa patrie était un désert
jusqu’à ce qu’elle fleurisse grâce au labeur des colons étrangers,
et qu’elle était en outre une terre sans peuple »[30]. De même, dans sa
« Déclaration d’indépendance » du 14 novembre 1988, le Conseil national
palestinien accuse les « forces locales et internationales » de « tenter
de propager le mensonge selon lequel « la Palestine est une terre sans peuple
»[31]. Hanan Ashrawi, porte-parole de l’OLP et ancienne doyenne de la faculté
des arts de l’université de Birzeit, estime que cette formule démontre que
les sionistes « ont cherché à nier l’existence même et l’humanité des
Palestiniens »[32]. Quant à Salman Abu Sitta, fondateur et président de la
Palestine Land Society, il qualifie le slogan de « mensonge odieux visant à
faire du peuple palestinien un groupe de sans-abri »[33].

Edward Said a cité cette phrase pour nier le droit à l’existence d’Israël
au motif que la revendication sioniste de la terre repose sur la fausse prémisse
que la Palestine était « une terre sans peuple »[34]. De nombreux disciples de
Saïd ont repris l’argument[35]. Le plus connu est peut-être Rashid Khalidi,
qui écrit : « Dans les premiers temps du mouvement sioniste, beaucoup de ses
partisans européens — et d’autres — croyaient que la Palestine était
vide et peu cultivée. Ce point de vue a été largement propagé par certains
des principaux penseurs et écrivains du mouvement, tels que Theodor Herzl,
Chaïm Nachman Bialik et Max Mandelstamm, Herzl ne mentionnant même pas les
Arabes dans son célèbre ouvrage, L’État juif. Ledit point de vue a été
résumé dans le slogan sioniste largement répandu : ‘Une terre sans peuple
pour un peuple sans terre’. »[36]

La déclaration de Khalidi est erronée sur le plan des faits. Plutôt
que de citer Der Judenstaat, il se réfère à un travail universitaire
lacunaire[37]. Herzl mentionne la population résidant en Palestine, mais
dans le contexte d’une discussion sur les emplacements possibles de son
projet d’État juif. Son analyse de l’impact politique que l’arrivée
d’habitants juifs serait susceptible d’avoir sur le projet sioniste était
prémonitoire. L’immigration, explique-t-il, « se poursuit jusqu’au
moment inévitable où la population autochtone se sent menacée et oblige le
gouvernement à mettre un terme à l’afflux de Juifs. L’immigration est
donc vaine si l’on n’a pas le droit souverain de la poursuivre »[38]. Il
est juste de dire que Herzl, à l’époque où il écrit Der Judenstaat
ne s’intéressait guère à la population existante, si ce n’est pour
évaluer son impact probable sur le sionisme. Affirmer pour autant qu’il
n’a « jamais mentionné » les Arabes de Palestine est faux. De même,
la formule « terre sans peuple » n’est jamais apparue dans les livres,
les lettres ou le journal de Herzl[39].

Khalidi pèche également par un manque de cohérence méthodologique dans
l’application des règles de grammaire. Il utilise souvent le terme «
peuple » au sens ordinaire comme quasi-synonyme de nation en écrivant :
« [l]es Palestiniens sont un peuple avec des droits nationaux »[40] ou, «
[c]e livre remarquable raconte comment les Palestiniens se sont constitués
en tant que peuple[41]. Il justifie le terrorisme de la seconde intifada en
affirmant que « la violence qui a éclaté est le résultat naturel du désir
d’indépendance d’un peuple »[42]. Khalidi ne se méprend sur le terme «
peuple » que lorsqu’il discute de la formule « une terre sans peuple »[43].

De nombreux autres universitaires et commentateurs reprennent le slogan pour
discréditer le sionisme. Le journaliste radical Ronald Bleier, par exemple,
la cite comme un exemple de « mythe de la nature sauvage » et le compare
à la propagande nazie[44]. Norman Finkelstein, polémiste anti-israélien
qui, jusqu’à ce que sa titularisation soit refusée en 2007, enseignait à
l’université DePaul de Chicago, a également associé cette expression à
un mythe de la nature sauvage[45]. Lawrence Davidson, professeur d’histoire
à l’université de West Chester en Pennsylvanie, parle de « nettoyage
ethnique au niveau conceptuel »[46]. Jacqueline Rose, professeur d’anglais
à l’université Queen Mary de Londres, qualifie l’expression de «
mensonge éhonté »[47]. Des post-sionistes comme Tom Segev et Joël Beinin,
qui s’opposent au caractère juif de l’Etat d’Israël, ont également
utilisé la critique du slogan pour étayer leurs arguments[48], tout comme
l’historien révisionniste Benny Morris[49]. Même certains sionistes ont été
conduits par ces attaques à se méprendre sur la formule. Dans Commentary,
Hillel Halkin suggère que les photographes auraient cadré l’une des
premières photos de Tel-Aviv « pour étayer les affirmations sionistes selon
lesquelles les Juifs, ‘un peuple sans terre’, retournaient en Palestine,
‘une terre sans peuple’ »[50].  Un slogan sioniste ?

Dans l’esprit de nombreux détracteurs du sionisme, la formule « une terre
sans peuple » est devenue un élément déterminant du péché originel du
sionisme. Mais dans quelle mesure ce slogan a-t-il été utilisé par les
premiers sionistes ? Selon la doxa sioniste officielle de l’époque « le
but du sionisme est de créer pour le peuple juif un foyer en Palestine garanti
par le droit public ». Les groupes sionistes ont utilisé toute une série
d’autres slogans, dont « Torah et travail », « La terre d’Israël pour
le peuple d’Israël selon la Torah d’Israël » et « Sionisme, socialisme
et émancipation de la diaspora ». Ces slogans, ainsi que « L’État juif
», « Retour à la terre », « Retour à Sion », « Patrie juive », « Une
Palestine ouverte à tous les Juifs » et « Foyer national juif » (de loin le
plus fréquent) ont été largement diffusés par les sionistes. Une recherche
dans sept grands journaux américains — Atlanta Constitution, Boston Globe,
Chicago Tribune, Los Angeles Times, New York Times, Wall Street Journal et
Washington Post[51] — permet de dénombrer plus de 3 000 mentions de la
formule « foyer national juif » jusqu’en 1948. Aucune autre formule et
aucun autre slogan sioniste ne se rapproche de ce chiffre. En revanche, on
ne dénombre que quatre mentions de la formule de Zangwill, « un pays sans
peuple »[52] toutes antérieures à 1906. Il n’est fait non plus aucune
mention de ses variantes : « terre sans peuple » ou « pays sans nation ». La
consultation de la base de données ProQuest’s Historical Newspapers permet
d’identifier une autre utilisation de la formule avant 1972 : en 1947, le «
Texte de la déclaration de Jamal el Husseini devant l’ONU sur la position
des Arabes sur la Palestine dénonçant la proposition onusienne de partition
»[53] accuse l’organisation sioniste d’avoir propagé le slogan « Donner
le pays sans peuple à un peuple sans pays ».

Malgré les affirmations de Husseini, Said et Khalidi, rien ne prouve que ce
slogan n’ait jamais été celui d’une organisation sioniste ou ait été
utilisé par l’une des figures de proue du mouvement. Seule une poignée
d’articles et de livres sionistes antérieurs à la création de l’État
y ont recours[54]. On peut s’étonner qu’une formule aussi fréquemment
attribuée aux dirigeants sionistes ne puisse être retrouvée qu’à
grand-peine dans les archives historiques[55].

Les participants au congrès sioniste de 1905 ont associé la formule avec la
personne de Zangwill[56] et il semble qu’elle soit tombée dans l’oubli
en même temps que sa proposition d’établir la patrie juive en Afrique
orientale britannique. Dans les rares cas où elle se retrouve dans une source
juive postérieure à 1905, il s’agit généralement d’une référence
spécifique au même Zangwill[57], abstraction faite des occasions où elle
apparaît lorsqu’un auteur juif cite un auteur chrétien[58].

La plupart des auteurs sont d’accord pour dire que la formule a été
brièvement utilisée dans un lointain passé. En 1914, Chaim Weizmann la
mentionne pour décrire les mentalités qui prévalaient dans les premiers
temps du mouvement[59]. L’écrivain et historien israélien Amos Elon a fait
remonter l’utilisation de cette formule par les sionistes à 1903 et précise
qu’elle a disparu du lexique dès 1917[60]. La seule utilisation de la formule
dans The Maccabean, le journal de la Fédération des sionistes américains,
date de 1901[61]. En 1922, le journaliste chrétien William Denison McCrackan
estime qu’elle est tombée en désuétude[62].

Faute de preuves d’une large utilisation par les publications et organisations
sionistes, l’affirmation selon laquelle « une terre sans peuple pour un
peuple sans terre » était un « slogan sioniste largement répandu »[63]
devrait être abandonnée.  Une terre sans peuple ?

Si Rashid Khalidi argue de l’utilisation du slogan pour accuser les dirigeants
sionistes de croire que la terre est « vide »[64], Edward Said préfère
modifier sa formulation pour alléguer que, selon les sionistes, la Palestine
est « une terre sans peuple »[65].

Mais des voyageurs tels que Keith, Blackstone, Stoddard et Zangwill (qui a
visité Israël pour la première fois en 1897 et dont le père partit y finir
ses jours) étaient bien conscients de la présence d’une population arabe,
certes peu nombreuse, que Blackstone au moins a prise en considération dans
sa conclusion qu’elle ne poserait pas d’obstacle à la restauration du
judaïsme[66]. Si certains sionistes ont cru qu’Israël était littéralement
vide, il est peu probable qu’ils l’aient fait après le débat sur les
conditions de vie en Palestine suscité par l’essai d’Ahad Ha’Am de 1891,
Truth from Eretz Yisrael [La vérité d’Eretz Yisrael][67].

Certains Juifs ont-ils imaginé la Terre d’Israël comme une contrée
abandonnée ? Peut-être. Mais il semble plus probable que les Juifs étaient
en mesure de savoir qu’il y avait suffisamment d’Arabes en Palestine
pour organiser des pogroms à Hébron et à Safed en 1834, tout en continuant
à qualifier la terre de vide. Les éditeurs de The Maccabean, par exemple,
estimaient en 1901 le nombre d’Arabes en Palestine à 150 000 seulement,
soit peut-être un tiers du chiffre réel, et suggéraient l’année suivante
qu’un tiers de la population était déjà juive. En 1905, ils qualifient
néanmoins la Palestine de « bonne terre, mais vide »[68].

Le sionisme, avec ses enthousiastes sans le sou et sans pouvoir et ses
projets grandioses de restauration d’une communauté juive, était un
mouvement de rêveurs. Le traitement du sujet par Herzl dans L’État juif
est caractéristique de ce point de vue[69]. Il ne mentionne la population
locale qu’en passant et seulement dans le contexte d’une discussion sur
les obstacles politiques qui se dressent sur le chemin de la construction
d’un État juif.

Comme l’a dit Israël Zangwill au lendemain de la Première Guerre mondiale, «
les Arabes devraient reconnaître que la route de la gloire nationale renouvelée
passe par Bagdad, Damas et La Mecque, et tous les vastes territoires qu’ils
ont libérés des Turcs, et s’en contenter… Les puissances qui les ont
libérés ont certainement le droit de leur demander de ne pas s’en prendre
à la petite bande [Israël] nécessaire à la renaissance d’un peuple encore
plus opprimé. »[70].  Diana Muir

Diana Muir est l’auteure de ‘Reflections in Bullough’s Pond: Economy
and Ecosystem in New England’ (University Press of New England, 2000). Nous
la remercions de nous avoir autorisé à traduire en français et publier ce
texte paru dans Middle East Quarterly (Printemps 2008, pp. 55-62) Notes

[1] Rashid Khalidi, Palestinian Identity: The Construction of Modern National
Consciousness (New York: Columbia University Press, 1997), p. 101.

[2] Voir par exemple, Hanan Ashrawi, Sydney Morning Herald, Nov. 6, 2003.

[3] Saree Makdisi, « Said, Palestine, and the Humanism of Liberation, »
Critical Inquiry, 31 (2005): 443; idem, « An Iron Wall of Colonization,
» Counterpunch, Jan. 26, 2005.

[4] Muhammad Muslih, The Origins of Palestinian Nationalism (New York: Columbia
University Press, 1988).

[5] Edward Said, The Question of Palestine (New York: Times Books, 1979), p. 9.

[6] Alexander Keith, The Land of Israel According to the Covenant with Abraham,
with Isaac, and with Jacob (Edinburgh: William Whyte and Co., 1843), p. 43.

[7] Whitefish, Mont.: Kessinger Publishing, 2005 (originally published in 1826).

[8] Keith, The Land of Israel According to the Covenant with Abraham, p. 43.

[9] Cité par Adam M. Garfinkle, « On the Origin, Meaning, Use, and Abuse of
a Phrase, » Middle Eastern Studies, Oct. 1991, p. 543.

[10] Shaftsbury, cité par Albert Hyamson, « British Projects for the
Restoration of Jews to Palestine, » American Jewish Historical Society
Publications, 1918, no. 26, p. 140.

[11] Edwin Hodder, The Life and Work of the Seventh Earl of Shaftsbury (London:
Cassell and Co., 1887), p. 487.

[12] Van de Velde, C.W.M., Narrative of a Journey through Syrian and Palestine
in 1851 and 1852 (Edinburgh: Wm. Blackwood and Sons, 1854), United Presbyterian
Magazine, Wm. Oliphant and Sons, Edinburgh, 1854, vol. 7, p. 403.

[13] Horatius Bonar, The Land of Promise: Notes of a Spring Journey from
Beersheba to Sidon (New York: R. Carter and Brothers, 1858), extrait de The
Theological and Literary Journal (New York), July 1858-Apr. 1859, p. 149

[14] William Blackstone, Palestine for the Jews (Oak Park, Ill.: self-pub.,
1891), republié dans Christian Protagonists for Jewish Restoration (New York:
Arno, 1977), p. 17.

[15] Sermon by C. H. Banning, cité par George Seaton Bowes, Information and
Illustration, Helps Gathered from Facts, Figures, Anecdotes, Books, etc., for
Sermons, Lectures, and Addresses (London: James Nisbett and Co., 1884), p. 128.

[16] John L. Stoddard, Lectures: Illustrated and Embellished with Views of the
World’s Famous Places and People, Being the Identical Discourses Delivered
during the Past Eighteen Years under the Title of the Stoddard Lectures,
vol. 2. (Boston: Balch Brothers Co., 1897), p. 113.

[17] Voir, par exemple, William Henry Withrow, Religious Progress in the Century
(London: Linscott Publishing Company, 1900), p. 184; Gospel in All Lands (New
York: Methodist Episcopal Church Missionary Society, Jan. 1902), pp. 199-200.

[18] Harlan Page Beach, A Geography and Atlas of Protestant Missions: Their
Environment, Forces, Distribution, Methods, Problems, Results, and Prospects
at the Opening of the Twentieth Century (New York: Student Volunteer Movement
for Foreign Missions, 1901), p. 521.

[19] Harlan Page Beach, A Geography and Atlas of Protestant Missions: Their
Environment, Forces, Distribution, Methods, Problems, Results, and Prospects
at the Opening of the Twentieth Century (New York: Student Volunteer Movement
for Foreign Missions, 1901), p. 521

[20] Augustus Hopkins Strong, Miscellanies (Philadelphia: Griffith and Rowland
Press, 1912), p. 98

[21] Garfinkle, « On the Origin, Meaning, Use, and Abuse of a Phrase, »
p. 539; Israel Zangwill, « The Return to Palestine, » New Liberal Review,
Dec. 1901, p. 615.

[22] Yaakov Ariel, On Behalf of Israel: American Fundamentalist Attitudes
toward Jews, Judaism, and Zionism, 1865-1945 (New York: Carlson Publishing,
1991), pp. 70-2.

[23] Khalidi, Palestinian Identity, p. 163

[24] Muslih, The Origins of Palestinian Nationalism, pp. 131-54.

[25] Ameen Rihani, « The Holy Land: Whose to Have and to Hold? » The Bookman,
Jan. 1918, p. 10

[26] Norman Dwight Harris, Europe and the East (Boston: Houghton Mifflin,
1926), p. 93

[27] William Denison McCrackan, The New Palestine: An Authoritative Account
of Palestine since the Great War (Boston: Page Company, 1922), p. 250

[28] Martin Buber, A Land of Two Peoples: Martin Buber on Jews and Arabs,
Paul Mendes-Flohr, ed. (Chicago: University of Chicago Press, 2005), p. 14

[29] Sami Hadawi, Bitter Harvest, Palestine between 1914 and 1967 (New York:
New World Press, 1967), p. 10; Izzat Tannous, The « Activities » of the
Hagana, Irgun, and Stern Gang: As Recorded in British Command Paper No. 6873
(New York: Palestine Liberation Organization, 1968), p. 3.

[30] Walter Laqueur and Barry Rubin, eds., The Israel-Arab Reader: A Documentary
History of the Middle East Conflict (New York: Penguin, 2001), pp. 174-5.

[31] « Palestinian National Council Declaration of Independence, » Algiers,
Nov. 14, 1988.

[32] The Sydney Morning Herald, Nov. 6, 2003

[33] Matt Horton, « The Atlas of Palestine 1948, » The Washington Report on
Middle East Affairs, Aug. 2005, p. 58.

[34] Said, The Question of Palestine, p. 9.

[35] Par exemple, Saree Makdisi, « Israel’s Fantasy Stands in Way of Peace, »
The Arab American News (Dearborn), Feb. 5-Feb. 11, 2005; Nur Masalha, Expulsion
of the Palestinians: The Concept of « Transfer » in Zionist Political Thought
(Washington, D.C.: Institute for Palestine Studies, 1992), p. 6.

[36] Khalidi, Palestinian Identity, p. 101.

[37] Khalidi se réfère à Anita Shapira, Land and Power: The Zionist Recourse
to Force, 1881-1948 (New York: Oxford University Press, 1992), p. 41.

[38] Theodor Herzl, The Jewish State, Sylvie d’Avigdor, trans. (London :
Nutt, 1896); idem, The Jewish State, Sylvie d’Avigdor, trans. (New York:
Dover, 1988), p. 95.

[39] Garfinkle, « On the Origin, Meaning, Use and Abuse of a Phrase, » p. 539.

[40] Rashid Khalidi, « Observations on the Right of Return, » Journal of
Palestine Studies, Winter 1992, p. 30

[41] Rashid Khalidi, jacket blurb for Baruch Kimmerling and Joel S. Migdal,
The Palestinian People: A History (Cambridge: Harvard University Press, 2003).

[42] Rashid Khalidi, « To End the Bloodshed, » Christian Century, Nov. 22-29,
2000, p. 1206

[43] Khalidi, Palestinian Identity, p. 101

[44] Ronald Bleier, review of « Image and Reality of the Israel-Palestine
Conflict, » Middle East Policy, Oct. 1999, p. 195.

[45] Norman Finkelstein, Image and Reality of the Israel-Palestine Conflict
(London: Verso Books, 1995), p. 95.

[46] Lawrence Davidson, « Christian Zionism as a Representation of American
Manifest Destiny, » Critique: Critical Middle East Studies, Summer 2005, p. 161.

[47] Jacqueline Rose, The Question of Zion (Princeton: Princeton University
Press, 2005), p. 44.

[48] Tom Segev, One Palestine, Complete: Jews and Arabs under the British
Mandate (New York: Owl Books, 2001), p. 493; Joel Beinin, « Political Economy
and Public Culture in a State of Constant Conflict: Fifty Years of Jewish
Statehood, » Jewish Social Studies, July 31, 1998, p. 96.

[49] Benny Morris, Righteous Victims: A History of the Zionist Arab Conflict,
1881-2001 (New York: Vintage, 2001), p. 42

[50] Hillel Halkin, « The First Hebrew City, » Commentary, Feb. 2007, p. 57.

[51] ProQuest Historical Newspapers database, accessed Nov. 27, 2007.

[52] The New York Times, Nov. 23, 1901, May 20, 1903; The Chicago Daily Tribune,
Dec. 22, 1901; The Washington Post, Aug. 27, 1905.

[53] The New York Times, Sept. 30, 1947.

[54] Voir Israel Herbert Levinthal, Judaism, An Analysis and An Interpretation
(New York and London: Funk and Wagnalls, 1935), p. 254; Morris Silverman, ed.,
Sabbath and Festival Prayerbook with a New Translation, Supplementary Readings,
and Notes (New York: Rabbinical Assembly of America and the United Synagogue of
America, 1946), p. 324; Max Raisin, A History of the Jews in Modern Times (New
York: Hebrew Publishing Company, 1919), p. 356; The Zionist Review, Apr. 1918,
p. 231; Leonard Mars, « The Ministry of the Reverend Simon Fyne in Swansea:
1899-1906, » Jewish Social Studies, Winter/Spring 1988, p. 92.

[55] Alan Dowty, The Jewish State, A Century Later (Berkeley: University of
California Press, 2001), p. 267.

[56] The Washington Post, Aug. 27, 1905.

[57] Voir « The Restoration of Judea, » New York Globe editorial, May 1, 1917,
repris dans Zionism Conquers Public Opinion (New York: Provisional Executive
Committee for General Zionist Affairs, 1917), p. 16; Richard James Horation
Gottheil, Zionism (Philadelphia: Jewish Publication Society of America, 1914),
p. 139

[58] Walter M. Chandler statement, The American War Congress and Zionism:
Statements by Members of the American War Congress on the Jewish National
Movement (New York: Zionist Organization of America, 1919), p 154.

[59] Paul Goodman, Chaim Weizmann : A Tribute on His Seventieth Birthday
(London: V. Gollancz, 1945), p. 153.

[60] Amos Elon, The Israelis: Founders and Sons (New York: Holt, Reinhart,
Winston, 1971), p. 149

[61] Raphael Medoff, American Zionist Leaders and the Palestinian Arabs,
1898-1948 (Ph.D. diss., Yeshiva University, 1991), p. 17

[62] McCrackan, The New Palestine, p. 250

[63] Khalidi, Palestinian Identity; p. 101

[64] Ibid.

[65] Said, The Question of Palestine, p. 9

[66] Ariel, On Behalf of Israel, p. 74.

[67] Alan Dowty, « Much Ado about Little: Ahad Ha’am’s ‘Truth from Eretz
Yisrael,’ Zionism, and the Arabs, » Israel Studies, Fall 2000, pp. 154-81.

[68] Medoff, American Zionist Leaders and the Palestinian Arabs, p. 19

[69] Shapira, Land and Power, p. 51

[70] Israel Zangwill, The Voice of Jerusalem (New York, Macmillan and Company,
1921) p. 110

