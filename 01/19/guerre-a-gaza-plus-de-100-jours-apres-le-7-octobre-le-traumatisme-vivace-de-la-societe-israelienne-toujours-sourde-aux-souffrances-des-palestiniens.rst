

.. _imbert_2024_01_19:

==============================================================================================================================================================================
2024-01-19 Gaza : Plus de cent jours après le 7 octobre 2023, le traumatisme vivace de la société israélienne, toujours sourde aux conséquences meurtrières de la guerre
==============================================================================================================================================================================

- https://www.lemonde.fr/international/article/2024/01/19/guerre-a-gaza-plus-de-100-jours-apres-le-7-octobre-le-traumatisme-vivace-de-la-societe-israelienne-toujours-sourde-aux-souffrances-des-palestiniens_6211721_3210.html


Auteur Louis Imbert
=====================

- https://www.lemonde.fr/signataires/louis-imbert/


Gaza : Plus de cent jours après le 7 octobre 2023, le traumatisme vivace de la société israélienne, toujours sourde aux conséquences meurtrières de la guerre
=================================================================================================================================================================

Guerre à Gaza : plus de 100 jours après le 7 octobre, le traumatisme vivace
de la société israélienne, toujours sourde aux conséquences meurtrières
de la guerre

Par Louis Imbert (Tel-Aviv, Sdérot, Ramat Yohanan (Israël), envoyé spécial)


L’immense majorité des Israéliens jugent que le conflit leur a été imposé
par le massacre de 1 200 civils et militaires par le Hamas, et préfèrent
ignorer les destructions et les morts liées aux opérations militaires dans
l’enclave palestinienne.  Tel-Aviv reprenait vie. La grande cité du bord de
mer paraissait oublier la guerre qui se poursuit à Gaza depuis trois mois, à
50 kilomètres au sud. Début janvier, Michal Feldon s’est résolue à dîner
en ville, avec son compagnon, après trois mois passés à se morfondre. Cette
pédiatre a confié ses jumeaux à une babysitter, puis elle a renoncé
devant les terrasses illuminées. Pas le cœur. Elle est rentrée chez elle,
pour échanger avec quelques amis sur Internet : « J’ai trois personnes au
monde avec qui parler depuis le début de la guerre », soupire-t-elle.

Michal appartient à une minorité négligeable en Israël : quelques
centaines de personnes qui manifestent chaque semaine contre la guerre, sous
haute protection policière. Lorsqu’elle quitte ces rassemblements, elle
cache ses banderoles appelant à « la paix », de crainte d’être prise
à partie. Ses concitoyens l’effraient, qui vivent comme une nécessité
existentielle l’éradication de toute menace posée par le Hamas. Unanimement,
ils jugent que cette guerre leur a été imposée par le massacre de 1 200
civils et militaires le 7 octobre 2023. Cette guerre est juste, ils n’en
doutent pas et en assument les conséquences.

A l’étranger, des experts militaires comparent cette campagne à la
destruction de la ville allemande de Dresde par les Alliés en 1945, et à la
prise de Grozny, en Tchétchénie, par l’armée russe en 1995. Au moins 24
000 Palestiniens ont été tués, soit 1 % de la population de la bande de
Gaza, pour la plupart des femmes et des enfants. Israël met ces morts sur
le compte du Hamas, qui se terre parmi les civils. Le pays se perçoit comme
un malade du cancer acharné à détruire sa tumeur. Il fait mine d’ignorer
qu’il impose ce traitement à un corps étranger, palestinien. Des discours
aux accents génocidaires se sont banalisés à une vitesse sidérante au plus
haut niveau de l’Etat, comme aux machines à café de l’hôpital Shamir,
près de Lod (centre), où travaille Michal Feldon.

La pédiatre a été épouvantée par un confrère, homme libéral et modéré,
qui assénait dès le 10 octobre 2023 : « Nous devons bombarder tous les
hôpitaux de l’enclave. Le Hamas s’y cache, il n’y a pas d’autre
moyen. » Sur un réseau social, début janvier, Michal a publié cette
statistique atroce : selon l’Unicef, 1 000 enfants ont été amputés d’une
jambe ou des deux à Gaza depuis le début des bombardements. « Des amis m’ont
répondu qu’il n’y a pas d’innocents là-bas, qu’ils ont torturé les
nôtres le 7 octobre et qu’ils torturent encore nos otages », regrette-t-elle.

Un souvenir lui est alors revenu en mémoire : « Il y a vingt ans, deux
enfants palestiniens s’étaient électrocutés à Gaza. C’était un
accident. Ils avaient été transférés en Israël et amputés dans mon
hôpital. Un généreux anonyme, israélien, avait promis de financer à
vie leurs prothèses. Face à deux enfants, nous avions de l’empathie ;
aujourd’hui, ils sont mille et nous en sommes incapables. » Michal est
issue d’une famille de militants. Sa tante, la journaliste Yael Lotan, a
été condamnée en justice pour avoir osé rencontrer des cadres exilés de
l’Organisation de libération de la Palestine, en 1986, en Roumanie. Sa mère,
historienne, tâche de rassurer Michal : « A 80 ans, elle a vu d’autres
guerres et je voudrais la croire lorsqu’elle prétend que la tempête passera
et que les gens retrouveront leurs esprits. »

« Cette journée ne s’est jamais vraiment terminée »
========================================================

Le 7 octobre 2023 est un évènement inouï pour Israël et la guerre n’est
que son onde de choc. « Cette journée ne s’est jamais vraiment terminée. Je
ne sais pas quand ce sera le cas », s’interroge Ofer Gitai. Coordinateur
de sécurité du kibboutz de Beeri, il a échappé avec son épouse et ses
trois filles au Hamas, en se cachant. Durant quinze heures, il a assuré à
ses voisins, par texto, que l’armée était sur le point d’arriver. Le 16
janvier, le kibboutz a annoncé que deux otages, capturés à Beeri et emportés
à Gaza, étaient morts en captivité. Cinq autres membres de la communauté
y demeurent prisonniers, parmi une centaine d’Israéliens et d’étrangers.

La famille Gitai s’est repliée dans un kibboutz du nord, à Ramat
Yohanan. Comme eux, plus de 200 000 déplacés ignorent quand ils pourront
rentrer chez eux, au pourtour de Gaza mais aussi à la frontière libanaise,
soumis aux tirs réguliers du Hezbollah. Lorsque Ofer allume sa télévision,
l’actualité lui paraît arrêtée au premier jour de cette tragédie :
« C’est la même chose encore et encore, chaque journal d’information
s’ouvre sur une famille d’otages ; ils évoquent tel épisode des attaques
sur un village – il y en a tant. Puis ils montrent des images des ruines de
Gaza, mais pas les vidéos que filment les Gazaouis. Je ne vois pas de civils
palestiniens sur mon écran. »

Cependant, jamais une nation n’a disposé de tant d’informations sur
un conflit, en temps réel. Des parents et des amis d’Ofer, réservistes,
sont déployés à Gaza, comme des milliers d’autres. Il échange avec eux
par textos. Ces soldats rentrent pour des permissions de week-end et parlent
à leurs proches. Ofer regarde souvent les vidéos de Gaza en ruines et du
quotidien des positions militaires, qu’ils attachent à leurs profils en ligne.

Ce conflit a beau se dérouler en territoire palestinien, c’est la mémoire
d’Israël et ses traumatismes élémentaires qu’il travaille. « Mes
beaux-parents se sont cachés dans leur grenier le 7 octobre. Ils ont pensé à
Anne Frank, raconte Ofer Gitai. Nous nous sommes sentis abandonnés, faibles et
vulnérables, sans police ni Etat. Certains ont encore ce sentiment trois mois
plus tard. » La promesse de protection de l’Etat juif, proclamé en 1948,
dans la foulée de la Shoah, s’est écroulée. Les Israéliens voudraient
corriger, oublier ou venger cela à Gaza.

Ofer en convient : « Il est plus facile de ne pas différencier combattants du
Hamas et civils gazaouis, cela évite de se faire des cicatrices à l’âme. »
Cette confusion est ancienne. En isolant l’enclave palestinienne dans un
blocus strict depuis 2007, Israël a oublié ses habitants, dont il a pourtant la
responsabilité en tant que puissance occupante. « La plupart des Israéliens
ignoraient Gaza derrière son mur. A Beeri, cela n’a jamais été possible,
rappelle Ofer. Avant 2007, des gens du kibboutz allaient faire des courses à
Gaza. Puis le Hamas a tiré régulièrement ses roquettes sur nous. Mais même
pendant la guerre, certains voisins sont restés en contact avec des travailleurs
gazaouis qui ont été renvoyés dans l’enclave après le 7 octobre. »

« Même pour les “woke”, un viol devrait être un viol »
=============================================================

L’armée a commencé, en décembre 2023, à raser une part des quartiers
orientaux de Gaza pour y ménager une profonde « zone tampon ». Ce n’est
pas assez pour le maire de Sdérot, Alon Davidi. Cet édile, issu de la droite
religieuse, exige que la moitié nord de Gaza demeure vide, tant que ses
administrés ne seront pas rentrés chez eux. « Je sais ce qu’il s’est
passé le 7 octobre. Beaucoup de Gazaouis ne soutiennent pas seulement le
Hamas. Ils en font partie », estime l’infatigable promoteur de sa ville, qui
croit pouvoir ranimer les vastes chantiers immobiliers désertés de Sdérot. De
l’hôtel où il réside à Jérusalem, le maire ferraille contre l’Etat,
qui menace de couper les subsides de ses électeurs, afin de les contraindre à
rentrer chez eux. Il imagine à l’avenir un maillage du territoire de Gaza par
l’armée, un tri puis un contrôle serré de ses habitants, aussi longtemps
que nécessaire. Il souhaite que la citadelle Israël rebâtisse ses contreforts.

« Israël est une île livrée à elle-même », constate, en soupirant,
Yifat Bitton. Cette juriste, spécialiste des violences sexuelles, s’alarme
du gouffre d’incompréhension que la guerre creuse entre son pays et le
reste du monde. En décembre 2023, elle a tenté d’y jeter un pont. Elle
s’est rendue au siège des Nations unies, à New York, pour évoquer les
victimes de sévices sexuels du Hamas le 7 octobre 2023. Elle ne comprenait
pas qu’ONU Femmes, organe voué à sensibiliser à ces violences, ait attendu
deux mois pour les mentionner, dans un communiqué liminaire. « Leur silence
offrait du carburant à ceux qui niaient les crimes du Hamas. Il légitimait
la non-reconnaissance du mal qui avait été fait », cingle cette activiste.

Le 4 décembre 2023, Mme Bitton s’exprime dans un hall bondé, dans le cube
de verre des Nations unies, tache de lumière sur le morne boulevard qui domine
l’East River. La femme d’affaires Sheryl Sandberg et l’ancienne secrétaire
d’Etat Hillary Clinton disent leur soutien en tribune. L’évènement est
coorganisé par le représentant d’Israël aux Nations unies, Guilad Erdan,
qui a suscité la consternation en Israël, peu avant, en arborant une étoile
jaune, marquée des mots « plus jamais ça », durant un Conseil de sécurité.

« Je ne suis pas aveugle, précise Mme Bitton, je sais que mon discours est
utilisé par le gouvernement israélien », qui s’efforce de légitimer sa
conduite de la guerre, alors que 2 millions de Gazaouis errent dans l’enclave,
menacés par la faim et les maladies. « Mais cela ne m’empêche pas, en
tant que défenseuse des droits humains, de présenter les faits dans leur
complexité, avec nuance », estime-t-elle. Depuis deux mois, cette experte
collaborait avec les enquêteurs de la police, de l’armée et du renseignement,
qui avaient tardé également à prendre ces drames en compte.

A New York, Mme Bitton souhaite aussi répondre aux progressistes, qui dénoncent
la guerre israélienne sur les campus. « Ils ont effacé le 7 octobre et
sont vite passés au 8 octobre. Il était pour moi inimaginable que certains
perçoivent de telles horreurs comme un acte de libération. C’est pour
cela qu’il fallait amener les violences sexuelles [dans le débat public] :
même pour les “woke”, un viol devrait être un viol et ne saurait être
toléré en aucune circonstance. » Le 2 janvier, Mme Bitton s’est réjouie
de la démission de Claudine Gay, la première présidente noire d’Harvard,
condamnée par sa défense empêtrée de la liberté d’expression face à des
débordements antisémites. Mais l’Israélienne redoute que ce psychodrame
n’aggrave la longue rupture des progressistes américains avec son pays.

« De la colère contre l’impréparation de l’Etat »
=======================================================

Fille d’une Marocaine et d’un Yéménite, engagée pour les droits des juifs
originaires du monde arabe en Israël, Yifat Bitton craint que son pays se
découvre bien seul après guerre, en proie à ses démons. « Nos divisions
sont si profondes, incompréhensibles à l’extérieur, et aujourd’hui
personne ne veut prêter l’oreille à ces complexités. Même le traumatisme
du 7 octobre n’est pas un dénominateur commun. J’ai peur que nous nous
déchirions tout à fait après-guerre », avance-t-elle.

Pour l’heure, le conflit impose encore une union nationale de façade. «
Le public ne veut pas de lutte politique. Il ne veut pas que le gouvernement
tombe », estime Nimrod Dweck. Durant huit mois, jusqu’au 6 octobre 2023, cet
ancien directeur d’un incubateur de start-up à Tel-Aviv a manifesté dans les
rues. Son mouvement, Darkenu, participait aux protestations contre la réforme
de la justice voulue par le gouvernement de Benyamin Nétanyahou. Le conflit
a signé le coup d’arrêt de ce projet dénoncé comme liberticide. Mais
nombre d’Israéliens considèrent encore le premier ministre et ses alliés
ultranationalistes et religieux comme la principale menace à la sécurité
de l’Etat.

Darkenu prétend influencer « la droite modérée », afin de ralentir
la longue dérive identitaire du pays. Dès le 7 octobre 2023, il s’est
investi dans l’effort de guerre, avec les principales organisations de
manifestants. M. Dweck garde « de la colère contre l’impréparation de
l’Etat », à laquelle la société civile a pallié en aidant à évacuer les
habitants de la périphérie de Gaza, puis à distribuer des repas et de l’aide
dans les centres d’accueil. Son mouvement a exigé la fermeture de ministères
pléthoriques et a accusé M. Nétanyahou d’avoir affaibli l’administration,
en distribuant des prébendes à ses alliés fondamentalistes.

« Cette guerre a fait éclater une bulle d’illusions »
===========================================================

Le cap des 100 jours de guerre a été passé le 14 janvier. Israël s’enlise
dans un conflit long et coûteux. Le Hamas n’a pas été « détruit »,
les otages demeurent à Gaza et le gouvernement se divise : des élections
législatives sont de plus en plus probables. Mais seuls M. Nétanyahou et
l’extrême droite osent déjà faire campagne. Le premier ministre blâme
les généraux pour l’écroulement du 7 octobre 2023. Ses alliés exigent un
nettoyage ethnique à Gaza et la recolonisation de l’enclave. Ils refusent
tout accord partiel avec le Hamas pour libérer les otages. Ils promettent
une guerre perpétuelle à Gaza et en Cisjordanie. Face à eux, les partis du
centre et de gauche sont aphones, et les meneurs des manifestations de 2023
se veulent consensuels et non partisans.

Adolescent, Nimrod Dweck buvait des bières avec ses copains de Modiin (centre),
au-dessus d’une vallée qui donne sur la Cisjordanie toute proche. «
Mon cauchemar à l’époque, c’était que les Arabes brisent le mur et
envahissent le quartier », se rappelle-t-il. Aujourd’hui, ses deux enfants
dorment dans son lit, à Tel-Aviv : il peut les emmener rapidement à l’abri si
les sirènes du Dôme de fer, le système de défense antiaérienne israélien,
retentissent. Allumer son téléphone le matin reste « le pire moment de la
journée » : il craint d’apprendre la mort d’un sixième camarade de
lycée engagé à Gaza, où près de 200 soldats ont été tués. « Cette
guerre a fait éclater une bulle d’illusions. Tu pouvais peut-être voyager
trois fois par an en Europe et Tel-Aviv était l’une des villes les plus
chères du monde, mais plein de gens pensaient à émigrer, surtout dans la
bulle des élites “tech”. Israël avait l’air confortable mais il ne
l’était pas, notamment à cause du coût de la sécurité. »

M. Dweck imagine sans mal des années d’opération militaire, de plus
faible intensité, « avant qu’une nouvelle réalité n’émerge à
Gaza ». Cela lui semble acceptable. Il sait que ses concitoyens parleront
longtemps de « sécurité » avant tout. « Mais si Gaza est invivable,
les murs n’empêcheront pas le chaos de nous atteindre. Plus de 70 % des
gens veulent une solution politique là-bas, tout en y maintenant la liberté
d’action de l’armée », croit-il, sans s’appesantir sur la contradiction
insondable de ces deux exigences.

Louis Imbert (Tel-Aviv, Sdérot, Ramat Yohanan (Israël), envoyé spécial)
