

.. _guiton_le_devin_2024_01_05:

============================================================================================================================================================================
2024-01-05 **Entretien Le renseignement israélien face au 7 octobre : «Parler de failles est un euphémisme… Ce sont des trous noirs»** par Amaelle Guiton et Willy Le Devin
============================================================================================================================================================================

- https://www.liberation.fr/international/moyen-orient/le-renseignement-israelien-face-au-7-octobre-parler-de-failles-est-un-euphemisme-ce-sont-des-trous-noirs-20240105_FEIAZ4SIKBFTDM2E7RZJB6A3ZU/

Entretien Le renseignement israélien face au 7 octobre : «Parler de failles
est un euphémisme… Ce sont des trous noirs» 

Le journaliste d’investigation israélien Ronen Bergman, à l’origine de
nombreuses révélations sur la faillite sécuritaire de l’Etat hébreu,
revient pour «Libération» sur l’échec du renseignement, qui s’est
mépris sur les intentions du Hamas à Gaza et a gravement sous-estimé ses
capacités opérationnelles.  


Il est sans conteste l’un des meilleurs connaisseurs des services de sécurité
de son pays, et l’auteur, notamment, d’un ouvrage de référence consacré
à l’histoire des assassinats ciblés commandités par Israël, Lève-toi
et tue le premier (Grasset, 2020). Depuis les massacres perpétrés le 7
octobre par le Hamas dans le sud d’Israël (1 140 morts, dont une majorité
de civils), Ronen Bergman, journaliste au New York Times et à Yediot Aharonot,
le premier quotidien israélien, enquête pour comprendre la dramatique faillite
sécuritaire de l’Etat hébreu. Il révélait ainsi, dès le 30 novembre
dans les colonnes du New York Times, que le renseignement israélien s’était
procuré, en 2022, un document du Hamas présentant un plan d’attaque similaire
à celui mis en œuvre l’année suivante. Sauf que le scénario a alors été
jugé irréaliste… Le journaliste d’investigation a également cosigné,
le 10 décembre, une longue enquête sur la manière dont Benyamin Nétanyahou,
tout à sa volonté d’affaiblir l’Autorité palestinienne en Cisjordanie,
a encouragé les financements qataris vers la bande de Gaza. Trois mois après
l’attaque terroriste sans précédent déclenchée par l’organisation
palestinienne, Bergman revient, pour Libération, sur les racines et les
conséquences du fatal échec des services israéliens.

Que peut-on dire aujourd’hui des raisons pour lesquelles Israël a été
incapable de prévenir les massacres du 7 octobre ?

Nous ne savons pas tout, mais nous en savons désormais beaucoup. Nous
avons connaissance d’un grand nombre de documents et nous savons aussi,
dans une certaine mesure, comment le Hamas a envisagé et planifié les
événements. Il y a plusieurs composantes essentielles à cette faillite
sécuritaire, toutes connectées les unes aux autres. La première, c’est
l’échec du renseignement. Dès le 7 au soir, j’ai eu une conversation avec
un haut gradé qui m’a assuré que la surprise avait été totale. Je lui
ai répondu : ce n’est pas que je ne vous crois pas, mais je suis certain
que tôt ou tard, d’autres éléments émergeront, des erreurs d’analyse,
car ce n’est pas une petite incursion, ce n’est pas un raid, c’est une
invasion, c’est la guerre… Il me semblait inconcevable qu’une action de
cette ampleur soit passée complètement inaperçue.

Justement, vous avez révélé que le renseignement israélien avait eu
connaissance d’un plan d’attaque du Hamas, un document désigné par les
autorités sous le nom de code «Mur de Jéricho», mais ne l’avait pas
jugé crédible. Comment l’expliquez-vous ?

Il faut absolument garder en tête, pour comprendre ce qui s’est passé, que
les intentions d’un acteur et ses capacités sont deux choses différentes. Le
«Mur de Jéricho» ne répondait pas à la question des intentions du Hamas –
veut-il attaquer ou non ? – mais portait uniquement sur ses capacités. Or, la
première erreur du renseignement a été, pour simplifier, une incompréhension
totale des intentions de Yahya Sinwar [le chef du Hamas dans la bande de Gaza,
ndlr]. Les responsables des services de sécurité avaient bien identifié
un risque majeur pour Israël, lié à la crise politique provoquée par la
réforme judiciaire : tout au long de l’année qui a précédé la guerre,
ils ont averti Benyamin Nétanyahou que cette crise, et ses conséquences dans
l’armée de réserve [de nombreux réservistes se sont mobilisés contre
la réforme, ndlr], pourrait être vue par les ennemis d’Israël comme une
fenêtre d’opportunité pour attaquer. Le Premier ministre n’a tenu aucun
compte de ces alertes. Mais jusqu’au 7 octobre, l’approche générale était
que les ennemis d’Israël, s’ils pouvaient envoyer des commandos-suicides,
n’étaient pas eux-mêmes suicidaires : ceux d’entre eux qui contrôlent un
Etat ou exercent un pouvoir politique comprennent qu’une guerre totale avec
Israël les met en grand risque. Le Hezbollah illustre bien ce pragmatisme quand
il s’agit de sa propre existence : il sait qu’entrer en guerre aboutirait
à la destruction de certaines parties du Liban, que cela ruinerait tout ce
pour quoi il travaille. Israël pensait la même chose du Hamas : qu’il
pouvait être contenu, dissuadé. Cinq jours seulement avant la guerre, le
conseiller à la sécurité nationale de Nétanyahou déclarait que le Hamas
comprenait «le prix de la provocation»…

Et cette incompréhension des intentions de Yahya Sinwar aurait pesé sur
l’évaluation, par les services de sécurité, de ce dont le Hamas était
capable ?

Dans le monde du renseignement, l’évaluation des intentions et l’évaluation
des capacités devraient être totalement séparées. Cela reste bien sûr
à prouver, mais il me semble que, lorsque vous pensez que quelqu’un ne va
pas attaquer, vous risquez d’attribuer moins d’importance aux signes de
préparation d’une attaque, et au recueil des informations allant dans ce sens.

L’été dernier, une analyste de l’unité de renseignement électronique
de l’armée a pourtant tiré la sonnette d’alarme, mais elle n’a pas
été écoutée… Que s’est-il passé ?

La découverte du «Mur de Jéricho» aurait pu être la plus grande réussite
du renseignement israélien : c’était, en 2022, une vue de l’intérieur
des plans de guerre secrets du Hamas. Or, ce document a été compris non pas
comme un plan viable, mais comme [ce que les analystes militaires appellent]
une «boussole pour la construction de la force» : ce que le Hamas souhaitait
devenir, et non là où il en était. Et la différence entre l’évaluation
des capacités du Hamas par l’armée israélienne à l’époque et ce que
décrit le document est immense… En novembre 2022, les militaires israéliens
évaluaient entre 2 000 et 3 000 le nombre de [membres des] commandos Nukhba
[les unités d’élite du Hamas, ndlr] prêts à envahir Israël, mais
estimaient qu’il ne pouvait en envoyer que 70, depuis deux points le long
de la frontière. Le «Mur de Jéricho» parle de 1 680 combattants envoyés
depuis 60 lieux… En juillet 2023, une analyste chevronnée de l’unité
8200, qui surveillait les exercices du Hamas, a alerté ses responsables :
pour elle, le «Mur de Jéricho» n’était pas l’expression d’un désir,
d’un rêve, mais présentait les capacités actuelles du Hamas, qui avait
acquis selon elle une compétence militaire nouvelle. Mais ce qu’elle disait
a été balayé par l’un des colonels destinataires de son message. Elle a
pourtant insisté, mais sans succès. Et son avertissement n’est pas sorti
de cet échange de mails chiffrés. Les hauts responsables n’en ont pas eu
connaissance, ce qui est, là encore, un échec majeur.

Dans quelle mesure ces erreurs d’appréciation, tant sur les intentions que
sur les capacités opérationnelles du Hamas, ont-elles eu pour conséquence
des failles dans la collecte d’informations ?

Parler de failles est un euphémisme… Ce sont des trous noirs ! Dans la nuit
qui a précédé l’attaque, le Hamas a mobilisé 2 000 commandos. Chacun
d’entre eux a généralement une famille. Cela veut dire que probablement
5 000 à 10 000 personnes pouvaient penser que quelque chose était en train
de se produire. C’est un échec massif de la collecte d’information.

Est-ce un problème de capacités du renseignement israélien, ou de mauvais
choix de priorités ? Vous avez écrit que l’unité 8200, à l’époque,
avait cessé depuis des mois de surveiller les communications radio des
militants du Hamas…

C’est tout cela à la fois sans doute, mais c’est aussi un manque
d’imagination, de capacité à réfléchir hors des sentiers battus. Israël
n’a jamais envisagé une situation dans laquelle Yahya Sinwar et quelques
autres individus dans la bande de Gaza prendraient une décision et ne la
partageraient avec personne – une décision qui changerait l’histoire du
Proche-Orient, voire l’histoire du monde. Et justement, si ceux qui ont pris
cette décision avaient consulté les leaders politiques du Hamas qui sont hors
de Gaza, ou l’Iran, ou le Hezbollah, Israël l’aurait su. C’était la
supériorité qu’Israël pensait avoir en matière de renseignement. Mais
Sinwar avait justement compris que s’il voulait infliger une défaite à
Israël, il ne le pouvait que par une surprise totale…

Et l’armée a en effet été prise de court…

Le quota de forces positionnées à la frontière avec Gaza avait été calculé
en fonction d’une certaine menace : le scénario le plus pessimiste, c’était
celui d’infiltrations en deux, trois ou quatre points, avec à chaque fois
une cinquantaine de [membres des] commandos Nukhba… L’armée n’était
pas du tout préparée à la menace réelle. Il faut ajouter à cela un long
processus de corrosion du niveau de vigilance des soldats et des commandants
présents à la frontière, dû à la clôture de sécurité.

C’est un point qui a été souvent évoqué : l’excès de confiance, dans
le renseignement électronique d’une part, dans les systèmes de sécurité
à la frontière d’autre part.

Je suis d’accord sur ces deux points. La confiance dans la supériorité du
renseignement israélien, c’est ce qui est au cœur de la surprise de la
guerre du Kippour en 1973, et de la surprise d’octobre 2023. Le 6 octobre
dernier, à l’occasion du 50e anniversaire de la guerre du Kippour, j’ai
supervisé la publication de deux tribunes dans Yediot Aharonot. L’une était
signée du major général Aharon Zeevi-Farkash, ancien chef du renseignement
militaire [de 2002 à 2006, ndlr], et l’autre de ma plume. Chacun avec ses
propres mots, nous disions tous deux qu’à cause du péché d’orgueil,
et à cause des lacunes du travail d’enquête sur la guerre de 1973, nous
risquions de subir une nouvelle attaque surprise de la part d’un de nos
ennemis. Et cela, à la veille de la guerre en cours ! On nous a demandé
depuis : comment saviez-vous ? Bien sûr, nous ne savions rien, nous tirions
simplement les enseignements de ce qui s’est passé il y a cinquante ans. Un
jour, lorsque les gens découvriront les documents classifiés de la guerre
d’octobre 2023, ils seront choqués de voir à quel point elle est similaire,
dans ses composantes, à ce qui s’est produit en 1973.

Comment décririez-vous le débat sur la faillite sécuritaire du 7 octobre
aujourd’hui en Israël ?

Dans l’opinion publique, Nétanyahou est considéré comme le principal
responsable, même si la responsabilité des chefs du renseignement militaire, du
Shin Bet, de la défense, est aussi pointée, et ce pour de bonnes raisons. Mais
Nétanyahou a poursuivi sa politique alors même qu’il avait été alerté
des risques qu’elle créait, et il a permis au Hamas de continuer à exister
et de se renforcer grâce à l’argent du Qatar. Il avait un agenda politique
: affaiblir l’Autorité palestinienne, garder un Hamas fort mais pas trop,
pour ne pas avoir à négocier… Aujourd’hui, on assiste à une opération
massive, sur les médias sociaux, pour tenter de décharger Nétanyahou de
toute responsabilité et tout mettre sur le dos des chefs de l’armée et
du renseignement.

Et quelles seront les conséquences à moyen et long terme ? Il y aura
certainement une commission d’enquête ?

Israël va devoir réécrire sa politique stratégique de défense. Pour beaucoup
d’Israéliens, nous ne pourrons plus considérer que ces organisations
islamistes ne sont pas nihilistes, puisque l’une d’elles [le Hamas]
a commis, comme je le disais, une forme de suicide. Et les différents
échecs – militaires, opérationnels, du renseignement – conduiront à une
réorganisation totale de l’establishment de la défense israélienne. Une
commission d’enquête conclura probablement à la responsabilité de quelques
hauts responsables, y compris Nétanyahou. Mais ce travail prendra du temps,
or le rythme des événements est beaucoup plus rapide aujourd’hui, avec les
médias et les réseaux sociaux, qu’à l’époque de la guerre du Kippour
; les effets politiques et personnels de la guerre actuelle se manifesteront
sans doute bien avant qu’un rapport d’enquête ne soit rendu public.
