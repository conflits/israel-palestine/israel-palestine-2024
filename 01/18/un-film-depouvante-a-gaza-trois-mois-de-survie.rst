

.. _kodmani_masseguin_2024_01_18:

=========================================================================================================================================================
2024-01-18 **Guerre Israël-Hamas «Tel un film d’épouvante» : pour les Gazaouis, trois mois de morts et de survie** par  Hala Kodmani et Léa Masseguin
=========================================================================================================================================================

- https://www.liberation.fr/international/moyen-orient/un-film-depouvante-a-gaza-trois-mois-de-survie-20240118_OVY7ABI6L5EMHN2CE6MD3LYICE/

Auteures
============

- https://www.liberation.fr/auteur/hala-kodmani/
- https://www.liberation.fr/auteur/lea-masseguin/

par Hala Kodmani et Léa Masseguin

Guerre Israël-Hamas «Tel un film d’épouvante» : pour les Gazaouis, trois mois de morts et de survie
=======================================================================================================

Guerre Israël-Hamas «Tel un film d’épouvante» : pour les Gazaouis,
trois mois de morts et de survie

La guerre entre le Hamas et Israëldossier Face à l’offensive israélienne,
la quasi-totalité des 2 millions de Gazaouis ont fui leur maison. Escalade
des violences, pénurie de nourriture, manque d’installations sanitaires…
A Rafah, dans le sud de l’enclave, la population en vient à regretter la
«vie d’avant» le 7 octobre.

Dans le camp d'Al-Maghazi, dans le centre de la bande de Gaza, le 16
janvier. (AFP)par Hala Kodmani et Léa Masseguin publié aujourd'hui à 19h45

Plus d’une fois ces dernières semaines, Salwa s’est réveillée en croyant
avoir fait un cauchemar. «Mais dès que j’ouvre les yeux sur le plafond de la
salle de classe, que j’aperçois les centaines de matelas alignés autour du
mien, que je sens mon fils de 4 ans blotti contre moi sous la couverture, que
j’entends le bourdonnement des drones israéliens dans le ciel, je me rends
à l’évidence : c’est bien la réalité que nous vivons.» Réfugiée dans
une école de l’UNRWA, l’agence de l’ONU pour les réfugiés palestiniens,
à Khan Younès, dans le sud de la bande de Gaza, la mère de famille s’est
déplacée dès la mi-octobre avec son mari et ses trois enfants d’un quartier
populaire de la ville de Gaza, où ils vivaient, quelques jours avant que leur
maison ne soit détruite par un bombardement aérien. Salwa n’est pas la
seule à éprouver ce sentiment d’incrédulité face à la situation dans
laquelle elle se trouve depuis trois mois. «On est plusieurs ici à partager
l’impression de regarder un film d’épouvante», déplore-t-elle dans
une série de messages vocaux envoyés par WhatsApp. En raison du black-out
médiatique imposé par Israël dans l’enclave palestinienne, les Gazaouis
ont de plus en plus de mal à témoigner de la situation désastreuse sur place.
Des produits périmés et de l’herbe pour se nourrir

Des centaines de milliers de familles sont entassées dans les locaux de
l’UNRWA. Les écoles, bureaux dispensaires ou entrepôts de l’agence
abritent aujourd’hui 1,4 million de personnes sur un total d’environ
2 millions de déplacés, soit la quasi-totalité de la population de
l’enclave palestinienne. Les premiers arrivés, comme Salwa et les siens,
logés dans un bâtiment en dur avec des murs et un plafond, paraissent
désormais privilégiés. Car ceux qui sont arrivés plus tard se retrouvent
dans des tentes modestes, collées les unes aux autres dans les espaces à
l’air libre. Dans certains centres d’accueil, les réfugiés se sont mis
d’accord pour que les femmes et les enfants restent à l’intérieur des
bâtiments, tandis que les hommes s’installent dans les campements dehors.

Tous survivent dans des conditions sanitaires et humaines déplorables,
qui se dégradent de jour en jour depuis trois mois. Ils manquent de tout
et beaucoup ont faim, les produits alimentaires se faisant rares. Selon une
étude réalisée en décembre par l’organisation de défense des droits
de l’homme Euro-Med Human Rights Monitor, basée à Genève, environ 64 %
des Gazaouis ont avoué avoir mangé de l’herbe et des produits périmés
pour se rassasier depuis le début de la guerre. Le secrétaire général de
l’ONU, António Guterres, dénonce régulièrement des conditions humanitaires
«catastrophiques» dans la bande de Gaza et met en garde contre «l’ombre
de la famine» et des maladies qui s’abattent sur la population.

Les écoles sont à l'arrêt depuis le 7 octobre. A Rafah, dans la bande de
Gaza, le 18 janvier. (Ibraheem Abu Mustafa/Reuters)

Boire, manger, dormir, aller aux toilettes… Les besoins primaires de la vie
sont tout ce qui préoccupe les Gazaouis au quotidien. La chasse au pain, ou
à la farine pour en fabriquer, est l’obsession chaque matin pour tous. En
obtenir est la victoire du jour. Tous les membres de la famille participent
à l’effort. «On voit beaucoup d’enfants, de moins de 10 ans souvent,
dans les longues files devant les rares boulangeries qui fonctionnent,
attendre leur tour pendant deux heures ou plus, raconte Iman, employée dans
une organisation humanitaire dans le sud de Gaza. D’autres s’alignent bidon
à la main, pendant plusieurs heures parfois, devant les points de distribution
d’eau. On les voit revenir pliés et grimaçant sous le poids de la dizaine
de litres qu’ils portent, mais tous fiers et souriants de rapporter de quoi
dépanner leur famille. C’est aussi une manière d’occuper les enfants qui
traînent et s’amusent toute la journée dehors puisque les écoles sont à
l’arrêt depuis le 7 octobre.» Selon l’humanitaire, les élèves demandent
sans cesse quand les salles de cours rouvriront et qu’ils retrouveront leurs
camarades de classe.bonnés

La nostalgie de «leur vie d’avant» revient dans les conversations
des déplacés gazaouis quand ils veulent échapper à leur quotidien de
souffrance. «Tout me manque, écrit Hani al-Hajjah, jeune poète et écrivain
de Gaza dans son journal de bord publié par le site d’informations libanais
Daraj. Le balcon où je prenais mon café le matin, les soirées avec les
copains, le silence qui me dérangeait la nuit, mais surtout la mer à l’aube
comme à la tombée de la nuit.» Les Gazaouis évoquent souvent les couchers
de soleil sur les plages le long de la côte, où les familles venaient se
ressourcer sous des parasols multicolores, qui donnaient au littoral des
airs de carnaval. Sur l’une d’entre elles, un club de surf avait même
vu le jour afin de permettre aux jeunes Palestiniens de retrouver un peu de
liberté dans cette «prison à ciel ouvert» – comme était qualifiée la
bande de Gaza depuis le blocus israélien de 2007, après la prise de contrôle
du territoire par le Hamas.

A Gaza en août. ( Ali Jadallah/Anadolu via AFP)

La vie était pourtant loin d’être idyllique dans l’enclave gérée
par une administration exigeante et corrompue, où près de la moitié
des actifs étaient déjà sans emploi avant le 7 octobre. Le taux de
pauvreté dépassait les 35 %. «Les difficultés économiques combinées aux
restrictions de déplacements ont entraîné beaucoup de frustrations au sein
de la population. Les jeunes étaient désespérés, avec aucune perspective»,
raconte Jihad, un Gazaoui de 55 ans qui vivait jusqu’au 6 novembre à Gaza, à
300 mètres de l’hôpital Al-Shifa, avec son épouse et ses trois enfants. Le
travailleur humanitaire, qui a vécu un mois sous les bombardements incessants
de l’armée israélienne, fait partie des rares personnes ayant pu quitter
l’enclave. Il décrit une situation qui n’a cessé de se «détériorer»
ces dernières années, même si elle n’a rien de comparable à ce qui se passe
aujourd’hui. Selon l’Organisation internationale du travail, au moins 66% des
emplois ont été perdus à Gaza depuis le début de l’offensive israélienne.
Deuxièmes, troisièmes exodes…

Sur les plages de Gaza, les scènes de joie ont laissé place au chaos. Des
forêts de tentes, fabriquées souvent par les déplacés eux-mêmes avec des
bouts de plastique et de tissu, sont plantées sur le sable. Variante d’une
autre misère, le tableau de ces campements sauvages contraste avec les images
des quartiers désertés et dévastés par les bombardements israéliens dans la
partie nord de Gaza. Une scène d’apocalypse s’est déroulée ces derniers
jours sur une plage du sud de l’enclave quand un camion d’aide humanitaire
a été détourné et pris d’assaut par la foule. Dans une vidéo qui circule
sur les réseaux sociaux, des centaines de jeunes hommes débraillés, pieds
nus dans le sable, se bousculent pour accéder à la cargaison de nourriture
et récupérer un sac de riz ou une boîte de conserve.

Alors que l’armée israélienne a investi le Nord, y compris la ville
de Gaza qui comptait plus de 1 million d’habitants avant le 7 octobre,
les déplacés sont poussés vers des zones méridionales de plus en plus
restreintes et encombrées. Le rétrécissement de l’espace de vie possible
est particulièrement remarquable à Rafah, à la frontière avec l’Egypte. La
population de la ville a été multipliée par quatre, atteignant aujourd’hui
1,2 million de personnes du fait de l’afflux de réfugiés des autres régions
dévastées par les combats. Un exode qui se poursuit d’ailleurs alors que
des familles arrivent encore pour leur deuxième ou troisième déplacement et
essaient de trouver un endroit pour se poser. Ils s’installent dans des tentes
qui ne les protègent ni du froid ni des frappes aériennes israéliennes. La
plupart n’ont fui qu’avec quelques vêtements, n’imaginant pas que le
conflit allait s’éterniser. «C’est comme si ces cent jours avaient duré
cent ans», a résumé dans un reportage sur Al-Jazeera une quinquagénaire
déplacée sur cette bande frontalière, dans un campement informel sur le sable.

A Rafah, où 1,4 million de personnes sont parquées, le 17 janvier.

Or Rafah n’a ni les infrastructures ni les services suffisants pour répondre
aux besoins de toutes ces populations et la situation humanitaire s’y
dégrade de plus en plus rapidement. Le manque d’installations sanitaires est
particulièrement pénible pour les dizaines de milliers de déplacés. Attendre
son tour dans une file pendant une heure ou plus avant d’accéder aux WC est
ressenti comme une humiliation quotidienne, en particulier par les femmes. Quant
aux enfants, «ils ont du mal à se retenir. Et cela ajoute à leur angoisse
de la vie bouleversée. Les cas d’énurésie dans le sommeil sont de plus
en plus courants chez les enfants jusqu’à 10 ou 12 ans», note Iman, la
travailleuse humanitaire.

Près de Bordeaux, où il est désormais installé avec sa famille, Jihad
décrit la terrible vie quotidienne de ses sœurs, son frère et ses neveux
restés dans «l’enfer de Gaza» : «Ils tentent de survivre dans un abri
bricolé dans le sud de la bande, où ils ont de plus en plus de mal à
trouver à manger, à boire, et des médicaments. Les escalades de violence
auxquelles on assistait avant le 7 octobre ne sont rien par rapport à ce qui
se passe maintenant. Israël a imposé un blocus hermétique sur tous les
plans, et c’est la population civile qui paie le plus lourd tribut.» Si
Jihad aimerait que ses proches le rejoignent en France, sa sœur aînée de
73 ans ne quitterait la bande de Gaza pour rien au monde, y compris dans ces
conditions déplorables : «Elle ne veut pas sortir, elle a construit toute
sa vie là-bas. Et ses enfants ne l’abandonneront pas.»

Salwa avoue se demander parfois «s’il ne valait pas mieux mourir sous les
décombres de notre maison pulvérisée par un bombardement que survivre dans
les conditions dégradées et humiliantes dans ce camp de déplacés». Et
sans horizon.
