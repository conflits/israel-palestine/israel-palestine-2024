.. index::
   pair: Tal Bruttmann; Le 7 octobre a mis fin au 'plus jamais ça (2024-01-18)

.. _bruttmann_2024_01_18:

======================================================================================
2024-01-18 **Le 7 octobre 2023 a mis fin au 'plus jamais ça' par Tal Bruttmann**
======================================================================================

- https://akadem.org/le-7-octobre-a-mis-fin-au-plus-jamais-ca
- :ref:`antisem:tal_bruttmann`

Entretien
============

Le 7 octobre 2023 a mis fin au "plus jamais ça"

L'historien devant l'actualité
Tal Bruttmann, historien
Stéphane Bou, journaliste
18 janvier 2024
HISTOIRE DE LA SHOAH•7 OCTOBRE•ISRAËL AU QUOTIDIEN

.. figure:: images/tal_bruttmann.webp

"Le 7 octobre constitue une rupture majeure dans l'idée qu'il existe un
refuge pour les Juifs, un havre dans lequel ils ne seraient pas attaqués
en tant que Juifs".

L'historien Tal Bruttmann analyse, dans un grand entretien avec Stéphane Bou,
la nature singulière de cet événement, la profusion des images en circulation
et la référence omniprésente à la Shoah.


Audio
======

- https://cdn.akadem.org/articles/audio/64845a72-0dcd-496d-9079-c9fbbb5add0b.mp3


Tal Bruttmann et Stéphane Brou
=================================

.. figure:: images/bruttmann_brou.webp


Une tuerie de masse méthodique
==================================

.. figure:: images/une_tuerie_de_masse_methodique.webp

NPA
=====

.. figure:: images/ignoble_npa.webp



