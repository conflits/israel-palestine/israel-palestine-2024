

.. _rouger_2024_01_04:

=========================================================================================================
2024-01-04 Trois mois après les massacres du Hamas, les Israéliens traumatisés et isolés par Rouger
=========================================================================================================


- https://www.liberation.fr/international/moyen-orient/trois-mois-apres-les-massacres-du-hamas-les-israeliens-traumatises-et-isoles-20240104_CRZKK55ZURFENNNK6WJ2KG7CDQ/


.. tags:: Traumatisme, 7 octobre

Guerre Trois mois après les massacres du Hamas, les Israéliens traumatisés
et isolés

Bloquée par la peine et l’horreur des massacres du 7 octobre, la société
israélienne ferme les yeux sur la destruction de Gaza. Tirée vers l’extrême
droite par des militants religieux expansionnistes, elle s’éloigne de plus
en plus du reste du monde.

Des écoliers de Kiryat Gat accueillent mardi des réfugiés du kibboutz de
Nir Oz, attaqué par le Hamas le 7 octobre. (Ilia Yefimovich/DPA. Abaca)par
Nicolas Rouger, correspondant à Tel-Aviv publié le 4 janvier 2024 à 20h16

Les évacués du kibboutz de Nir Oz ont quitté leur hôtel, un peu à
l’écart de la ville balnéaire d’Eilat, le 2 janvier. Ici, la mer Rouge
luit de bleu, entourée de montagnes désertiques. C’est en bus que ces 160
personnes étaient arrivées, dans la nuit du 8 octobre, après presque deux
jours d’horreur – une centaine d’autres avaient été tuées ou prises
en otage. C’est en bus aussi qu’elles sont reparties, vers un quartier de
la ville prolétaire de Kiryat Gat, à 3 h 30 de route vers le nord.

«Beaucoup ne sont pas prêts à prendre ce nouveau départ», dit Debra
Kalmanowitz, manager des services psychosociaux à l’ONG IsraAid, qui passe
le plus clair de son temps à Eilat depuis le 8 octobre, «mais c’est
important de les pousser à reprendre une routine». Les enfants pourront
retourner à l’école, et certains parents au travail. Le futur reste très
incertain : la communauté ne sait pas si elle pourra retrouver son kibboutz,
à 80 % détruits. Mais au moins jusqu’à la fin de l’année scolaire,
les évacués auront un toit, une cuisine, des habitudes.

Kiryat Gat est à une vingtaine de kilomètres à vol d’oiseau de Beit Hanoun,
première ville à la frontière au nord-est dans la bande de Gaza. L’armée
israélienne a annoncé qu’elle se préparait à retirer des troupes,
mais les bombes continuent à tomber dans l’enclave – et l’assassinat
ciblé de Saleh al-Arouri, numéro 2 du Hamas, à Beyrouth mardi n’a fait
qu’éloigner la possibilité d’un cessez-le-feu, le retour des otages et
l’arrivée libre de l’aide humanitaire dans Gaza.

Au 90e jour de la guerre, jeudi, le ministère de la Santé du Hamas décomptait
22 438 morts, dont 125 dans les dernières vingt-quatre heures. Plus de 80 %
de la population de Gaza, près de 2 millions de personnes, vivent dans des
conditions abjectes, à la merci de la faim et de l’épidémie. Les denrées
qui rentrent sont parfois prises d’assaut, réquisitionnées par le Hamas,
ou tout simplement revendues au plus offrant par des «businessmen». Les
prix ont été multipliés par dix, les communications sont mauvaises, parfois
entièrement coupées. Et les journalistes locaux, seuls aptes à pouvoir donner
ces informations, sont aux abois, épuisés, tués, sans relève d’une presse
internationale à laquelle l’accès reste bloqué.  Religion prépondérante

Depuis Israël, pourtant si proche, tout cela reste invisible. «Ce n’est
pas que l’information n’est pas accessible, explique la politologue Dahlia
Scheindlin, mais elle n’est pas mise en priorité. Quand on parle de la
situation humanitaire à Gaza, on en parle en huitième position, après
trente minutes de journal télévisé.» Cela tient encore au traumatisme,
qui ne cesse de se réverbérer dans la société israélienne. «Le 7 octobre
est dans tous les esprits – et c’est d’autant plus le cas, parce que de
plus en plus de témoignages sortent», explique-t-elle.

Cette différence si marquante creuse l’isolement d’Israël par rapport
au reste de la communauté internationale, bien que le soutien inconditionnel
de Washington laisse encore l’Etat hébreu à l’abri des sanctions. Même
dans l’intelligentsia de Tel-Aviv, on assigne le soutien aux Palestiniens
à de l’antisémitisme, on se sent assiégé. Mais le credo est toujours le
même, répété partout : «Ensemble, nous vaincrons.» Même si on n’est
pas sûr de la forme de cette victoire.

Des publicités de plusieurs dizaines de mètres de haut accueillent les
automobilistes au carrefour d’Azrieli, près du quartier général de
l’armée israélienne à Tel-Aviv. L’une d’entre elles demande le
retour des otages israéliens à Gaza. Une autre fait l’apologie du peuple
d’Israël victorieux, citant la Bible, un lion rugissant sur un fond bleu
et blanc. Une dernière met en scène une influenceuse connue, une main sur
le front, récitant le Shema Israël, la profession de foi juive quotidienne.

«Pour nous, cette guerre représente, en quelque sorte, une opportunité»,
explique le jeune rabbin dynamique Honi Yakunt, dont l’organisation Un instant
de sagesse est à l’origine de la campagne, soutenue par un riche donateur
anonyme. «Israël est à tous les juifs, et nous devons nous rassembler
autour de la tradition. Quand nous sommes désunis, c’est à ce moment-là
que la calamité frappe, comme le 7 octobre», explique-t-il. A l’entrée
de son bureau, dans un complexe moderne dans la ville antique de Césarée,
une représentation du troisième Temple, trônant sur l’esplanade des
Mosquées, au centre d’un Jérusalem moderne. C’est pour quand ? «Pour
demain, avec l’aide de Dieu», dit Honi en souriant.

Depuis le début de la guerre, la religion et ses symboles ont pris une place
de plus en plus prépondérante dans l’imaginaire israélien. Benyamin
Nétanyahou, grand laïc devant l’Eternel, a fait référence à des
figures bibliques dans ses allocutions, assimilant la menace du Hamas à celle
d’Amalek, ennemi archétypal des juifs. Cette rhétorique ruisselle dans le
reste de la société : «Vous pensez venir dire ici «Free Palestine» ? /Ptui,
fils d’Amalek /De la Galilée à Eilat, tout le pays est en uniforme»,
rappe la chanteuse Stilla dans la chanson guerrière Harbu Darbu, première
en écoutes sur les réseaux sociaux.  Pression de l’extrême droite

La partie militante et expansionniste de l’extrême droite israélienne
essaie de tirer son épingle du jeu. A Jérusalem, alors que toutes les
institutions étaient fermées, les commissions municipales de planning urbain
ont repris du service dès le 9 octobre, profitant du climat pour mettre en
route d’importants projets de colonisation dans la partie est de la ville. En
Cisjordanie, les associations de colons ont courtisé la presse, avec un certain
succès, pour faire passer un double message : l’attaque du 7 octobre a eu
lieu parce qu’on s’est débarrassé des colonies de peuplement à Gaza ;
et, si on ne renforce pas la colonisation en Cisjordanie, Tel-Aviv pourrait
devenir la cible d’une autre attaque comme celle du 7 octobre.

Ce message est relayé par les membres d’extrême droite du gouvernement –
comme Betzalel Smotrich, qui affirmait mercredi sur X (anciennement Twitter)
que «70 % des Israéliens soutiennent l’émigration volontaire des Gazaouis»,
un pourcentage qui n’est confirmé par aucun sondage fiable. A cette pression
de l’exécutif, il faut ajouter une pression par le bas : «Depuis plusieurs
années, le corps de l’armée israélienne a été peu à peu investi par
une pensée nationale-religieuse», explique Dahlia Scheindlin.

La guerre va continuer, assure l’establishment israélien, pendant plusieurs
mois au moins, même si son intensité va varier et malgré la pression
interne des 200 000 évacués. En Cisjordanie, les opérations quotidiennes
dans les villes palestiniennes n’arrivent pas à endiguer l’attrait de
la lutte armée parmi les Palestiniens, une façon de montrer que les buts
stratégiques des Israéliens – éradiquer le Hamas, ramener les otages,
et un sens de sécurité aux Israéliens – semblent compromis sans une
résolution de fond du conflit.

La tâche échouera peut-être à Benny Gantz, grand général au charisme
discutable mais à la crédibilité intacte qui semble faire consensus,
alors que Benyamin Nétanyahou perd toute légitimité. Selon les derniers
sondages, en cas d’élections, son parti de l’Unité nationale obtiendrait
38 sièges sur 120 à la Knesset – un score jamais atteint par le Likoud sous
Nétanyahou, qui lui tomberait à 16 sièges au lieu de 32. L’ancien chef
d’état-major a déjà commencé à poser ses pions, marquant des points
là où Nétanyahou a fauté – en se rapprochant par exemple des familles
des otages encore retenus à Gaza, estimés à plus de 120 personnes. Il
fait aussi des ouvertures auprès des leaders étrangers, comme auprès du
président français Emmanuel Macron, qui s’est entretenu avec lui mardi
comme s’il était son homologue.  «Soumission ou silence»

Benny Gantz, allié à d’anciens piliers du Likoud, a une attitude tout aussi
inflexible par rapport aux Palestiniens. Rares sont les voix en Israël à se
prononcer pour la paix. Seules les forces vives du partenariat juif-arabe se
lèvent encore, malgré les difficultés. «Le massacre à Gaza est célébré
en Israël avec un discours aux accents fascistes, même dans les endroits
où on existe ensemble, juifs et arabes, au travail, dans les supermarchés»,
explique Sally Abed, Palestinienne d’Israël et cofondatrice de l’association
Debout ensemble. Et si la majorité ne le revendique pas, elle le tolère.»

«Il y a une déshumanisation générale des Palestiniens. Cela touche aussi
les Palestiniens citoyens d’Israël – il y a plus que jamais un refus
d’accepter que nous faisons partie intégrante de cette nation. On nous
demande soumission ou silence», ajoute la militante. Plus que tout, elle
craint la dépolitisation de sa communauté, qui représente un cinquième de la
population du pays. «Tant qu’on est en colère, ça va, dit-elle. Quand on
ne parle plus, on coule. Or, avec la dualité de nos expériences, nous sommes
un lien critique pour la construction d’une paix pérenne, non seulement ici,
mais dans tout le Moyen-Orient.»
