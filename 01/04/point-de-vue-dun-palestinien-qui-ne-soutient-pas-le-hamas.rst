

=========================================================================================================
2024-01-04 Point de vue d'un Palestinien qui ne soutient pas le Hamas 4 janvier 2024 Par Youssef J.
=========================================================================================================

https://blogs.mediapart.fr/youssef-j/blog/040124/point-de-vue-dun-palestinien-qui-ne-soutient-pas-le-hamas


.. tags:: Palestinien

Point de vue d'un Palestinien qui ne soutient pas le Hamas 4 janvier 2024 |
Par Youssef J.

La solution à ce conflit, c’est Israël et la Palestine : leurs peuples,
surtout leurs jeunesses, méritent un avenir plus radieux. Pourquoi en est-on
arrivé au 7 octobre, que penser de l’Autorité Palestinienne, et quelles
pistes pour le lendemain ?

Le 7 octobre demeurera une date clé dans l’histoire du conflit
Israélo-Palestinien : ce jour-là, les combattants du Hamas traversent les
confins de Gaza et attaquent les localités avoisinantes, tuant au passage des
centaines de résidents et prenant pour otages plus de deux cent personnes,
hommes femmes et enfants inclus. Durant leur attaque ces combattants ont commis,
vidéos a l’appui, des atrocités envers leurs victimes qu’aucun objectif
nationaliste ou « libératoire » ne justifie, et ces actes sont justement
considérés des crimes de guerre.

L’armée Israélienne a riposté, bombardant Gaza et entreprenant une invasion
terrestre de ce territoire. Le nombre des victimes civiles palestiniennes à
ce jour a dépassé les 22000, les blessés se comptent par milliers, et les
infrastructures de Gaza, y compris écoles, hôpitaux, bâtiments publics,
et habitations civiles sont en grande partie sinon complètement détruites.

Les tueries de civils doivent être universellement condamnées, et aucune
population civile, où qu’elle soit et qui qu’elle soit, ne doit être
exposée à de tels actes et à des pertes de vies. Pourquoi les hommes du
Hamas ont-ils déployé tant de rage, de vengeance, et d’agressivité durant
leur attaque ? Expliquer n’est en aucun cas justifier, mais, vu l’ampleur
des drames du côté Israélien comme du côté Palestinien, il est impératif
de connaître les raisons.

Le conflit Israélo-Palestinien dure depuis des décennies, avec son lot de
l’occupation et de ses méfaits, de colonisation rampante en Cisjordanie, et
d’humiliations quotidiennes aux points de contrôle innombrables dressés par
l’armée Israélienne entre les villes et villages Palestiniens. Cependant
il faut regarder plus près les développements du côté de Gaza (durant
les 20 dernières années), du côté de Jérusalem-Est et de la Cisjordanie
(durant les 2 dernières années) pour comprendre que le statut quo espéré
par le gouvernement Israélien était intenable et qu’une explosion annoncée
n’était en fait qu’en attente.

Gaza était mis sous blocus durant plus ou moins les 15 dernières années,
avec des ravitaillements étroitement surveillés par l’armée Israélienne,
et des restrictions très lourdes sur les Gazaouis qui avaient besoin
de se déplacer hors de Gaza que ce soit pour voyager ou pour des raisons
familiales, pour étudier ou pour se soigner, même si c’est pour aller en
Cisjordanie. Pour se rendre en Israël les permis de travail étaient délivrés
à des adultes mariés soigneusement triés, laissant toute une génération
de jeunes grandissant et murissant dans les confins de ce territoire, exposés
uniquement à leur quotidien morose, sans avoir accès aux multiples facettes
de vie que les jeunes de leur âge connaissent ailleurs. Plusieurs centres
culturels et librairies ont été bombardés et détruits au fil des années
par l’armée Israélienne, on se demande pourquoi. Les personnes de cette
génération, aujourd’hui âgés de 20 ans ou quelques, ont été empêchés
de par les conditions de vie infligées sur eux d’avoir un quelconque espoir
d’un avenir meilleur. Ils sont ainsi devenus une audience captive pour les
fondamentalistes religieux qui contrôlent Gaza et dont la doctrine première
et la lutte armée contre Israël.

En Cisjordanie, le nombre de Palestiniens tués cette année entre janvier et
septembre par l’armée Israélienne a dépassé les 200, un nombre jamais
atteint en si peu de temps durant l’histoire récente de ce conflit, la
plupart d’entre eux étant des habitants des camps de réfugiés de Jenine et
de Naplouse, entre autres. Les soldats de l’armée Israélienne ainsi que les
colons Juifs extrémistes ont été autorisés, par le gouvernement Israélien,
à tirer et tuer, impunément et sans comptes à rendre, sur tout Palestinien
qui leur semble agir de façon hostile ou suspecte. En plus les colons, enhardis
par leur gouvernement, ont accru leurs attaques sur les Palestiniens dans les
campagnes de Cisjordanie et sur les routes, et plus particulièrement contre
les villages, coupant ou brulant les oliviers avoisinant, sachant très bien
que les retombées économiques liées aux oliviers sont un moyen de subsistance
vital pour ces villageois.

A Jérusalem-Est, les colons extrémistes ont aussi accru leurs efforts pour
déloger des familles Palestiniennes de leurs maisons, plus récemment dans
les quartiers de Cheikh Jarrah et de Silwan.

La mosquée El-Aqsa à Jérusalem-Est est probablement l’un des endroits les
plus chargés émotionnellement pour les Musulmans. Les extrémistes Juifs le
savent et pourtant ils ont multiplié leurs incursions sur ce site ces derniers
mois. Ils le font pertinemment sachant que c’est une provocation très
sérieuse pour les Palestiniens musulmans de Jérusalem et d’ailleurs. Là
aussi ces extrémistes ont été encouragés par des ministres dans le
gouvernement actuel, et sont même protégés par les services de sécurité
Israéliens lors de leurs intrusions sur l’Esplanade des mosquées. C’est
dans ce contexte que le Hamas a nommé son attaque « Déluge d’Al-Aqsa ».

Il est significatif que les attaques (perçues comme telles) de groupes
extrémistes Juifs religieux contre un lieu sacré Musulman aient été opposées
par une contre-attaque du groupe extrémiste religieux Islamique Hamas. C’est
une évolution inopportune du cours du conflit Israélo-Palestinien pour
beaucoup d’Israéliens et de Palestiniens séculaires, qui croyaient et
qui croient toujours à une solution pacifique de ce conflit suivant le
concept de la solution à deux états. L’Organisation de Libération de
la Palestine et par la suite l’Autorité Palestinienne (AP) ont reconnu
Israël depuis la signature des accords d’Oslo dès 1993. Il est vraiment
étonnant qu’une majorité de médias occidentaux affichent un dénigrement
persistant de l’AP depuis le début de cette dernière crise. Les Etats Unis
et la Communauté Européenne ont aussi ignoré cette Autorité bien avant,
alignant leurs positions sur celles des gouvernements Israéliens en place
qui n’ont cessé de la discréditer durant les 20 dernières années.

Comment évaluer le bilan de l’AP ? Depuis la conférence de paix de Madrid et
par la suite les accords d’Oslo, plus de 300,000 Palestiniens de la Diaspora
ont pu revenir et s’installer en Cisjordanie et à Gaza, contribuant à
la croissance économique et au développement des territoires Palestiniens,
et contribuant au bien-être des citoyens Palestiniens dans l’ensemble.

L’AP a établi des institutions gouvernementales, a entrepris et achevé
des travaux d’infrastructure, a promulgué des lois et directives qui
ont encouragé des investissements publics et privés dans les travaux
publics, les hôpitaux, les universités et les écoles, et les équipements
touristiques. Les lois et directives ont aussi organisé le fonctionnement du
cadastre et l’enregistrement des biens immobiliers, les échanges commerciaux,
et les services de statut civil. Parmi les réalisations notoires il faut
citer l’instauration d’un système d’assurance maladie très complet, et
l’inauguration de musées ainsi que la réhabilitation de sites archéologiques
(avec dans plusieurs cas l’aide d’organisations non gouvernementales, de la
Communauté Européenne, et du Japon). Les services gouvernementaux et municipaux
sont efficaces et en grande partie automatisés, rivalisant avec ceux d’autres
pays avancés. Un secteur bancaire impressionnant s’est développé (parti de
succursales d’une seule banque Jordanienne opérant en Cisjordanie et d’une
banque locale à Gaza avant 1993, le réseau bancaire compte aujourd’hui plus
d’une douzaine de banques Palestiniennes et régionales couvrant tous les
Territoires Occupés, supervisés par une Autorité Monétaire qui impose les
pratiques et les règles de conformité internationales). Un acquis essentiel
de l’AP est le passeport Palestinien qui permet à son titulaire de voyager
dans des pays sur les cinq continents.

Le président de l’AP, Mahmoud Abbas, a toujours soutenu une solution
pacifique du conflit depuis les années 1990, et a essayé de mettre en œuvre
les accords d’Oslo depuis son élection en 2005, sans y parvenir, puisque
les gouvernements Israéliens successifs dirigés par le Likoud ne veulent
absolument pas la solution des deux états. Ces gouvernements n’ont par
ailleurs jamais cessé de miner Abbas et l’AP, privant les Palestiniens des
rentrées financières dues à l’AP comme prévu par les accords d’Oslo
concernant les impôts sur les importations des Territoires Occupés, et
empêchant les élections parlementaires Palestiniennes parce que Abbas
insistait que les Palestiniens de Jérusalem-Est devaient participer à ces
élections. Abbas a souvent dénoncé les attentats solitaires perpétrés par
des Palestiniens en Israël, les qualifiant « d’actes vains et absurdes
» sachant très bien qu’il sera critiqué par ses détracteurs. Il a
aussi soutenu les mesures de coordination sécuritaires entre l’AP et les
services de sécurité Israéliens, qui tendaient à assurer la sécurité de
ses citoyens civils aussi bien que celle des Israéliens civils, sachant, là
encore, qu’il sera attaqué par ses détracteurs dont certains le qualifie
de traitre. Si Israël et les puissances Occidentales avaient sérieusement
travaillé avec l’Autorité Palestinienne et traité Gaza et sa population
avec dignité et responsabilité, Israéliens et Palestiniens se trouveraient
dans une situation infiniment meilleure que la situation actuelle.

Maintenant, qu’est-ce qu’il faut faire, tout de suite ?

Arrêter définitivement l’attaque militaire israélienne contre Gaza
Relâcher tous les otages Israéliens, et relâcher tous les prisonniers
politiques palestiniens des prisons israéliennes Acheminer suffisamment
de médicaments et fournitures hospitalières, de denrées alimentaires et
carburants, à Gaza. Il serait opportun aussi de permettre la venue d’un
personnel médical temporaire à Gaza

Faire stationner des forces mandatées par les Nations Unis tout le long des
frontières entre Gaza et Israël Procurer les fonds nécessaires pour la bien
nommée UNRWA (L’Office de secours et de travaux des Nations Unis pour les
réfugiés de Palestine dans le Proche-Orient, établie en 1949!); son travail
et ses services sont colossalement requis maintenant plus que jamais, pour
restaurer un semblant de vie normale à la population de Gaza (L’UNRWA, déjà
sur place, est responsable d’écoles, de cliniques, de fournitures de vivres,
entre autre) Accepter l’état de Palestine en tant que membre à part entière
à L’ONU (ce qui déclenchera des dynamiques constructives indéniables),
et reconnaissance mutuelle entre la Palestine et Israël.  Dans le court terme,
il faudra de nouvelles élections législatives en Israël et en Palestine. Pour
renforcer la sécurité des deux pays, et même si cela peut paraitre utopique
à priori, deux séries de mesures doivent être mises en place :

Des lois doivent être promulguées (par un gouvernement de transition en
Palestine, et par le parlement Israélien), stipulant que tout candidat(e)
à des élections politiques doit, pour le côté Palestinien, reconnaitre la
Palestine dans les frontières du 4 Juin 1967 (A savoir la Cisjordanie, Gaza,
et Jérusalem-Est), et, pour le côté Israélien, reconnaitre les frontières
de l’état d’Israël dans les frontières du 4 Juin 1967. Cela évitera,
pour de bon, que des responsables Palestiniens au pouvoir rêvassent d’une
Palestine historique «de la mer à la rivière » , et que des responsables
Israéliens au pouvoir rêvassent d’un « Grand Israël» englobant la
Cisjordanie et au-delà.  En plus, les lois Israéliennes devraient prévoir
des sanctions très précises concernant tout assassinat de Palestiniens, par
des colons ou par des militaires Israéliens, pour ne plus laisser le champ
libre à une « chasse au Palestinien » impunie et sans conséquences. De
même, les lois Palestiniennes devraient prévoir des sanctions très
précises concernant tout assassinat d’Israéliens par des Palestiniens.
Les négociations politiques devront reprendre dans le cadre des résolutions
des Nations Unies et sous l’égide de la communauté internationale. Les
résolutions du Sommet Arabe de Beyrouth en 2002 ont repris les résolutions
des Nations Unies et ont proposé la reconnaissance d’Israël par les états
Arabes y compris l’Arabie Saoudite, dans le cadre d’un règlement instituant
les deux états, et ceci bien avant les accords d’Abraham. La solution à
ce conflit, c’est Israël et la Palestine : leurs peuples, surtout leurs
jeunesses, méritent un avenir plus radieux.
