

.. _poinssot_agri_2024_01_04:

=============================================================================================================================
2024-01-04 **Pour l’agriculture palestinienne, ce qui se passe depuis le 7 octobre est « un désastre »** par Amélie Poinssot
=============================================================================================================================

https://www.mediapart.fr/journal/international/040124/pour-l-agriculture-palestinienne-ce-qui-se-passe-depuis-le-7-octobre-est-un-desastre

Pour l’agriculture palestinienne, ce qui se passe depuis le 7 octobre est
« un désastre »

À Gaza sous les bombes comme en Cisjordanie occupée, l’eau est devenue
un enjeu crucial, et le conflit met en évidence une injustice majeure dans
l’accès à cette ressource vitale. Entretien avec l’hydrologue Julie
Trottier, chercheuse au CNRS.

Amélie Poinssot

4 janvier 2024 à 09h40

Des cultures gâchées, une population gazaouie sans eau potable… Et en
toile de fond de la guerre à Gaza, une extrême dépendance des territoires
palestiniens à l’eau fournie par Israël. L’inégal accès à la ressource
hydrique au Proche-Orient est aussi une histoire d’emprise sur les ressources
naturelles.

Entretien avec l’hydrologue Julie Trottier, chercheuse au CNRS, qui a fait sa
thèse sur les enjeux politiques de l’eau dans les territoires palestiniens
et a contribué à l’initiative de Genève, plan de paix alternatif pour le
conflit israélo-palestinien signé en 2003, pour laquelle elle avait fait,
avec son collègue David Brooks, une proposition de gestion de l’eau entre
Israéliens et Palestiniens.

Mediapart : L’accès à l’eau est-il un enjeu dans le conflit qui oppose
Israël au Hamas depuis le 7 octobre ?

Julie Trottier : Oui, l’accès à l’eau est complètement entravé à
Gaza aujourd’hui. En Cisjordanie, la problématique est différente, mais
le secteur agricole y est important et se trouve mal en point.

Il faut savoir que l’eau utilisée en Israël vient principalement du
dessalement d’eau de mer. C’est la société israélienne Mekorot qui
l’achemine, et elle alimente en principe la bande de Gaza en eau potable à
travers trois points d’accès. Mais depuis le 7 octobre, deux d’entre eux
ont été fermés, il n’y a plus qu’un point de livraison, au sud de la
frontière est, à Bani Suhaila.

Cependant, 90 % de l’eau consommée à Gaza était prélevée dans des
puits. Il y a des milliers de puits à Gaza, c’est une eau souterraine
saumâtre et polluée, car elle est contaminée côté est par les composés
chimiques issus des produits utilisés en agriculture, et infiltrée côté
ouest par l’eau de mer.

Comme l’électricité a été coupée, cette eau ne peut plus être pompée ni
désalinisée. En coupant l’électricité, Israël a supprimé l’accès à
l’eau à une population civile. C’est d’une violence extrême. On empêche
2,3 millions de personnes de boire et de cuisiner normalement, et de se laver.

Les stations d’épuration ne fonctionnent plus non plus, et les eaux usées
non traitées se répandent ; le risque d’épidémie est considérable.

On parle moins de l’accès aux ressources vitales en Cisjordanie… Pourtant
la situation s’aggrave également dans ces territoires.

En effet. Le conflit a éclaté peu avant la saison de cueillette des olives en
Cisjordanie. Pour des raisons de sécurité, craignant de supposés mouvements
de terroristes, de nombreux colons ont empêché des agriculteurs palestiniens
d’aller récolter leurs fruits.

La majorité des villages palestiniens se trouvent non loin d’une colonie. En
raison des blocages sur les routes, les temps de trajet sont devenus extrêmement
longs. Mais si l’on ne circule plus c’est aussi parce que la peur domine. Des
colons sont équipés de fusils automatiques, des témoignages ont fait état
de menaces et de destruction d’arbres, de pillages de récoltes.

Résultat : aujourd’hui, de nombreux agriculteurs palestiniens n’ont plus
accès à leurs terres. Pour eux, c’est un désastre. Quand on ne peut pas
aller sur sa terre, on ne peut plus récolter, on ne peut pas non plus faire
fonctionner son système d’irrigation.

L’accès à l’eau n’est malheureusement pas un problème nouveau pour
la Palestine.

C’est vrai. En Cisjordanie, où l’eau utilisée en agriculture vient
principalement des sources et des puits, des colonies ont confisqué de nombreux
accès depuis des années. Pour comprendre, il faut revenir un peu en arrière...

Avant la création d’Israël, sur ces terres, l’accès à chaque source,
à chaque puits, reposait sur des règles héritées de l’histoire locale
et du droit musulman. Il y avait des « tours d’eau » : on distribuait
l’abondance en temps d’abondance, la pénurie en temps de pénurie,
chaque famille avait un moment dans la journée pendant lequel elle pouvait se
servir. Il y avait certes des inégalités, la famille descendant de celui qui
avait aménagé le premier conduit d’eau avait en général plus de droits,
mais ce système avait localement sa légitimité.

À l’issue de la guerre de 1948-1949, plus de 700 000 Palestiniens ont été
expulsés de leurs terres. Celles et ceux qui sont arrivés à ce qui correspond
aujourd’hui à la Cisjordanie n’avaient plus que le « droit de la soif
» : ils pouvaient se servir en cruches d’eau, mais pas pour irriguer les
champs. Les droits d’irrigation appartenaient aux familles palestiniennes qui
étaient déjà là, et ce fut accepté comme tel. Plus tard, les autorités
jordaniennes ont progressivement enregistré les différents droits d’accès
à l’eau. Mais ce ne sera fait que pour la partie nord de la Cisjordanie.

À l’intérieur du nouvel État d’Israël, en revanche, la population
palestinienne partie, c’est l’État qui s’est mis à gérer l’ensemble
de l’eau sur le territoire. Dans les années 1950 et 1960, il aménage la
dérivation du lac de Tibériade, ce qui contribuera à l’assèchement de
la mer Morte.

En 1967, après la guerre des Six Jours, l’État hébreu impose que tout
nouveau forage de puits en Cisjordanie soit soumis à un permis accordé par
l’administration israélienne. Les permis seront dès lors attribués au
compte-gouttes.

Il y a dans les territoires palestiniens une dépendance complète à l’égard
d’Israël pour la ressource en eau.

Après la première Intifida, en 1987, les difficultés augmentent. Comme cela
devient de plus en plus difficile pour la population palestinienne d’aller
travailler en Israël, de nombreux travailleurs reviennent vers l’activité
agricole, et les quotas associés aux puits ne correspondent plus à la demande.

Par la suite, les accords d’Oslo, en 1995, découpent la Cisjordanie, qui
est un massif montagneux, en trois zones de ruissellement selon un partage
quantitatif correspondant aux quantités prélevées en 1992 – lesquelles
n’ont plus rien à voir avec aujourd’hui. La répartition est faite comme
si l’eau ne coulait pas, comme si cette ressource était un simple gâteau à
découper. 80 % des eaux souterraines sont alors attribuées aux Israéliens,
et seulement 20 % aux Palestiniens.

L’accaparement des ressources s’est donc exacerbé à la faveur de la
colonisation. Au-delà de l’injustice causée aux populations paysannes,
l’impact du changement climatique au Proche-Orient ne devrait-il pas imposer
de fonctionner autrement, d’aller vers un meilleur partage de l’eau ?

Si, tout à fait. Avec le changement climatique, on va droit dans le mur
dans cette région du monde où la pluviométrie va probablement continuer à
baisser dans les prochaines années.

C’est d’ailleurs pour cette raison qu’Israël a lancé le dessalement de
l’eau de mer. Six stations de dessalement ont été construites. C’est le
choix du techno-solutionnisme, une perspective coûteuse en énergie. L’État
hébreu a même créé une surcapacité de dessalement pour accompagner une
politique démographique nataliste. Et pour rentabiliser, il cherche à vendre
cette eau aux Palestiniens. De fait, l’Autorité palestinienne achète chaque
année 59 % de l’eau distribuée par Mekorot. Elle a refusé toutefois une
proposition d’exploitation d’une de ces usines de dessalement.

Il faut le souligner : il y a dans les territoires palestiniens une dépendance
complète à l’égard d’Israël pour la ressource en eau.

Quant à l’irrigation au goutte à goutte, telle qu’elle est pratiquée
dans l’agriculture palestinienne, ce n’est pas non plus une solution
d’avenir. Cela achemine toute l’eau vers les plantes cultivées, et
transforme de ce fait le reste du sol en désert, alors qu’il faudrait un
maximum de biodiversité sous nos pieds pour mieux entretenir la terre. Le
secteur agricole est extrêmement consommateur d’eau : 70 à 80 % des
ressources hydriques palestiniennes sont utilisées pour l’agriculture.

Tout cela ne date pas du 7 octobre. Mais les événements font qu’on va vers
le contraire de ce que l’on devrait faire pour préserver les écosystèmes
et l’accès aux ressources. L’offensive à Gaza, outre qu’elle empêche
l’accès aux terres agricoles le long du mur, va laisser des traces de
pollution très graves dans le sol… En plus de la tragédie humaine, il y
a là une catastrophe environnementale.

Cependant, c’est précisément la question de l’eau qui pourrait avoir
un effet boomerang sur le pouvoir israélien et pousser à une sortie du
conflit. Le reversement actuel des eaux usées, non traitées, dans la mer,
va avoir un impact direct sur les plages israéliennes, car le courant marin
va vers le nord. Cela ne pourra pas durer bien longtemps.
