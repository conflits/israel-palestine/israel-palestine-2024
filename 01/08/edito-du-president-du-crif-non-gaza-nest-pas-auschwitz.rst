.. index::
   pair: Yonathan Arfi ; Édito du Président du Crif : Non, Gaza n'est pas Auschwitz (20224-01-08)

.. _arfi_2023_01_08:

==============================================================================================
2024-01-08 **Édito du Président du Crif : Non, Gaza n'est pas Auschwitz** par Yonathan Arfi
==============================================================================================

.. tags:: Camus, Nommer les choses

Mal nommer les choses c’est ajouter au malheur de ce monde » écrivait Albert Camus en 1944
===============================================================================================

« Ce renversement accusatoire est donc bien une stigmatisation volontaire et
calculée du seul État juif. Et, hélas, nous le savons, l'opprobre qu'elle
suscite s’étendra mécaniquement sur les Juifs, où qu'ils vivent. »

Non, Gaza n'est pas Auschwitz.

Parmi les accusations portées contre Israël, la comparaison entre le sort des
Palestiniens aujourd'hui et celui des Juifs pendant la Shoah mais également
l'accusation mensongère de « génocide » sont les plus infamantes.

La formule est connue : « Mal nommer les choses c’est ajouter au malheur
de ce monde » écrivait Albert Camus en 1944. Alors que doit se tenir à
la Cour Pénale Internationale cette semaine l'audience d'Israël face à
l'Afrique du Sud, nombreux sont les acteurs du débat public qui ont recours
à ces accusations fallacieuses, depuis le Conseil Français du Culte Musulman
(CFCM) dans ces derniers communiqués jusqu'au Président turc Erdogan comparant
Netanyahou à Hitler, depuis les élus La France Insoumise (LFI) parlant de
génocide jusqu'à certaines instances internationales…

Sans rien retirer par ailleurs à l'empathie légitime pour la souffrance de la
population civile palestinienne, victime et otage de cette guerre déclenchée
par le Hamas, **ces accusations doivent être rejetées fermement**.

Alors, à quoi correspond cette accusation de génocide ? 
Que signifie cette nazification d’Israël ? 

Apposer l’image du génocide sur la guerre à Gaza, vise à accoler l’étiquette 
de l’ultime infamie sur l’État d’Israël. 
Infondée matériellement et juridiquement, cette accusation vise en fait 
d'autres buts, politiques.

Cette inversion perverse vise la perturbation des boussoles morales
=====================================================================

D'abord, cette inversion perverse transforme l'État refuge des victimes de la
Shoah, en un État de bourreaux, et vise la perturbation des boussoles morales
du public. Puis, en affublant ainsi symboliquement Israël du qualificatif de
nazi, l’accusation soulage les consciences européennes de la culpabilité
de la Shoah. Enfin, en maximisant la représentation de la culpabilité morale
d’Israël, l’accusation minimise la gravité des exactions du Hamas le 7
octobre. Accuser Israël de génocide est au fond la stratégie la plus efficace
pour passer sous silence les pogroms du 7 octobre et la pulsion exterminatrice
qui a animé les terroristes du Hamas massacrant, violant et suppliciant des
populations civiles israéliennes...


La cause palestinienne a souvent utilisé le miroir de l’Histoire juive pour formuler son propre récit
==========================================================================================================

Ce renversement accusatoire n’est pas nouveau. La cause palestinienne a souvent
utilisé le miroir de l’Histoire juive pour formuler son propre récit. Ainsi,
le choix du mot Nakba (« catastrophe », en arabe) pour qualifier la date
historique de l’indépendance de l’État d’Israël et les déplacements
d’une partie des populations juives et arabes alors présentes, répond au
sens du mot « Shoah » en hébreu.

Mais surtout, ce renversement accusatoire désinhibe toutes les
violences. Accuser Israël de mener un génocide, d’être le nouvel État
nazi, justifie les discours les plus radicaux, allant jusqu’à normaliser
l’exigence du démantèlement d'Israël. Face à un État prétendument
génocidaire, quelle violence ne serait pas légitime ?

Ne soyons pas naïfs : ceux qui ont recours à cette terminologie ne le font
que pour accuser Israël. Leur indignation sélective épargne les exactions
des grands régimes autoritaires du monde et occulte les victimes ouighours en
Chine, rohingyas en Birmanie ou chrétiens du Nigéria... Comme bien entendu,
ils n’ont jamais critiqué les opérations militaires occidentales contre
Daech à Mossoul et Raqqa, ou contre Al Qaida en Afghanistan, malgré des
victimes civiles là aussi malheureusement nombreuses.

Ce renversement accusatoire est donc bien une stigmatisation volontaire et
calculée du seul État juif. Et, hélas, nous le savons, l'opprobre qu'elle
suscite s’étendra mécaniquement sur les Juifs, où qu'ils vivent.

Nous, Français juifs, avons la responsabilité de dénoncer ces amalgames
dangereux tant qu'il est encore temps.

Car non, Gaza n'est pas Auschwitz.

Yonathan Arfi, Président du Crif
