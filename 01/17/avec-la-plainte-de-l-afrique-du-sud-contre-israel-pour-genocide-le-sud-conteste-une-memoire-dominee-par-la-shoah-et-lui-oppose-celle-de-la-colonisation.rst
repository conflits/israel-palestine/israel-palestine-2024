

.. _kauffman_2024_01_17:

===================================================================================================================================================================================================
2024-01-17 « Avec la plainte de l’Afrique du Sud contre Israël pour génocide, le Sud conteste une mémoire dominée par la Shoah et lui oppose celle de la colonisation » par Sylvie Kauffmann
===================================================================================================================================================================================================

- https://www.lemonde.fr/idees/article/2024/01/17/avec-la-plainte-de-l-afrique-du-sud-contre-israel-pour-genocide-le-sud-conteste-une-memoire-dominee-par-la-shoah-et-lui-oppose-celle-de-la-colonisation_6211366_3232.html


Auteure Sylvie Kauffmann
============================

- https://www.lemonde.fr/signataires/sylvie-kauffmann/

« Avec la plainte de l’Afrique du Sud contre Israël pour génocide, le Sud conteste une mémoire dominée par la Shoah et lui oppose celle de la colonisation »
================================================================================================================================================================

Les audiences du recours déposé par Pretoria devant la Cour internationale
de justice illustrent l’émergence du Sud global et sa remise en cause
de l’ordre installé par les Occidentaux, estime dans sa chronique Sylvie
Kauffmann, éditorialiste au « Monde ».

Publié hier à 17h01, modifié hier à 19h07 Temps de Lecture 3 min. Read
in English

Ce n’est pas tout à fait un hasard si l’un des plus brillants conseils
de l’équipe de juristes qui plaide le recours de l’Afrique du Sud contre
Israël, accusé de génocide devant la Cour internationale de justice (CIJ), est
une avocate irlandaise. Perruque blanche du XVIIe siècle sur ses longs cheveux,
Blinne Ni Ghralaigh a fait un redoutable exposé clinique, jeudi 11 janvier
à La Haye, aux Pays-Bas, de ce qu’elle a qualifié de « premier génocide
diffusé en direct » à propos des Palestiniens de Gaza. La jeune juriste,
font valoir certains de ses admirateurs, jouit d’une double qualification :
experte reconnue dans la défense des droits humains en droit international,
elle vient d’un pays qui est une ancienne colonie.

Cette double qualification et le fait qu’elle soit évoquée illustrent la
dimension très particulière de la plainte déposée contre Israël devant la
plus haute juridiction des Nations unies. Fondé sur le caractère massif et
le bilan humain de la riposte militaire israélienne aux massacres commis par
le Hamas le 7 octobre, le recours sud-africain dépasse la simple procédure
judiciaire. Il est la plainte du Sud global contre les critères occidentaux de
la supériorité morale. Il est la remise en cause d’un ordre international
installé par le plus puissant allié de l’accusé, les Etats-Unis. Il est
aussi la contestation d’une mémoire dominée par la Shoah, à laquelle
s’oppose ouvertement celle de la colonisation.

Israël accusé de génocide devant la CIJ, « c’est le monde à l’envers
», s’est indigné Benyamin Nétanyahou, premier ministre d’un pays né
du plus grand génocide du XXe siècle, celui qui a vu six millions de juifs
exterminés par le régime nazi. Il ne croit pas si bien dire. Le monde, en
effet, est en train de s’inverser, et ce qui se passe ces jours-ci devant
les dix-sept juges de la CIJ à La Haye est le symbole de ce basculement.
« Soixante-quinze ans d’apartheid »

Quel que soit le verdict final de la Cour sur le caractère génocidaire
de l’offensive israélienne à Gaza, quelle que soit sa décision sur la
demande de suspension des opérations militaires présentée par Pretoria,
le seul fait que, dans le contexte actuel, cette accusation contre Israël
ait été portée par un pays lui-même symbole de la répression coloniale
et de la ségrégation raciale est historique.

« Les Palestiniens ont enduré soixante-quinze ans d’apartheid,
cinquante-six ans d’occupation et treize ans de blocus », a déclaré le
ministre sud-africain de la justice, Ronald Lamola, devant le tribunal. La
figure de Nelson Mandela, icône de la résistance à l’apartheid et de la
clarté morale, plane inévitablement sur ces audiences. Pour se défendre,
Israël a choisi un autre symbole, un rescapé de la Shoah, un juge de 87 ans,
Aharon Barak.

Mais comment ne pas voir derrière cet affrontement l’émergence du Sud
global comme force politique et la perte d’hégémonie du monde occidental
? « Ce changement de perspective, dans l’optique du Sud, serait d’autant
plus fort si Israël venait à être perçu comme génocidaire par la CIJ et
plus largement par l’opinion publique », relève Pierre Hazan, expert de
la médiation des conflits et auteur de plusieurs ouvrages sur la justice et
la guerre, dont Négocier avec le diable (Textuel, 2022).

En 1948, l’écrivain martiniquais Aimé Césaire « faisait remarquer que
l’Occident reconnaissait Auschwitz parce que les victimes étaient blanches,
mais pas Gorée parce que les victimes étaient africaines, souligne Pierre
Hazan. Le Sud global considère – largement à raison – que l’Occident
n’a jamais fait le travail de mémoire qui s’imposait pour les crimes de
la colonisation et de l’esclavage, alors qu’il l’a fait pour les crimes
nazis ».

Enjeu intérieur
===================

En témoigne la réaction du président de Namibie, Hage Geingob, lorsque
Berlin a proposé de défendre Israël devant la CIJ « compte tenu de
l’histoire allemande et du crime contre l’humanité de la Shoah » :
le président Geingob a rappelé que l’Allemagne avait commis le premier
génocide du XXe siècle dans son pays avec le massacre, de 1904 à 1908,
des peuples herero et nama, génocide reconnu par Berlin en 2021.

Déjà sensible au moment de l’invasion de l’Ukraine par la Russie,
en février 2022, lorsque de nombreux pays du Sud, notamment africains comme
l’Afrique du Sud, ont rejeté la lecture occidentale de l’agression russe,
cette dynamique Sud contre Nord a été galvanisée par les événements du
7 octobre.


Au mépris de sa définition juridique, le mot « génocide » est devenu courant à propos des Palestiniens de Gaza
=====================================================================================================================

Au mépris de sa définition juridique, le mot « génocide » est devenu courant
à propos des Palestiniens de Gaza. La violence des colons en Cisjordanie a
propulsé le facteur colonial sur le devant de la scène, mais à sens unique :
les efforts de l’Ukraine pour présenter aux pays du Sud l’agression russe
comme une guerre coloniale ont largement échoué.


Pour l’Afrique du Sud, l’enjeu est aussi intérieur
=======================================================

Pour l’Afrique du Sud, l’enjeu est aussi intérieur. Dans l’affaiblissement
des piliers de l’ANC, le parti au pouvoir depuis 1994, l’attachement à
la cause palestinienne et au système judiciaire est resté structurant. «
Comme nous, disait Mandela en 1990, les Palestiniens luttent pour le droit à
l’autodétermination. »

Et prendre la tête du mouvement propalestinien sur la scène internationale
grâce à la procédure devant la CIJ renforce la position de l’Afrique
du Sud dans l’ascension du Sud global.

**Cela lui permettra peut-être aussi de corriger le souvenir de 2015, lorsque
Pretoria avait accueilli le leader soudanais Omar Al-Bachir, recherché pour
génocide et crimes de guerre par la Cour pénale internationale, et l’avait
laissé repartir sans l’arrêter**.


Est-il possible de parvenir à additionner et à reconnaître les mémoires blessées, chacune dans sa singularité, sans chercher à les hiérarchiser ou à les opposer ?
=======================================================================================================================================================================

Est-il possible de parvenir à additionner et à reconnaître les mémoires
blessées, chacune dans sa singularité, sans chercher à les hiérarchiser
ou à les opposer ?

Pour Pierre Hazan, c’est l’un des enjeux de la bataille judiciaire à La
Haye.

Mais on en est, pour l’heure, au stade de l’affrontement.

Sylvie Kauffmann(Editorialiste au « Monde »)
