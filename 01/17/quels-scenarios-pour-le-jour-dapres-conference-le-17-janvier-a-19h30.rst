
.. _conf_2024_01_17:

================================================================================================
2024-01-17 Quels scénarios pour le « jour d’après »? Conférence le 17 janvier 2024 à 19H30
================================================================================================

- https://fr.jcall.eu/evenements/conferences/quels-scenarios-pour-le-jour-dapres-conference-le-17-janvier-a-19h30
- https://www.aacce.fr/2024/01/evenement.html


Plus de trois mois après le début de la guerre contre le Hamas suite aux crimes
atroces et aux viols qu’il a perpétrés le 7 octobre, les forces de Tsahal
intensifient leurs combats dans la bande de Gaza et la situation des populations
devient chaque jour de plus en plus dramatique. Le nombre de soldats tués
depuis l’entrée de l’armée israélienne dans la bande de Gaza augmente
chaque jour (175 à cette date) ainsi que celui des victimes palestiniennes
(plus de 21000 selon le ministère de la santé palestinien). Dans le nord,
le Hezbollah continue de lancer des roquettes contre des cibles civiles et
militaires, tout en ne s’engageant pas dans une guerre généralisée,
même après l’élimination à Beyrouth de Saleh Al-Aouri, le numéro 2
du Hamas par un drone israélien. Les Houthis au Yémen  poursuivent leurs
attaques contre les cargos en mer Rouge obligeant les marines occidentales
à intervenir pour rétablir le passage des navires dans un détroit qui est
vital pour le commerce international.

Les négociations pour libérer les 132 Israéliens toujours otages du Hamas
semblent arrêtées réduisant l’espoir de les récupérer tous vivants. Les
propos des ministres israéliens appartenant aux partis d’extrême droite
évoquant le déplacement possible de populations gazaouies vers des pays
tiers suscitent des condamnations de plus en plus vives de la part de pays
alliés d’Israël alors que les violences des colons contre les Palestiniens
en Cisjordanie et la répression de l’armée sont condamnées à l’ONU.

Le Premier ministre Benjamin Netanyahou, son ministre de la défense Yoav
Gallant ainsi que Beni Gantz, qui participent tous les trois au cabinet de
guerre restreint, continuent d’affirmer que les objectifs de guerre restent
l’éradication du Hamas et le retour des otages, mais ils ne fixent toujours
pas d’objectifs politiques à cette guerre avec un plan pour le « jour
d’après », un plan que ce gouvernement serait bien en peine de définir
compte tenu de la composition de sa coalition.

La population israélienne continue à soutenir son armée et à se mobiliser
pour aider les réfugiés qui ont dû quitter leurs localités situées près
des frontières Sud et Nord et pallier ainsi les déficiences de l’État,
mais elle reste divisée sur la façon de gérer la situation à Gaza après la
guerre. Et pourtant il faut dès maintenant s’y préparer et construire les
alliances nécessaires pour la prendre en charge. Vivant encore sous le coup
du traumatisme du 7 octobre et des annonces quotidiennes des soldats tombés
au combat, elle est peu préoccupée par la situation de la population de Gaza
dont les images sont absentes des chaînes de télévision israéliennes.

Israël ne peut pas poursuivre cette guerre sans le soutien constant des
États-Unis. Combien de temps ceux-ci pourront-ils continuer à le faire si
ce gouvernement ne définit pas ses objectifs pour l’après-guerre et ne
met pas un frein aux comportements de ses extrémistes ?

Quels sont les scénarios possibles pour le « jour d’après » ?

C’est ce dont nous débattrons le Mercredi 17 janvier à 19h30 (heure
française) au cours de la réunion que nous organisons en vidéo conférence
conjointement avec La Paix Maintenant

Avec la participation de :

Elie Barnavi professeur émérite d’histoire de l’Occident moderne
à l’Université de Tel-Aviv, ancien ambassadeur d’Israël en
France, directeur du comité scientifique du Musée de l’Europe à
Bruxelles. Auteur de nombreux ouvrages sur Israël, les religions et
le conflit Jean-Pierre Filiu, chercheur au CERI, est professeur des
universités en histoire du Moyen-Orient contemporain à Sciences Po
(Paris). Il est l’auteur de nombreux ouvrages, sur le monde arabe et
l’Islam contemporain, diffusés en plus de quinze langues

Inscription obligatoire pour participer à la vidéo conférence via le
formulaire ci-dessous

Vous recevrez en temps voulu le lien et les identifiants pour vous connecter.
Clôture des inscriptions: le 17 janvier à 13h.

Formulaire d’inscription : Formulaire colloque : Quels scénarios pour le
"jour d'après"?
